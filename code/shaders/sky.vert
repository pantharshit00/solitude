layout (location = 0) in vec4 position;
layout (location = 1) in vec4 tangent;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec2 uv0;
layout (location = 4) in vec2 uv1;
layout (location = 5) in vec4 color;

uniform mat4 u_vp;

out Data
{
	vec4 position;
	vec3 normal;
} o;

void main()
{
	v4 vertex = vec4(position.xyz * 100, 1.0);
	gl_Position = u_vp * vertex;
	o.position = vertex;
	o.normal = normal;
}
