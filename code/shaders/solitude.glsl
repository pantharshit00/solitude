#define m4 	mat4x4
#define r32 float
#define s32 int
#define u32 int unsigned
#define v2 	vec2
#define v3 	vec3
#define v4 	vec4

#define lerp(a, b, t) mix(a, b, t)
#define clamp01(t) clamp(t, 0.0, 1.0)

#define PI 3.1415926535897932384626433832795028841971693993751058209749445923078164062
#define TAU 6.283185307179586
#define DEG2RAD 0.017453292519943295
#define RAD2DEG 57.29577951308232

#define saturate(x) max(0.0, min(1.0, x))
#define luminance(x) dot(x, vec3(0.22, 0.707, 0.071))

vec2 parallax_offset(float h, float height, vec3 viewDir)
{
	h = h * height - height / 2.0;
	vec3 v = normalize(viewDir);
	v.z += 0.42;
	return h * (v.xy / v.z);
}

vec2 panorama(vec3 ray)
{
	return vec2(0.5 + 0.5 * atan(ray.x, -ray.z) / PI, acos(ray.y) / PI);
}