layout (location = 0) in vec4 position;
layout (location = 1) in vec4 tangent;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec2 uv0;
layout (location = 4) in vec2 uv1;
layout (location = 5) in vec4 color;

out Data
{
	vec4 position;
	vec2 uv0;
	vec2 uv1;
	vec4 color;
} o;

void main()
{
	gl_Position = position;
	o.position = position;
	o.uv0 = uv0;
	o.uv1 = uv1;
	o.color = color;
}
