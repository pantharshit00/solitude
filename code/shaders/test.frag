#version 450

layout (location = 0) out vec4 o_color;

layout (location = 0) in Data
{
	vec4 pos;
} i;

void main()
{
	vec4 c = i.pos;
	c += vec4(1.0, 1.0, 1.0, 1.0);
	c -= i.pos * 0.99999;
	o_color = c;
}