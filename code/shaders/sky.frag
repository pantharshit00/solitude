layout (location = 0) out vec4 color;

in Data
{
	vec4 position;
	vec3 normal;
} i;

uniform sampler2D u_main_tex;

void main()
{
	vec2 uv = panorama(normalize(i.normal));
	vec4 c = texture(u_main_tex, uv);
	color = c;
}
