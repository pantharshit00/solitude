#version 450

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 tangent;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec2 uv0;
layout (location = 4) in vec2 uv1;
layout (location = 5) in vec4 color;

layout(binding = 0) uniform Object
{
	mat4 mvp;
} object;

layout (location = 0) out Data
{
	vec4 pos;
} o;

out gl_PerVertex 
{
	vec4 gl_Position;
};

void main() 
{
	vec4 vertex = vec4(position.xyz, 1.0);
	vec4 pos = transpose(object.mvp) * vertex;
	o.pos = pos;
	gl_Position = pos;
}
