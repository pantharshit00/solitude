layout (location = 0) out vec4 color;

uniform sampler2D u_main_tex;

in Data
{
	vec4 position;
	vec2 uv0;
	vec2 uv1;
	vec4 color;
} i;

void main()
{
	vec2 uv = i.uv0;
	uv.y = 1.0 - uv.y;

	vec4 tex = texture(_MainTex, uv);
	vec4 col = tex.rrrr * i.color * _Color;
	color = col;
}
