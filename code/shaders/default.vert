layout (location = 0) in vec4 position;
layout (location = 1) in vec4 tangent;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec2 uv0;
layout (location = 4) in vec2 uv1;
layout (location = 5) in vec4 color;

layout(binding = 0) Shared {
	vec3 eye;
} global;

layout(binding = 1) Object {
	mat4 model;
	mat4 modelinv;
	mat4 mvp;
	vec4 tex_st;
	vec4 light_st;
} object;


uniform vec3 u_eye;

uniform vec4 u_color;
uniform mat4 u_model;
uniform mat4 u_modelinv;
uniform mat4 u_mvp;
uniform vec4 u_tex_st;
uniform vec4 u_light_st;

out Data
{
	vec4 pos;
	vec3 tangent;
	vec3 bitangent;
	vec3 normal;
	vec2 uv0;
	vec2 uv1;
	vec4 color;
	vec3 wpos;
	vec3 eye;
	vec3 viewdir;
} o;

void main()
{
	vec4 vertex = vec4(position.xyz, 1.0);
	vec4 wpos = transpose(u_model) * vertex;
	vec4 pos = transpose(u_mvp) * vertex;
	
	mat3 model_to_wdir = mat3(u_model);
	vec3 wnormal = normalize(model_to_wdir * normal);
	vec3 wtangent = normalize(model_to_wdir * tangent.xyz);
	vec3 wbinormal = cross(wnormal, wtangent) * tangent.w;
	vec3 eye = u_eye - wpos.xyz;
	
	o.pos = pos;
	o.tangent = wtangent;
	o.bitangent = wbinormal;
	o.normal = wnormal;
	o.uv0 = uv0 * u_tex_st.xy + u_tex_st.zw;
	o.uv1 = uv1 * u_light_st.xy + u_light_st.zw;
	o.color = color * u_color;
	o.wpos = wpos.xyz;
	o.eye = eye;
	o.viewdir = (u_modelinv * vec4(u_eye, 1.0)).xyz - position.xyz;
	
	gl_Position = pos;
}
