#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 tangent;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec2 uv0;
layout (location = 4) in vec2 uv1;
layout (location = 5) in vec4 color;

layout (binding = 0) uniform UniformBufferObject
{
	mat4 mvp;
} u;

void main()
{
	vec4 vertex = vec4(position.xyz, 1.0);
	vec4 pos = transpose(u.mvp) * vertex;
	gl_Position = pos;
}
