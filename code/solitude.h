namespace KeyCode
{
	enum : u64
	{
		None = 0,
		Escape = 1 << 1,

		A = 1 << 16,
		D = 1 << 17,
		S = 1 << 18,
		W = 1 << 19,
		Q = 1 << 20,
		E = 1 << 21,
	};
}

enum class RenderingType : u32
{
	None,
	Vulkan,
	OpenGL,
};

struct TextureCollection
{
	Texture albedo;
	Texture ao;
	Texture height;
	Texture normal;
	Texture roughness;
};

struct Solitude
{
	struct
	{
		r32 Fixed;
		r32 SinceStart;
		r32 MaxStep;
		r32 Delta;
	} Time;
    
	struct
	{
		r32 TargetRate;
		s32 Index;
        s32 Fixed;
	} Frame;
    
	struct
	{
		string Version;
		string Path;
		string AssetPath;
	} App;
    
	struct
	{
		s32 Width;
		s32 Height;
		r32 Aspect;
	} Screen;
    
	struct
	{
		Font Inconsolata;
		Texture Sky;
		TextureCollection RockyMoss;
		TextureCollection AcousticFoam;
        
        
		Texture Clear;
		Texture Black;
		Texture White;
		Texture Magenta;
	} Assets;
    
	struct
	{
		Camera MainCamera;
		v3 CameraVelocity;
		v3 CameraTargetPosition;
		r32 CameraRotateX;
		r32 CameraRotateY;
	} Game;
    
	struct
	{
		flags64 Keys;
		v2 Delta;
		v2 Deltas[16];
		s32 DeltaIndex;
	} Input;
    
	RenderSettings Rendering;
	RenderCommands ToRender;
	static_buffer_pool* Temp;
};
