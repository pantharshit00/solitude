static void
Start(Solitude& solitude)
{
	solitude.Input.Keys = KeyCode::None;
	solitude.Game.CameraTargetPosition = { 0,0,-10 };
	solitude.Game.MainCamera.transform = {
		solitude.Game.CameraTargetPosition,
		0.0f, 0.0f, 0.0f, 1.0f,
	};
	solitude.Game.CameraRotateX = 0.0f;
	solitude.Game.CameraRotateY = 0.0f;

    // OLD
	//InitializeDefaultTextures();
	var tmp0 = StaticBuffer(solitude.Temp);
	
    
    Font inconsolata = LoadFont(solitude, StringNonAlloc("fonts/inconsolata"), tmp0);
	solitude.Assets.Inconsolata = inconsolata;
    
	solitude.ToRender.IndexCapacity = SOLITUDE_MAX_ALLOC / sizeof(u32);
	solitude.ToRender.IndexCount = 0;
	solitude.ToRender.Indices = alloc(u32, solitude.ToRender.IndexCapacity);
	solitude.ToRender.VertexCapacity = SOLITUDE_MAX_ALLOC / sizeof(Vertex);
	solitude.ToRender.VertexCount = 0;
	solitude.ToRender.Vertices = alloc(Vertex, solitude.ToRender.VertexCapacity);
    
	InitializePrimitives();
    
    
	//solitude.Assets.Clear = ColorToTexture({ 0.0f, 0.0f, 0.0f, 0.0f });
	//solitude.Assets.Black = ColorToTexture({ 0.0f, 0.0f, 0.0f, 1.0f });
	//solitude.Assets.White = ColorToTexture({ 1.0f, 1.0f, 1.0f, 1.0f });
	//solitude.Assets.Magenta = ColorToTexture({ 1.0f, 0.0f, 1.0f, 1.0f });
    
    
	var path = String(tmp0);
	Append(path, solitude.App.AssetPath);
	Append(path, "skies/norway_valley.jpg");
	//Append(path, "skies/snowy_train_tracks.jpg");
	Terminate(path);
	//solitude.Assets.Sky = LoadTexture(path.data_cchar);
    
	if (false)
	{
		path.length = solitude.App.AssetPath.length;
		Append(path, "textures/TexturesCom_Rock_Mossy_1K_albedo.png");
		Terminate(path);
		//solitude.Assets.RockyMoss.albedo = LoadTexture(path.data_cchar);
        
		path.length = solitude.App.AssetPath.length;
		Append(path, "textures/TexturesCom_Rock_Mossy_1K_ao.png");
		Terminate(path);
		//solitude.Assets.RockyMoss.ao = LoadTexture(path.data_cchar);
        
		path.length = solitude.App.AssetPath.length;
		Append(path, "textures/TexturesCom_Rock_Mossy_1K_height.png");
		Terminate(path);
		//solitude.Assets.RockyMoss.height = LoadTexture(path.data_cchar);
        
		path.length = solitude.App.AssetPath.length;
		Append(path, "textures/TexturesCom_Rock_Mossy_1K_normal.png");
		Terminate(path);
		//solitude.Assets.RockyMoss.normal = LoadTexture(path.data_cchar);
        
		path.length = solitude.App.AssetPath.length;
		Append(path, "textures/TexturesCom_Rock_Mossy_1K_roughness.png");
		Terminate(path);
		//solitude.Assets.RockyMoss.roughness = LoadTexture(path.data_cchar);
	}
	{
		path.length = solitude.App.AssetPath.length;
		Append(path, "textures/TexturesCom_Various_AcousticFoam_4K_albedo.png");
		Terminate(path);
		//solitude.Assets.AcousticFoam.albedo = LoadTexture(path.data_cchar);
        
		path.length = solitude.App.AssetPath.length;
		Append(path, "textures/TexturesCom_Various_AcousticFoam_4K_ao.png");
		Terminate(path);
		//solitude.Assets.AcousticFoam.ao = LoadTexture(path.data_cchar);
        
		path.length = solitude.App.AssetPath.length;
		Append(path, "textures/TexturesCom_Various_AcousticFoam_4K_height.png");
		Terminate(path);
		//solitude.Assets.AcousticFoam.height = LoadTexture(path.data_cchar);
        
		path.length = solitude.App.AssetPath.length;
		Append(path, "textures/TexturesCom_Various_AcousticFoam_4K_normal.png");
		Terminate(path);
		//solitude.Assets.AcousticFoam.normal = LoadTexture(path.data_cchar);
        
		path.length = solitude.App.AssetPath.length;
		Append(path, "textures/TexturesCom_Various_AcousticFoam_4K_roughness.png");
		Terminate(path);
		//solitude.Assets.AcousticFoam.roughness = LoadTexture(path.data_cchar);
	}
    
    
	//_Setup(Cube);
	//_Setup(Quad);
	//_Start(solitude);
}

static void
Update(Solitude& solitude)
{
	///////////////////////////////////////////////////
	// Update
	// TODO: EXECUTE GAME STUFF
	///////////////////////////////////////////////////
    
	// Camera
	{
        var& time = solitude.Time;

		r32 weight = 1.0f;
		r32 divider = 1.0f;
		v2 delta = solitude.Input.Delta;
		for (var i = 0; i < 16; i++)
		{
			weight *= 0.5f;
			var j = (i + solitude.Input.DeltaIndex) % 24;
			delta += weight * solitude.Input.Deltas[j];
			divider += weight;
		}
		solitude.Input.DeltaIndex--;
		if (solitude.Input.DeltaIndex < 0)
			solitude.Input.DeltaIndex = 15;
		solitude.Input.Deltas[solitude.Input.DeltaIndex] = solitude.Input.Delta;
		delta /= divider;

		solitude.Game.CameraRotateX += delta.y * 0.015f;
		solitude.Game.CameraRotateY += delta.x * 0.015f;
		solitude.Game.MainCamera.transform.rotation = ToQuaternion({ solitude.Game.CameraRotateX, solitude.Game.CameraRotateY, 0 });
		var rotation = solitude.Game.MainCamera.transform.rotation;

		v3 cameraVelocity{ 0,0,0 };
		if		(solitude.Input.Keys & KeyCode::D) cameraVelocity += v3{ 1.0f, 0.0f, 0.0f };
		else if (solitude.Input.Keys & KeyCode::A) cameraVelocity -= v3{ 1.0f, 0.0f, 0.0f };
		if		(solitude.Input.Keys & KeyCode::E) cameraVelocity += v3{ 0.0f, 1.0f, 0.0f };
		else if (solitude.Input.Keys & KeyCode::Q) cameraVelocity -= v3{ 0.0f, 1.0f, 0.0f };
		if		(solitude.Input.Keys & KeyCode::W) cameraVelocity -= v3{ 0.0f, 0.0f, -1.0f };
		else if (solitude.Input.Keys & KeyCode::S) cameraVelocity += v3{ 0.0f, 0.0f, -1.0f };
        
		cameraVelocity = rotation * cameraVelocity;
		cameraVelocity = Normalize(cameraVelocity);
		cameraVelocity *= 10.0f * time.Delta;
		solitude.Game.CameraTargetPosition += cameraVelocity;
		solitude.Game.MainCamera.transform.position = SmoothDamp(
			solitude.Game.MainCamera.transform.position,
			solitude.Game.CameraTargetPosition,
			&solitude.Game.CameraVelocity,
			0.08f,
			time.Delta);
	}
}

static void
FixedUpdate(Solitude& solitude)
{
    
}

static void
DrawTexts(Solitude& solitude, string text, rect area, Font& font, TextSettings settings)
{
	Vertex vertices[4];
	v2 position{ area.min_x, area.max_y };
	r32 width = solitude.Screen.Width;
	r32 height = solitude.Screen.Height;

	for (var i = 0; i < text.length; i++)
	{
		assert(solitude.ToRender.VertexCount + 4 < solitude.ToRender.VertexCapacity);
		assert(solitude.ToRender.IndexCount + 6 < solitude.ToRender.IndexCapacity);
		assert(text[i] >= 0 && text[i] < 128);

		var c = text[i];
		var g = font.Glyphs[c];
		switch (c)
		{
			case ' ': {
				position.x += settings.character_spacing + g.advance;
			} break;
			case '\t': {} break;
			case '\n': {} break;
			case '\r': {} break;
			case '\v': {} break;
			case '\f': {} break;
			default:
			{
				vertices[0].position = { position.x / width, position.y / height, 0.0f, 1.0f };


			} break;
		}

	}
}

static void
Render(Solitude& solitude)
{
	//
	var rendering = &solitude.Rendering;

	// Setup camera
	var camera = solitude.Game.MainCamera;
	camera.aspect = solitude.Screen.Aspect;
	camera.ffd = 1.0f;// 0.78f;
	camera.ncp = 0.3f;
	camera.fcp = 1000.0f;
	m4_inv projection = Perspective(camera.aspect, camera.ffd, camera.ncp, camera.fcp);
	m4_inv view = MatrixView(camera.transform.position, camera.transform.rotation);
	m4_inv vp;
	vp.forward = projection.forward * view.inverse;
	vp.inverse = Inverse(vp.forward);

	rendering->View = view;
	rendering->Projection = projection;
	rendering->VP = vp;

	// UI
	{
		var width = solitude.Screen.Width;
		var height = solitude.Screen.Height;


	}
}


static flags64
SDL_KeycodeToKeyCode(SDL_Keycode code)
{
    switch(code)
    {
        case SDLK_ESCAPE: return KeyCode::Escape;
        case SDLK_a: return KeyCode::A;
        case SDLK_d: return KeyCode::D;
        case SDLK_s: return KeyCode::S;
        case SDLK_w: return KeyCode::W;
        case SDLK_q: return KeyCode::Q;
        case SDLK_e: return KeyCode::E;
    }
    return KeyCode::None;
}

static void
HandleEvent(Solitude& solitude, SDL_Event& event, bool& running)
{
    switch (event.type)
    {
        case SDL_KEYDOWN: {
            var code = SDL_KeycodeToKeyCode(event.key.keysym.sym);
            solitude.Input.Keys |= code;
            if (solitude.Input.Keys & KeyCode::Escape)
                running = false;
        } break;
        
        case SDL_KEYUP: {
            var code = SDL_KeycodeToKeyCode(event.key.keysym.sym);
            solitude.Input.Keys &= ~code;
        } break;
        
        case SDL_MOUSEMOTION: {
            r32 x = event.motion.xrel;
            r32 y = event.motion.yrel;
            solitude.Input.Delta = { x, y };
        } break;
        
        case SDL_QUIT: {
            running = false;
        } break;
    }
}

