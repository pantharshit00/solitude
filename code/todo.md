================TODO================
Compilable win32 application
-
String to Vertex[]
Add character spacing
Signed distance field for text and effects
-
Change <chrono> with internals
Change old update sleep time to smooth deltatime to get a more consistent framerate



================DONE================
Character to Vertex[4]
Primitive object Quad
Primitive object Cube
TempMem: Get and dispose temporary memory
LoadObj
Math functions
Initialize OpenGL
Create window





================NOTE================
# Shaders
http://docs.gl/gl4/glUniform
http://docs.gl/gl4/glGetUniform
https://docs.unity3d.com/Manual/SL-UnityShaderVariables.html

s_camera_pos
s_time
s_matrix_mvp > projection matrix * view * current model
s_matrix_mv > view * current model
s_matrix_v > view
s_matrix_p > projection matrix
s_matrix_vp > view * projection matrix
s_matrix_t_mv > transpose of model * view
s_matrix_it_mv > inverse transpose of model * view
s_object_to_world > model
s_world_to_object > inverse model
































