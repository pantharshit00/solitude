//#define SOLITUDE_TEMP_SIZE (64 * 1024)
//#define SOLITUDE_MAX_ALLOC (4 * 1024 * 1024)
//#define SOLITUDE_TEMP_COLLECTION_SIZE 64
//#define SOLITUDE_INTERNAL 1
#define SOLITUDE_DEBUG 1
#define SOLITUDE_DEBUG_SHADERS 1
#define SOLITUDE_VULKAN 1
#define SOLITUDE_GL 0
#define SOLITUDE_TIME_STEP 0.01f
#define SOLITUDE_APP_NAME "solitude"

#include "platform/platforms.hpp"
#include "tests/main.hpp"

struct main_state
{
	struct video video;
	struct vulkan vulkan;
};
void to_string(io::fmt* fmt, main_state* arg)
{
	write(fmt, "lol");
}

int main(int argc, char* argv[])
{
	var test_val2 = -0.2345f;
	var test_val = 4255;
	var test_val3 = float2 { test_val2, test_val2 * test_val2 };
	var test_int2 = int2 { test_val, test_val + 1 };
	print("Test %v %v %v %v %v", sqrt(2.0f), sqrt(0.5f), sqrt(10000), acos(0.2f), acos(0.3f));
	print("Test: %t %v", test_val2, acos(test_val2));
	print("Test: %t %v", test_val2, -((test_val2) / -0.0f));
	print("Test: %t %v", test_val, test_val);
	print("Test: %t %v", test_int2, test_int2);
	print("Test: %t %v", test_val3, test_val3);
	print("Test: %t", "test");
	print("Test: %t", 't');
	print("Test: %t", (long)'t');
	print(244);


	test_all();
	print("Running program");
	
	error err;
	var state = new main_state();
	var video = &state->video;
	var vulkan = &state->vulkan;

	print("Test: %v", state);

	if (err = init_platform())
	{
		print("An error occured while initializing platform: %v", err);
		return err;
	}

	if (err = init(video))
	{
		print("An error occured while initializing video device: %v", err);
		return err;
	}
	
	if (err = init(vulkan, video))
	{
		print("An error occured while initializing vulkan: %v", err);
		dispose(video);
		return err;
	}
	
	print("Starting main loop..");
	for(bool run = true; run;)
	{
		for (var i = 0; i < 1024; i++)
		{
			var event = poll(video);
			if (event.type == InputEvent::Key)
			{
				run = false;
				break;
			}

			if (event.type == 0)
				break;
		}
		begin_frame(vulkan);
		print("test");
		end_frame(vulkan);
	}
	
	print("Closing..");
	dispose(vulkan);
	dispose(video);
	return 0;
}

