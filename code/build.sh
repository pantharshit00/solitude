#!/bin/bash
start_time=`date +%s%N`


# Determine arguments
run=0
release=0
for arg_i in "$@"
do
	if [ "$arg_i" = "-e" ] || [ "$arg_i" = "--release" ]
	then
		release=1
	fi
	if [ "$arg_i" = "-r" ] || [ "$arg_i" = "--run" ]
	then
		run=1
	fi
	if [ "$arg_i" = "-h" ] || [ "$arg_i" = "--help" ]
	then
		echo "-e --release  Export a build of the game"
		echo "-r --run      Run the game after compilation"
		echo "-h --help     Print available commands"
		exit 0
	fi
done






optimization=""
if [ $release = 1 ]
then
	optimization="-O3"
fi

cdir=$(dirname "${BASH_SOURCE[0]}")
file="/solitude.cpp"
rdir="/../builds/linux64/solitude"
echo "compiling \"$cdir$file\" to \"$cdir$rdir\""

if g++ -g -rdynamic "$cdir$file" -lX11 -m64 $optimization -o "$cdir$rdir" -Wl,--no-as-needed -ldl
then
	elapsed=$((($(date +%s%N) - $start_time)/1000000))
	echo "time elapsed: $(($elapsed/1000)).$(($elapsed%1000))s"
	echo
	echo
	if [ $run = 1 ]
	then
		$cdir$rdir
	fi
else
	elapsed=$((($(date +%s%N) - $start_time)/1000000))
	echo "time elapsed: $(($elapsed/1000)).$(($elapsed%1000))s"
fi

exit 0

