#define CRASH *(volatile int*)0=0
#if SOLITUDE_DEBUG
#define assert(expr) if (!(expr)) { *(volatile int*)0=0; }
#define s_assert(expr) static_assert(expr, "FAILED assertion: " #expr)
#define NotImplemented assert(!"NotImplemented")
#else
#define assert(expr)
#define s_assert(expr)
#define NotImplemented NotImplemented!
#endif



#define CONCAT_(a, b) a ## b
#define CONCAT(a, b) CONCAT_(a, b) 
#define MACRO_AS_STRING(x) MACRO_AS_STRING_(x)
#define MACRO_AS_STRING_(x) #x
#define VAR(name) CONCAT(name ## __SOLITUDE_MACRO_VAR_, __LINE__)

#define ARGUMENT_AT_0(arg0, ...) arg0
#define ARGUMENT_AT_1(arg0, arg1, ...) arg1
#define ARGUMENT_AT_2(arg0, arg1, arg2, ...) arg2
#define ARGUMENT_AT_3(arg0, arg1, arg2, arg3, ...) arg3
#define ARGUMENT_AT_4(arg0, arg1, arg2, arg3, arg4, ...) arg4
#define ARGUMENT_AT_0_(tuple) ARGUMENT_AT_0 tuple
#define ARGUMENT_AT_1_(tuple) ARGUMENT_AT_1 tuple
#define ARGUMENT_AT_2_(tuple) ARGUMENT_AT_2 tuple
#define ARGUMENT_AT_3_(tuple) ARGUMENT_AT_3 tuple
#define ARGUMENT_AT_4_(tuple) ARGUMENT_AT_4 tuple

#define iterate(i, arr)   for (int i = 0, VAR(length) = len(arr); i < VAR(length); ++i)
#define iterate_r(i, arr) for (int i = len(arr) - 1; i >= 0; --i)
#define foreach(v, arr)   \
	for (int VAR(keep) = 1, VAR(i) = 0, VAR(length) = len(arr);\
         VAR(keep) && VAR(i) < VAR(length); VAR(keep) = !VAR(keep), VAR(i)++)\
    		for (v = arr[VAR(i)]; VAR(keep); VAR(keep) = !VAR(keep))

#define null nullptr
#define var auto
#define let auto

#define ALIGN_POW2(v, a) (((v)+((a)-1)) & ~(((v)-(v))+(a)-1))
#define ALIGN_8(v) (((v)+7)&~7)
#define ALIGN_16(v) (((v)+15)&~15)
#define IS_POW2(v) (((v) & ~((v) - 1)) == (v))
#define KILOBYTE (1<<10)
#define MEGABYTE (1<<20)
#define GIGABYTE (1<<30)


// When cstddef is not included, create our own offsetof
#ifndef offsetof
#define offsetof(t, p) ((uptr)&(((t*)0)->p))
#endif



/**
 * Strings and buffers
 *
 *
 *
 *
 *
 * Notes:
 * Procedures clear and copy could profit from
 * loop unrolling. But it increases complexity.
 */
#define ARRAY_LEN(a) (sizeof(a)/sizeof(a[0]))
template<uptr N, class T>
constexpr uptr len(T(&)[N]) { return N; }
constexpr int len(int i) { return i; }
inline int len(const char* c)
{
	int result = 0;
	if (!c) result;
	while(c[result]) ++result;
	return result;
}
inline int cap(const char* c)
{
	int result = 0;
	if (!c) result;
	while(c[result++]);
	return result;
}
inline void copy(void* dest, void* source, uptr length)
{
	#if 0
	// https://elixir.bootlin.com/linux/latest/source/lib/string.c#L875
	byte* d;
	byte* s;
	uint* dw = (uint*)dest;
	uint* sw = (uint*)source;
	
	if (!((uptr)dw & 3) && !((uptr)sw & 3))
	{
		for (; len >= 4; len -= 4)
			*dw++ = *sw++;
	}

	d = (byte*)dw;
	s = (byte*)sw;
	for (; len >= 1; len -= 1)
		*d++ = *s++;
	#else
	var d = (byte*)dest;
	var s = (byte*)source;
	while(length--)
		*d++ = *s++;
	#endif
}
inline void clear(void* ptr, uptr size)
{
	var bytes = (byte*)ptr;
	while (size--) *bytes++ = 0;
}
inline uint hash(const void* src, uptr len, uint seed = 0)
{
	// Little-endian and big-endian will not produce the same result.
	// Therefore, only use this if the result is only used internally.
	// e.g. don't use it in save files, for it can be shared across platforms.
	// https://sites.google.com/site/murmurhash/
	const uint m = 0x5BD1E995;
	const int r = 24;
	const byte* data = (const byte*)src;
	uint h = seed ^ len;
	
	while (len >= 4)
	{
		uint k = *(const uint*)data;
		k *= m;
		k ^= k >> r;
		k *= m;
		h *= m;
		h ^= k;
		data += 4;
		len -= 4;
	}
	
	switch (len)
	{
		case 3: h ^= data[2] << 16;
		case 2: h ^= data[1] << 8;
		case 1: h ^= data[0];
		h *= m;
	}
	
	h ^= h >> 13;
	h *= m;
	h ^= h >> 15;
	
	return h;
}
inline bool equals(const void* lhs, const void* rhs, uptr len)
{
	const byte* l = (const byte*)lhs;
	const byte* r = (const byte*)rhs;
	while (len >= 4)
	{
		uint ld = *(const uint*)l;
		uint rd = *(const uint*)r;
		if (ld != rd)
			return false;

		l += 4;
		r += 4;
		len -= 4;
	}
	
	switch (len)
	{
		case 3: if (l[2] != r[2]) return false;
		case 2: if (l[1] != r[1]) return false;
		case 1: if (l[0] != r[0]) return false;
	}
	
	return true;
}


struct fixed_buffer
{
	byte* data;
	uptr length;

	fixed_buffer() = default;
	fixed_buffer(const char* c)
	{
		this->data = (byte*)c;
		this->length = len(c);
	}

	inline byte const& operator[](int index) const { return data[index]; }
	inline byte& operator[](int index) { return data[index]; }
};
using fixed_utf8   = fixed_buffer;
using fixed_string = fixed_utf8;

inline uptr len(fixed_buffer b) { return b.length; }
inline uptr cap(fixed_buffer b) { return b.length; }
inline void clear(fixed_buffer b)
{
	var size = b.length;
	while(size--) *b.data++ = 0;
}
inline bool equals(fixed_buffer a, fixed_buffer b) { return a.length == b.length && equals(a.data, b.data, a.length); }


struct buffer : public fixed_buffer
{
	uptr capacity;
};
using utf8    = buffer;
using string  = utf8;
inline uptr cap(buffer b) { return b.capacity; }
inline void clear(buffer b)
{
	var size = b.capacity;
	while(size--) *b.data++ = 0;
}


template<typename T>
struct slice
{
	T* elements;
	int length;
	
	slice() = default;
	slice(fixed_buffer b) : elements((T*)b.data), length(b.length / sizeof(T)) {
		assert(length * sizeof(T) == b.length);
	}

	inline T const& operator[](int index) const { return elements[index]; }
	inline T& operator[](int index) { return elements[index]; }
};

template<typename T>
inline int len(slice<T> s) { return s.length; }
template<typename T>
inline int cap(slice<T> s) { return s.length; }



template<typename T>
struct array
{
	T* elements;
	int length;
	int capacity;
	flags32 flags;
};

template<typename T>
inline int len(array<T> a) { return a.length; }
template<typename T>
inline int cap(array<T> a) { return a.length; }

/*template<typename T>
void free(array<T> arr)
{
	assert(arr.flags & memory::OWNER);
	free(arr.elements);
	arr.length = 0;
	arr.capacity = 0;
}*/





/**
 *
 *
 *
 *
 *
 *
 */
struct error
{
	int code;
	fixed_string message;
	fixed_string trace;
	
	error() = default;
	error(int c, fixed_string m, fixed_string t) : code(c), message(m), trace(t) {}
	
	operator int() const { return code; }
	
	static const error ok;
};
const error error::ok = error { };
#define make_error(str) (error(1, str, __FILE__ "::" MACRO_AS_STRING(__LINE__)))



/**
 * Multithreading
 *
 *
 *
 *
 *
 *
 *
 */
#if COMPILER_MSVC
	#define READ_BARRIER _ReadBarrier()
	#define WRITE_BARRIER _WriteBarrier()
	#define ATOMIC_COMPARE_EXCHANGE(v, n, e) _InterlockedCompareExchange((long volatile *)v, n, e)
	#define ATOMIC_EXCHANGE(v, n) _InterlockedExchange64((__int64 volatile *)v, n)
	#define ATOMIC_ADD(v, a) _InterlockedExchangeAdd64((__int64 volatile *)v, a)
	inline uint current_thread_id()
	{
		var thread_id = (byte*)__readgsqword(0x30);
		return *(uint*)(thread_id + 0x48);
	}
#elif COMPILER_LLVM
	#define READ_BARRIER asm volatile("" ::: "memory")
	#define WRITE_BARRIER asm volatile("" ::: "memory")
	#define ATOMIC_COMPARE_EXCHANGE(v, n, e) __sync_val_compare_and_swap(v, e, n)
	#define ATOMIC_EXCHANGE(v, n) __sync_lock_test_and_set(v, n)
	#define ATOMIC_ADD(v, a) __sync_fetch_and_add(v, a)
	inline uint current_thread_id()
	{
		uint thread_id;
		#if defined(__APPLE__) && defined(__x86_64__)
		asm("mov %%gs:0x00,%0" : "=r"(thread_id));
		#elif defined(__i386__)
		asm("mov %%gs:0x08,%0" : "=r"(thread_id));
		#elif defined(__x86_64__)
		asm("mov %%fs:0x10,%0" : "=r"(thread_id));
		#else
		#error Unsupported architecture
		#endif
		return thread_id;
	}
#else
	#error Compiler not supported!
#endif

struct mutex
{
	ulong volatile ticket;
	// some add 64 bytes padding to avoid false sharing
	ulong volatile serving;
};
inline void lock(mutex* m)
{
	var ticket = ATOMIC_ADD(&m->ticket, 1);
	while (ticket != m->serving);
}
inline void unlock(mutex* m)
{
	ATOMIC_ADD(&m->serving, 1);
}







union int_float
{
	float f;
	int i;
	uint u;
	byte b[4];
};

union mutable_const_ptr
{
	const void* c;
	void* m;
};




// Events
struct InputEvent
{
	static constexpr int Key = 1;
	int type;

	union
	{
		struct
		{
			int code;
		} key;
	};
};







// Math types 
union uint2
{
	struct { uint x, y; };
	uint e[2];
};
union uint3
{
	struct
	{
		union
		{
			uint2 xy;
			struct { uint x, y; };
		};
		uint z;
	};
	struct { uint __0; uint2 yz; };
	struct { uint r, g, b; };
	uint e[3];
};



union int2
{
	struct { int x, y; };
	int e[2];
};
union int3
{
	struct
	{
		union
		{
			int2 xy;
			struct { int x, y; };
		};
		int z;
	};
	struct { int __0; int2 yz; };
	struct { int r, g, b; };
	int e[3];
};
union int4
{
	struct
	{
		union
		{
			struct { int x, y; };
			struct { int r, g; };
			int2 xy;
			int2 rg;
		};
		union
		{
			struct { int z, w; };
			struct { int b, a; };
			int2 zw;
			int2 ba;
		};
	};
	struct
	{
		int __0;
		union
		{
			int2 yz;
			int2 gb;
		};
		int __1;
	};
	struct
	{
		union
		{
			int3 xyz;
			int3 rgb;
		};
		int __2;
	};
	struct
	{
		int __3;
		union
		{
			int3 yzw;
			int3 gba;
		};
	};
	int e[4];
};

union float2
{
	struct { float x, y; };
	float e[2];
};
union float3
{
	struct
	{
		union
		{
			float2 xy;
			struct { float x, y; };
		};
		float z;
	};
	struct { float __0; float2 yz; };
	struct { float r, g, b; };
	float e[3];
};
union float4
{
	struct
	{
		union
		{
			struct { float x, y; };
			struct { float r, g; };
			float2 xy;
			float2 rg;
		};
		union
		{
			struct { float z, w; };
			struct { float b, a; };
			float2 zw;
			float2 ba;
		};
	};
	struct
	{
		float __0;
		union
		{
			float2 yz;
			float2 gb;
		};
		float __1;
	};
	struct
	{
		union
		{
			float3 xyz;
			float3 rgb;
		};
		float __2;
	};
	struct
	{
		float __3;
		union
		{
			float3 yzw;
			float3 gba;
		};
	};
	float e[4];
};

union Rotor
{
	struct
	{
		union
		{
			float3 xyz;
			struct { float x, y, z; };
		};
		float w;
	};
	float4 v;
	float e[4];
};
union Quaternion
{
	struct
	{
		union
		{
			float3 xyz;
			struct { float x, y, z; };
		};
		float w;
	};
	float4 v;
	float e[4];
};

// layout - Row Major - m[row][col]
//   |   0     1
// --+------------
// 0 | 0:m00 1:m01
// 1 | 2:m10 3:m11
union float2x2
{
	struct
	{
		float m00, m01;
		float m10, m11;
	};

	float m[2][2];
	float e[4];
	float2 columns[2];
};


// layout - Row Major - m[row][col]
//   |   0     1     2
// --+------------------
// 0 | 0:m00 1:m01 2:m02
// 1 | 3:m10 4:m11 5:m12
// 2 | 6:m20 7:m21 8:m22
union float3x3
{
	struct
	{
		float m00, m01, m02;
		float m10, m11, m12;
		float m20, m21, m22;
	};

	float m[3][3];
	float e[9];
	float3 columns[3];
};


// layout - Row Major - m[row][col]
//   |    0      1      2      3
// --+----------------------------
// 0 |  0:m00  1:m01  2:m02  3:m03
// 1 |  4:m10  5:m11  6:m12  7:m13
// 2 |  8:m20  9:m21 10:m22 11:m23
// 3 | 12:m30 13:m31 14:m32 15:m33
union float4x4
{
	struct
	{
		float m00, m01, m02, m03;
		float m10, m11, m12, m13;
		float m20, m21, m22, m23;
		float m30, m31, m32, m33;
	};

	float m[4][4];
	float e[16];
	float4 columns[4];
};

struct float4x4fi
{
	float4x4 forward;
	float4x4 inverse;
};




// Rendering
struct vertex
{
	float4 position;
	float4 tangent;
	float3 normal;
	float2 uv0; // albedo texture
	float2 uv1; // lightmap texture
	float4 color; // TODO: maybe pack as uint?
	ushort light_index;
};



