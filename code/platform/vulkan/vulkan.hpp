#define VULKAN_DEBUG SOLITUDE_DEBUG & 1
#define VULKAN_FRAME_COUNT 2
#define VULKAN_REQUIRE_COMPUTE 0
#define VULKAN_REQUIRE_TRANSFER 0
#define VK_LOCAL_FUNC(handle, name) {\
	reinterpret_cast<void*&>(name) = func_of(handle, #name);\
	if (!name) {\
		return error(1,\
			"Could not find " #name " in " #handle ".",\
			__FILE__ "::" MACRO_AS_STRING(__LINE__));\
	}\
}
#define VK_INIT_FUNC(handle, name) {\
	reinterpret_cast<void*&>(vk->name) = func_of(handle, #name);\
	if (!vk->name) {\
		return error(1,\
			"Could not find " #name " in " #handle ".",\
			__FILE__ "::" MACRO_AS_STRING(__LINE__));\
	}\
}

#include "vulkan.h"

constexpr ulong VULKAN_MEMORY_CHUNK_CAPACITY = 256 * MEGABYTE;
constexpr int VULKAN_MAX_TEXTURE_COUNT = 512;



struct vulkan
{
	typedef void* (*get_proc_addr_t)(void*, const char*);
	struct memory_pool
	{
		VkPtr        memory;
		ulong        capacity;
		ulong        length;
		memory_pool* next;
	};
	struct memory_chunk
	{
		VkPtr memory;
		ulong offset;
	};
	struct buffer
	{
		VkPtr buffer;
		memory_chunk chunk;
		ulong capacity;
		ulong length;
		VkMemoryRequirements requirements;
	};
	struct buffer_chunk
	{
		VkPtr buffer;
		ulong offset;
		memory_chunk chunk;
	};
	struct image
	{
		VkImageCreateInfo info;
		VkPtr image;
		memory_chunk chunk;
	};
	struct render_target : public image
	{
		VkFormat format;
		VkPtr view;

		// TODO: Improve datastructure when multiple views
		VkPtr combined_image_sampler;
		VkPtr input_attachment;
		VkPtr storage_image;
	};
	struct vk_pipeline
	{
		VkPtr handle;
		VkPtr layout;
	};
	struct shader
	{
		void* data;
		int length;
		uptr handle;
	};
	struct alignas(256) ubo
	{
		alignas(64) float4x4 transform;
		alignas(16) float3   camera_position;
		// add fog and stuff
	};

	memory::pool pool;
	void* libvulkan;
	void* instance;
	void* device;
	void* physical_device;
	memory_pool memory_pools[32];
	buffer uniform_buffer;
	buffer vertex_buffer;
	buffer index_buffer;
	buffer uniform_buffer_staging;
	buffer vertex_buffer_staging;
	buffer index_buffer_staging;

	VkPtr debug_messenger;
	slice<VkExtensionProperties> extension_properties;
	slice<VkLayerProperties> layer_properties;
	get_proc_addr_t func_of_instance;
	get_proc_addr_t func_of_device;
	
	static constexpr int image_capacity = 4;
	struct
	{
		VkPtr    handle;
		uint2    extent;
		uint     index;
		uint     length;
		VkFormat format;
		VkPtr    images[image_capacity];
		VkPtr    views[image_capacity];
		VkPtr    fences[image_capacity];
	} swapchain;

	void* command_buffers[image_capacity];
	void* command_buffer;
	void* graphics_queue;
	void* presentation_queue;
	VkPtr command_pool;
	VkPtr request_semaphore;
	VkPtr present_semaphore;
	VkPtr timestamp_query_pool;
	buffer staging_buffer;

	// 
	VkViewport fullscreen;
	VkRect2D fullscreen_rect;
	VkPtr descriptor_pool;
	VkPtr descriptor_input_attachment;
	VkPtr descriptor_combined_image_sampler;
	VkPtr descriptor_storage_image;
	VkPtr descriptor_dynamic_uniform_buffer_object;
	VkPtr descriptorset_dynamic_ubo;
	VkPtr sampler;
	render_target rt_color_atc;
	render_target rt_zbuffer;
	render_target rt_zbuffer_atc;
	VkAttachmentDescription ad_swapchain;
	VkAttachmentDescription ad_color_multisample_atc;
	VkAttachmentDescription ad_zbuffer;
	VkAttachmentDescription ad_zbuffer_multisample_atc;

	struct
	{
		VkPtr swapchain;
		VkPtr swapchain_zbuffer;
		//VkPtr swapchain_zbuffer_atc;
	} pass;
	struct
	{
		VkPtr swapchain[image_capacity];
		VkPtr swapchain_zbuffer[image_capacity];
		VkPtr swapchain_zbuffer_atc[image_capacity];
	} framebuffer;
	struct
	{
		VkPipelineViewportStateCreateInfo fullscreen;
		VkPipelineRasterizationStateCreateInfo rasterization_cull_none;
		VkPipelineRasterizationStateCreateInfo rasterization_cull_back;
		VkPipelineRasterizationStateCreateInfo rasterization_cull_front;
		VkPipelineMultisampleStateCreateInfo multisample_none;
		VkPipelineMultisampleStateCreateInfo multisample_atc;
		VkPipelineColorBlendStateCreateInfo color_blend_none;
		VkPipelineColorBlendAttachmentState color_blend_attachment_none;
		VkPipelineColorBlendStateCreateInfo color_blend_premultiplied_alpha;
		VkPipelineColorBlendAttachmentState color_blend_attachment_premultiplied_alpha;
		VkPipelineDepthStencilStateCreateInfo zbuffer;
		VkPipelineDepthStencilStateCreateInfo zbuffer_test;
		VkPipelineInputAssemblyStateCreateInfo triangle_assembly;
		VkPipelineDynamicStateCreateInfo scissor;

		struct
		{
			VkVertexInputBindingDescription binding;
			VkVertexInputAttributeDescription descriptions[6];
			VkPipelineVertexInputStateCreateInfo info;
		} vertex_in;

	} pipeline;
	vk_pipeline pipelines[1];
	

	
	// TODO: Default textures: white, black, clear, magenta, cyan

	// Procedures
	VkResult (*vkEnumerateInstanceExtensionProperties)(const char*, uint*, void*);
	VkResult (*vkEnumerateInstanceLayerProperties)(uint*, void*);
	VkResult (*vkCreateInstance)(const void*, const void*, void**);

	void     (*vkDestroyInstance)(void*, const void*);
	VkResult (*vkEnumeratePhysicalDevices)(void*, uint*, void**);
	VkResult (*vkEnumerateDeviceExtensionProperties)(void*, const char*, uint*, void*);
	VkResult (*vkEnumerateDeviceLayerProperties)(void*, uint*, void*);
	void     (*vkGetPhysicalDeviceFeatures)(void*, void*);
	void     (*vkGetPhysicalDeviceFormatProperties)(void*, VkFormat, void*);
	VkResult (*vkGetPhysicalDeviceImageFormatProperties)(void*, VkFormat, VkImageType, VkImageTiling, flags32, flags32, void*);
	void     (*vkGetPhysicalDeviceProperties)(void*, void*);
	void     (*vkGetPhysicalDeviceQueueFamilyProperties)(void*, uint*, void*);
	void     (*vkGetPhysicalDeviceMemoryProperties)(void*, void*);
	VkResult (*vkGetPhysicalDeviceSurfaceSupportKHR)(void*, uint, VkPtr, uint*);
	VkResult (*vkGetPhysicalDeviceSurfaceCapabilitiesKHR)(void*, VkPtr, void*);
	VkResult (*vkGetPhysicalDeviceSurfaceFormatsKHR)(void*, VkPtr, uint*, void*);
	VkResult (*vkGetPhysicalDeviceSurfacePresentModesKHR)(void*, VkPtr, uint*, void*);
	VkResult (*vkGetImageMemoryRequirements)(void*, VkPtr, void*);
	VkResult (*vkGetBufferMemoryRequirements)(void*, VkPtr, void*);
	VkResult (*vkGetSwapchainImagesKHR)(void*, VkPtr, uint*, VkPtr*);
	void     (*vkGetDeviceQueue)(void*, uint, uint, void**);
	VkResult (*vkCreateSwapchainKHR)(void*, const void*, const void*, void*);
	VkResult (*vkCreateDevice)(void*, const void*, const void*, void**);
	VkResult (*vkCreateCommandPool)(void*, const void*, const void*, void*);
	VkResult (*vkCreateBuffer)(void*, const void*, const void*, void*);
	VkResult (*vkCreateSemaphore)(void*, const void*, const void*, void*);
	VkResult (*vkCreateFence)(void*, const void*, const void*, void*);
	VkResult (*vkCreateQueryPool)(void*, const void*, const void*, void*);
	VkResult (*vkCreateImage)(void*, const void*, const void*, void*);
	VkResult (*vkCreateImageView)(void*, const void*, const void*, void*);
	VkResult (*vkCreateDescriptorPool)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkCreateDescriptorSetLayout)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkCreatePipelineLayout)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkCreateGraphicsPipelines)(void*, VkPtr, uint, const void*, const void*, VkPtr*);
	VkResult (*vkCreateSampler)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkCreateRenderPass)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkCreateFramebuffer)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkUpdateDescriptorSets)(void*, uint, const void*, uint, const void*);
	VkResult (*vkAcquireNextImageKHR)(void*, VkPtr, ulong, VkPtr, VkPtr, uint*);
	VkResult (*vkBindBufferMemory)(void*, VkPtr, VkPtr, ulong);
	VkResult (*vkBindImageMemory)(void*, VkPtr, VkPtr, ulong);
	VkResult (*vkQueueSubmit)(void*, uint, const void*, VkPtr);
	VkResult (*vkQueueWaitIdle)(void*);
	VkResult (*vkQueuePresentKHR)(void*, const void*);
	VkResult (*vkDeviceWaitIdle)(void*);
	VkResult (*vkAllocateCommandBuffers)(void*, const void*, void**);
	VkResult (*vkAllocateMemory)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkAllocateDescriptorSets)(void*, const void*, VkPtr*);
	void     (*vkFreeMemory)(void*, VkPtr, const void*);
	VkResult (*vkMapMemory)(void*, VkPtr, ulong, ulong, flags32, void**);
	void     (*vkUnmapMemory)(void*, VkPtr);
	VkResult (*vkBeginCommandBuffer)(void*, const void*);
	VkResult (*vkEndCommandBuffer)(void*);
	VkResult (*vkWaitForFences)(void*, uint, const VkPtr*, uint, ulong); 
	VkResult (*vkResetCommandPool)(void*, VkPtr, flags32);
	VkResult (*vkResetFences)(void*, uint, const VkPtr*);
	void     (*vkDestroyDevice)(void*, const void*);
};



void dispose_platform(vulkan*);
error init_platform(vulkan*);
error init_instance_properties(vulkan*, const char*[], int*, const char*[], int*);
error init_surface(vulkan*, video*, VkPtr*);
error init_vulkan_procedures(vulkan* vk)
{
	var func_of = vk->func_of_instance;
	var instance = vk->instance;
	VK_INIT_FUNC(instance, vkDestroyInstance);
	VK_INIT_FUNC(instance, vkEnumeratePhysicalDevices);
	VK_INIT_FUNC(instance, vkEnumerateDeviceExtensionProperties);
	VK_INIT_FUNC(instance, vkEnumerateDeviceLayerProperties);
	VK_INIT_FUNC(instance, vkGetPhysicalDeviceFeatures);
	VK_INIT_FUNC(instance, vkGetPhysicalDeviceFormatProperties);
	VK_INIT_FUNC(instance, vkGetPhysicalDeviceImageFormatProperties);
	VK_INIT_FUNC(instance, vkGetPhysicalDeviceProperties);
	VK_INIT_FUNC(instance, vkGetPhysicalDeviceQueueFamilyProperties);
	VK_INIT_FUNC(instance, vkGetPhysicalDeviceMemoryProperties);
	VK_INIT_FUNC(instance, vkGetPhysicalDeviceSurfaceSupportKHR);
	VK_INIT_FUNC(instance, vkGetPhysicalDeviceSurfaceCapabilitiesKHR);
	VK_INIT_FUNC(instance, vkGetPhysicalDeviceSurfaceFormatsKHR);
	VK_INIT_FUNC(instance, vkGetPhysicalDeviceSurfacePresentModesKHR);
	VK_INIT_FUNC(instance, vkGetImageMemoryRequirements);
	VK_INIT_FUNC(instance, vkGetBufferMemoryRequirements);
	VK_INIT_FUNC(instance, vkGetSwapchainImagesKHR);
	VK_INIT_FUNC(instance, vkGetDeviceQueue);
	VK_INIT_FUNC(instance, vkCreateSwapchainKHR);
	VK_INIT_FUNC(instance, vkCreateDevice);
	VK_INIT_FUNC(instance, vkCreateCommandPool);
	VK_INIT_FUNC(instance, vkCreateBuffer);
	VK_INIT_FUNC(instance, vkCreateSemaphore);
	VK_INIT_FUNC(instance, vkCreateFence);
	VK_INIT_FUNC(instance, vkCreateQueryPool);
	VK_INIT_FUNC(instance, vkCreateImage);
	VK_INIT_FUNC(instance, vkCreateImageView);
	VK_INIT_FUNC(instance, vkCreateDescriptorPool);
	VK_INIT_FUNC(instance, vkCreateDescriptorSetLayout);
	VK_INIT_FUNC(instance, vkCreateSampler);
	VK_INIT_FUNC(instance, vkCreateRenderPass);
	VK_INIT_FUNC(instance, vkCreateFramebuffer);
	VK_INIT_FUNC(instance, vkCreatePipelineLayout);
	VK_INIT_FUNC(instance, vkCreateGraphicsPipelines);
	VK_INIT_FUNC(instance, vkUpdateDescriptorSets);
	VK_INIT_FUNC(instance, vkAcquireNextImageKHR);
	VK_INIT_FUNC(instance, vkBindBufferMemory);
	VK_INIT_FUNC(instance, vkBindImageMemory);
	VK_INIT_FUNC(instance, vkQueueSubmit);
	VK_INIT_FUNC(instance, vkQueueWaitIdle);
	VK_INIT_FUNC(instance, vkQueuePresentKHR);
	VK_INIT_FUNC(instance, vkDeviceWaitIdle);
	VK_INIT_FUNC(instance, vkAllocateCommandBuffers);
	VK_INIT_FUNC(instance, vkAllocateMemory);
	VK_INIT_FUNC(instance, vkAllocateDescriptorSets);
	VK_INIT_FUNC(instance, vkFreeMemory);
	VK_INIT_FUNC(instance, vkMapMemory);
	VK_INIT_FUNC(instance, vkUnmapMemory);
	VK_INIT_FUNC(instance, vkBeginCommandBuffer);
	VK_INIT_FUNC(instance, vkEndCommandBuffer);
	VK_INIT_FUNC(instance, vkWaitForFences);
	VK_INIT_FUNC(instance, vkResetCommandPool);
	VK_INIT_FUNC(instance, vkResetFences);
	VK_INIT_FUNC(instance, vkDestroyDevice);
	return error::ok;
}



uint vulkan_debug_callback(flags32 severity, flags32 type, const VkDebugUtilsMessengerCallbackDataEXT* data, void* user_data)
{
	print(data->pMessage);
	return 0;
}
inline vulkan::memory_chunk request_memory(vulkan* vk, VkMemoryRequirements requirements, flags32 flags)
{
	uint type_index = UINTMAX;
	VkPhysicalDeviceMemoryProperties properties;
	vk->vkGetPhysicalDeviceMemoryProperties(vk->physical_device, &properties);
	iterate(i, properties.memoryTypeCount)
	{
		if ((requirements.memoryTypeBits & (1 << i)) && (properties.memoryTypes[i].propertyFlags & flags) == flags)
		{
			type_index = i;
			break;
		}
	}
	
	var pool = vk->memory_pools + type_index;
	if (pool->capacity == 0)
	{
		*pool = {};
		iterate(i, 2)
		{
			ulong capacity = VULKAN_MEMORY_CHUNK_CAPACITY / (1 << i);
			VkMemoryAllocateInfo memoryAllocInfo = {};
			memoryAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
			memoryAllocInfo.pNext = null;
			memoryAllocInfo.allocationSize = capacity;
			memoryAllocInfo.memoryTypeIndex = type_index;
			
			VkPtr memory;
			VkResult result = vk->vkAllocateMemory(vk->device, &memoryAllocInfo, null, &memory);
			if (result == VK_SUCCESS)
			{
				pool->memory = memory;
				pool->capacity = capacity;
				break;
			}
		}

		assert(pool->capacity);
	}

	ulong aligned_offset = ALIGN_POW2(pool->length, requirements.alignment);
	ulong aligned_size   = ALIGN_POW2(requirements.size, requirements.alignment);
	assert(aligned_size <= pool->capacity - aligned_offset);
	pool->length = aligned_offset + aligned_size;

	vulkan::memory_chunk chunk;
	chunk.memory = pool->memory;
	chunk.offset = aligned_offset;
	return chunk;
}
inline vulkan::buffer create_buffer(vulkan* vk, ulong size, flags32 usage, flags32 flags)
{
	VkResult result;
	VkPtr buffer = null;
	{
		VkBufferCreateInfo bufCreateInfo = {};
		bufCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufCreateInfo.size = size;
		bufCreateInfo.usage = usage;
		bufCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		result = vk->vkCreateBuffer(vk->device, &bufCreateInfo, null, &buffer);
		assert(result == VK_SUCCESS);
	}

	VkMemoryRequirements requirements;
	vk->vkGetBufferMemoryRequirements(vk->device, buffer, &requirements);
	var chunk = request_memory(vk, requirements, flags);
	vk->vkBindBufferMemory(vk->device, buffer, chunk.memory, chunk.offset);

	vulkan::buffer b;
	b.buffer = buffer;
	b.chunk = chunk;
	b.capacity = size;
	b.length = 0;
	b.requirements = requirements;
	return b;
}
inline ulong push(vulkan* vk, vulkan::buffer* buffer, void* data, ulong length)
{
	var offset = buffer->length;
	var chunk = buffer->chunk;
	void* memory;
	VkResult result = vk->vkMapMemory(vk->device, chunk.memory, chunk.offset + offset, length, 0, &memory);
	assert(result == VK_SUCCESS);
	vk->vkUnmapMemory(vk->device, chunk.memory);
	buffer->length += length;
	return offset;
}

inline VkPtr create_image_view(vulkan* vk, VkPtr image, VkFormat format, flags32 aspect_flags, VkImageViewType type)
{
	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.image = image;
	viewInfo.viewType = type;
	viewInfo.format = format;
	viewInfo.subresourceRange.aspectMask = aspect_flags;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = 1;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;

	VkPtr view {};
	VkResult result = vk->vkCreateImageView(vk->device, &viewInfo, null, &view);
	assert(result == VK_SUCCESS);
	return view;
}
inline vulkan::image create_image(vulkan* vk, uint3 extent, VkFormat format, VkImageTiling tiling, flags32 usage, flags32 flags, VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT)
{
	VkPtr image;
	VkImageCreateInfo imageInfo{};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = extent.y <= 1 
		? VK_IMAGE_TYPE_1D
		: (extent.z <= 1 ? VK_IMAGE_TYPE_2D : VK_IMAGE_TYPE_3D);
	imageInfo.extent = extent;
	if (extent.z == 0)
		imageInfo.extent.z = 1;
	imageInfo.mipLevels = 1;
	imageInfo.arrayLayers = 1;
	imageInfo.format = format;
	imageInfo.tiling = tiling;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;
	imageInfo.usage = usage;
	imageInfo.samples = samples;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	
	VkImageFormatProperties properties;
	vk->vkGetPhysicalDeviceImageFormatProperties(vk->physical_device, format, imageInfo.imageType, tiling, imageInfo.usage, imageInfo.flags, &properties);
	VkResult result = vk->vkCreateImage(vk->device, &imageInfo, null, &image);
	assert(result == VK_SUCCESS);

	VkMemoryRequirements memory_requirements;
	vk->vkGetImageMemoryRequirements(vk->device, image, &memory_requirements);
	var chunk = request_memory(vk, memory_requirements, flags);
	vk->vkBindImageMemory(vk->device, image, chunk.memory, chunk.offset);

	vulkan::image i;
	i.info = imageInfo;
	i.image = image;
	i.chunk = chunk;
	return i;
}
inline VkPtr descriptor_of(vulkan* vk, VkDescriptorType type)
{
	switch (type)
	{
		case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER: return vk->descriptor_combined_image_sampler;
		case VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT: return vk->descriptor_input_attachment;
		case VK_DESCRIPTOR_TYPE_STORAGE_IMAGE: return vk->descriptor_storage_image;
	}
	return null;
}
inline VkPtr create_descriptor(vulkan* vk, VkPtr view, VkPtr previous, VkDescriptorType layout)
{
	VkPtr set = previous;
	if (!set)
	{
		var descriptor = descriptor_of(vk, layout);
		VkDescriptorSetAllocateInfo info {};
		info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		info.descriptorPool = vk->descriptor_pool;
		info.descriptorSetCount = 1;
		info.pSetLayouts = &descriptor;
		VkResult result = vk->vkAllocateDescriptorSets(vk->device, &info, &set);
		assert(result == VK_SUCCESS);
	}
	
	{
		VkDescriptorImageInfo info {};
		info.imageView = view;
		info.imageLayout = layout == VK_DESCRIPTOR_TYPE_STORAGE_IMAGE
			? VK_IMAGE_LAYOUT_GENERAL
			: VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		info.sampler = layout == VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
			? vk->sampler
			: null;

		constexpr int write_count = 1;
		VkWriteDescriptorSet writes[write_count] {};
		writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writes[0].dstSet = set;
		writes[0].dstBinding = 0;
		writes[0].dstArrayElement = 0;
		writes[0].descriptorType = layout;
		writes[0].descriptorCount = 1;
		writes[0].pImageInfo = &info;

		vk->vkUpdateDescriptorSets(vk->device, write_count, writes, 0, null);
	}

	return set;
}
inline vulkan::render_target create_render_target(vulkan* vk, uint2 extent, VkFormat format, flags32 usage, flags32 aspect = VK_IMAGE_ASPECT_COLOR_BIT, VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT)
{
	var image = create_image(vk, {extent.x, extent.y, 0}, format, VK_IMAGE_TILING_OPTIMAL, usage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, samples); 
	var view = create_image_view(vk, image.image, format, aspect, VK_IMAGE_VIEW_TYPE_2D);

	vulkan::render_target result {image, format, view, 0,0,0};
	if (usage & VK_IMAGE_USAGE_SAMPLED_BIT)
		result.combined_image_sampler = create_descriptor(vk, view, null, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER);
	if (usage & VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT)
		result.combined_image_sampler = create_descriptor(vk, view, null, VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT);
	if (usage & VK_IMAGE_USAGE_STORAGE_BIT)
		result.combined_image_sampler = create_descriptor(vk, view, null, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE);

	return result;
}

VkPipelineShaderStageCreateInfo create_stage(const char* shader_path, VkShaderStageFlagBits stage)
{


}



void dispose(vulkan* vk)
{
	var instance = vk->instance;
	if (instance && vk->debug_messenger)
	{
		VkResult (*vkDestroyDebugUtilsMessengerEXT)(void*, VkPtr, void*);
		reinterpret_cast<void*&>(vkDestroyDebugUtilsMessengerEXT) = vk->func_of_instance(instance, "vkDestroyDebugUtilsMessengerEXT");
		if (vkDestroyDebugUtilsMessengerEXT)
			vkDestroyDebugUtilsMessengerEXT(instance, vk->debug_messenger, null);
	}
	
	dispose_platform(vk);
}

error init_renderer(vulkan* vk)
{
	VkResult vk_result;
	struct error err {};
	
	{
		VkViewport viewport{};
		VkRect2D viewport_rect{};
		
		var extent = vk->swapchain.extent;
		viewport.x = 0;
		viewport.y = 0;
		viewport.width = extent.x;
		viewport.height = extent.y;
		viewport.minDepth = 0;
		viewport.maxDepth = 1;

		viewport_rect.offset = {0, 0};
		viewport_rect.extent = extent;

		vk->fullscreen = viewport;
		vk->fullscreen_rect = viewport_rect;
	}

	{
		VkPtr descriptorPool {};
		VkDescriptorPoolSize poolSizes[] = {
			{VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, VULKAN_MAX_TEXTURE_COUNT},
			{VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VULKAN_MAX_TEXTURE_COUNT},
			{VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VULKAN_MAX_TEXTURE_COUNT},
			{VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, VULKAN_MAX_TEXTURE_COUNT}
		};

		VkDescriptorPoolCreateInfo poolInfo {};
		poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		poolInfo.poolSizeCount = len(poolSizes);
		poolInfo.pPoolSizes = poolSizes;
		poolInfo.maxSets = VULKAN_MAX_TEXTURE_COUNT * 2 + 1;

		if (vk_result = vk->vkCreateDescriptorPool(vk->device, &poolInfo, null, &descriptorPool))
			return make_error(VkResultToString(vk_result));
		
		vk->descriptor_pool = descriptorPool;
	}

	{
		VkPtr descriptorSetLayout {};
		VkDescriptorSetLayoutBinding layoutBinding {};
		layoutBinding.binding = 0;
		layoutBinding.descriptorCount = 1;
		layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
		layoutBinding.pImmutableSamplers = null;
		layoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

		VkDescriptorSetLayoutBinding bindings[] = {layoutBinding};

		VkDescriptorSetLayoutCreateInfo layoutInfo = {};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = len(bindings);
		layoutInfo.pBindings = bindings;

		if (vk_result = vk->vkCreateDescriptorSetLayout(vk->device, &layoutInfo, null, &descriptorSetLayout))
			return make_error(VkResultToString(vk_result));

		vk->descriptor_input_attachment = descriptorSetLayout;
	}

	{
		VkPtr descriptorSetLayout {};
		VkDescriptorSetLayoutBinding layoutBinding {};
		layoutBinding.binding = 0;
		layoutBinding.descriptorCount = 1;
		layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		layoutBinding.pImmutableSamplers = null;
		layoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

		VkDescriptorSetLayoutBinding bindings[] = {layoutBinding};

		VkDescriptorSetLayoutCreateInfo layoutInfo = {};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = len(bindings);
		layoutInfo.pBindings = bindings;

		if (vk_result = vk->vkCreateDescriptorSetLayout(vk->device, &layoutInfo, null, &descriptorSetLayout))
			return make_error(VkResultToString(vk_result));

		vk->descriptor_combined_image_sampler = descriptorSetLayout;
	}

	{
		VkPtr descriptorSetLayout {};
		VkDescriptorSetLayoutBinding layoutBinding {};
		layoutBinding.binding = 0;
		layoutBinding.descriptorCount = 1;
		layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
		layoutBinding.pImmutableSamplers = null;
		layoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

		VkDescriptorSetLayoutBinding bindings[] = {layoutBinding};

		VkDescriptorSetLayoutCreateInfo layoutInfo = {};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = len(bindings);
		layoutInfo.pBindings = bindings;

		if (vk_result = vk->vkCreateDescriptorSetLayout(vk->device, &layoutInfo, null, &descriptorSetLayout))
			return make_error(VkResultToString(vk_result));

		vk->descriptor_storage_image = descriptorSetLayout;
	}

	{
		VkPtr descriptorSetLayout {};
		VkDescriptorSetLayoutBinding layoutBinding {};
		layoutBinding.binding = 0;
		layoutBinding.descriptorCount = 1;
		layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		layoutBinding.pImmutableSamplers = null;
		layoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

		VkDescriptorSetLayoutBinding bindings[] = {layoutBinding};

		VkDescriptorSetLayoutCreateInfo layoutInfo = {};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = len(bindings);
		layoutInfo.pBindings = bindings;

		if (vk_result = vk->vkCreateDescriptorSetLayout(vk->device, &layoutInfo, null, &descriptorSetLayout))
			return make_error(VkResultToString(vk_result));

		vk->descriptor_dynamic_uniform_buffer_object = descriptorSetLayout;
	}

	{
		VkSamplerCreateInfo samplerInfo = {};
		samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerInfo.magFilter = VK_FILTER_LINEAR;
		samplerInfo.minFilter = VK_FILTER_LINEAR;
		samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;

		samplerInfo.anisotropyEnable = VK_TRUE;
		samplerInfo.maxAnisotropy = 16;
		samplerInfo.borderColor = VK_BORDER_COLOR_INT_TRANSPARENT_BLACK;
		samplerInfo.unnormalizedCoordinates = VK_FALSE;
		samplerInfo.compareEnable = VK_FALSE;
		samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
		samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		samplerInfo.mipLodBias = 0.0f;
		samplerInfo.minLod = 0.0f;
		samplerInfo.maxLod = 0.0f;


		VkPtr sampler = {};
		if (vk_result = vk->vkCreateSampler(vk->device, &samplerInfo, null, &sampler))
			return make_error(VkResultToString(vk_result));
		vk->sampler = sampler;
	}

	VkSampleCountFlagBits max_samples = {};
	{
		VkPhysicalDeviceProperties properties;
		vk->vkGetPhysicalDeviceProperties(vk->physical_device, &properties);
		var colors = properties.limits.framebufferColorSampleCounts;
		var depths = properties.limits.framebufferDepthSampleCounts;
		var bits = min(colors, depths);
		if (bits & VK_SAMPLE_COUNT_64_BIT) max_samples = VK_SAMPLE_COUNT_64_BIT;
		else if (bits & VK_SAMPLE_COUNT_64_BIT) max_samples = VK_SAMPLE_COUNT_64_BIT;
		else if (bits & VK_SAMPLE_COUNT_32_BIT) max_samples = VK_SAMPLE_COUNT_32_BIT;
		else if (bits & VK_SAMPLE_COUNT_16_BIT) max_samples = VK_SAMPLE_COUNT_16_BIT;
		else if (bits & VK_SAMPLE_COUNT_8_BIT) max_samples = VK_SAMPLE_COUNT_8_BIT;
		else if (bits & VK_SAMPLE_COUNT_4_BIT) max_samples = VK_SAMPLE_COUNT_4_BIT;
		else if (bits & VK_SAMPLE_COUNT_2_BIT) max_samples = VK_SAMPLE_COUNT_2_BIT;
		else if (bits & VK_SAMPLE_COUNT_1_BIT) max_samples = VK_SAMPLE_COUNT_1_BIT;

		var extent = vk->fullscreen_rect.extent;
		vk->rt_color_atc   = create_render_target(vk, extent, VK_FORMAT_D32_SFLOAT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_IMAGE_ASPECT_DEPTH_BIT);
		vk->rt_zbuffer     = create_render_target(vk, extent, VK_FORMAT_D32_SFLOAT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT, VK_IMAGE_ASPECT_DEPTH_BIT, max_samples);
		vk->rt_zbuffer_atc = create_render_target(vk, extent, vk->swapchain.format, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT, VK_IMAGE_ASPECT_COLOR_BIT, max_samples);
	}





	// CREATE PIPELINE
	{
		VkPipelineColorBlendAttachmentState attachment = {};
		attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		attachment.blendEnable = VK_TRUE;
		attachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
		attachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
		attachment.colorBlendOp = VK_BLEND_OP_ADD;
		attachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
		attachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
		attachment.alphaBlendOp = VK_BLEND_OP_ADD;
		vk->pipeline.color_blend_attachment_premultiplied_alpha = attachment;
		
		VkPipelineColorBlendStateCreateInfo colorBlendState = {};
		colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		colorBlendState.attachmentCount = 1;
		colorBlendState.pAttachments = &vk->pipeline.color_blend_attachment_premultiplied_alpha;
		vk->pipeline.color_blend_premultiplied_alpha = colorBlendState;
	}

	{
		VkPipelineColorBlendAttachmentState attachment = {};
		attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		vk->pipeline.color_blend_attachment_none = attachment;

		VkPipelineColorBlendStateCreateInfo colorBlendState = {};
		colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		colorBlendState.attachmentCount = 1;
		colorBlendState.pAttachments = &vk->pipeline.color_blend_attachment_none;
		vk->pipeline.color_blend_none = colorBlendState;
	}

	{
		VkPipelineDepthStencilStateCreateInfo depthStencil = {};
		depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		depthStencil.depthTestEnable = VK_TRUE;
		depthStencil.depthWriteEnable = VK_TRUE;
		depthStencil.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
		vk->pipeline.zbuffer = depthStencil;
	}

	{
		VkPipelineDepthStencilStateCreateInfo depthStencil = {};
		depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		depthStencil.depthTestEnable = VK_TRUE;
		depthStencil.depthWriteEnable = VK_FALSE;
		depthStencil.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
		vk->pipeline.zbuffer_test = depthStencil;
	}

	{
		// default vertex
		{
			VkVertexInputBindingDescription binding {};
			binding.binding = 0;
			binding.stride = sizeof(vertex);
			binding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
			vk->pipeline.vertex_in.binding = binding;

			VkVertexInputAttributeDescription a_position {};
			a_position.binding = 0;
			a_position.location = 0;
			a_position.format = VK_FORMAT_R32G32B32A32_SFLOAT;
			a_position.offset = offsetof(vertex, position);
			vk->pipeline.vertex_in.descriptions[0] = a_position;

			VkVertexInputAttributeDescription a_tangent {};
			a_tangent.binding = 0;
			a_tangent.location = 1;
			a_tangent.format = VK_FORMAT_R32G32B32A32_SFLOAT;
			a_tangent.offset = offsetof(vertex, tangent);
			vk->pipeline.vertex_in.descriptions[1] = a_tangent;

			VkVertexInputAttributeDescription a_normal {};
			a_normal.binding = 0;
			a_normal.location = 2;
			a_normal.format = VK_FORMAT_R32G32B32_SFLOAT;
			a_normal.offset = offsetof(vertex, normal);
			vk->pipeline.vertex_in.descriptions[2] = a_normal;

			VkVertexInputAttributeDescription a_uv0 {};
			a_uv0.binding = 0;
			a_uv0.location = 3;
			a_uv0.format = VK_FORMAT_R32G32_SFLOAT;
			a_uv0.offset = offsetof(vertex, uv0);
			vk->pipeline.vertex_in.descriptions[3] = a_uv0;

			VkVertexInputAttributeDescription a_uv1 {};
			a_uv1.binding = 0;
			a_uv1.location = 4;
			a_uv1.format = VK_FORMAT_R32G32_SFLOAT;
			a_uv1.offset = offsetof(vertex, uv1);
			vk->pipeline.vertex_in.descriptions[4] = a_uv1;

			VkVertexInputAttributeDescription a_color {};
			a_color.binding = 0;
			a_color.location = 5;
			a_color.format = VK_FORMAT_R32G32B32A32_SFLOAT; // if uint -> VK_FORMAT_R8G8B8A8_UNORM
			a_color.offset = offsetof(vertex, color);
			vk->pipeline.vertex_in.descriptions[5] = a_color;

			VkPipelineVertexInputStateCreateInfo info {};
			info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
			info.vertexBindingDescriptionCount = 1;
			info.pVertexBindingDescriptions = &vk->pipeline.vertex_in.binding;
			info.vertexAttributeDescriptionCount = 6;
			info.pVertexAttributeDescriptions = vk->pipeline.vertex_in.descriptions;
			vk->pipeline.vertex_in.info = info;
		}
	}

	{
		VkPipelineInputAssemblyStateCreateInfo assembly {};
		assembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		assembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		assembly.primitiveRestartEnable = VK_FALSE;
		vk->pipeline.triangle_assembly = assembly;
	}

	{
		VkPipelineViewportStateCreateInfo fullscreen {};
		fullscreen.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		fullscreen.viewportCount = 1;
		fullscreen.pViewports = &vk->fullscreen;
		fullscreen.scissorCount = 1;
		fullscreen.pScissors = &vk->fullscreen_rect;
		vk->pipeline.fullscreen = fullscreen;
	}

	{
		VkPipelineRasterizationStateCreateInfo rasterization;
		rasterization.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterization.depthClampEnable = VK_FALSE;
		rasterization.rasterizerDiscardEnable = VK_FALSE;
		rasterization.polygonMode = VK_POLYGON_MODE_FILL;
		rasterization.lineWidth = 1.0f;
		rasterization.frontFace = VK_FRONT_FACE_CLOCKWISE;
		rasterization.depthBiasEnable = VK_FALSE;
		rasterization.depthBiasConstantFactor = 0.0f;
		rasterization.depthBiasClamp = 0.0f;
		rasterization.depthBiasSlopeFactor = 0.0f;

		var rasterization_cull_none = rasterization;
		rasterization_cull_none.cullMode = VK_CULL_MODE_NONE;
		vk->pipeline.rasterization_cull_none = rasterization_cull_none;

		var rasterization_cull_back = rasterization;
		rasterization_cull_back.cullMode = VK_CULL_MODE_BACK_BIT;
		vk->pipeline.rasterization_cull_back = rasterization_cull_back;

		var rasterization_cull_front = rasterization;
		rasterization_cull_front.cullMode = VK_CULL_MODE_FRONT_BIT;
		vk->pipeline.rasterization_cull_front = rasterization_cull_front;
	}

	{
		VkPipelineMultisampleStateCreateInfo multisample {};
		multisample.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		multisample.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
		vk->pipeline.multisample_none = multisample;

		multisample.rasterizationSamples = max_samples;
		multisample.sampleShadingEnable = VK_TRUE;
		multisample.minSampleShading = 1.0f / max_samples;
		multisample.pSampleMask = null;
		multisample.alphaToCoverageEnable = VK_TRUE;
		multisample.alphaToOneEnable = VK_TRUE;
		vk->pipeline.multisample_atc = multisample;
	}

	{
		VkPipelineDynamicStateCreateInfo dynamic_state {};
		VkDynamicState states[1] = {
			VK_DYNAMIC_STATE_SCISSOR,
		};

		dynamic_state.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
		dynamic_state.dynamicStateCount = 1;
		dynamic_state.pDynamicStates = states;
		vk->pipeline.scissor = dynamic_state;
	}

	{
		VkPtr layouts[1];
		layouts[0] = vk->descriptor_dynamic_uniform_buffer_object;

		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = vk->descriptor_pool;
		allocInfo.descriptorSetCount = 1;
		allocInfo.pSetLayouts = layouts;

		VkPtr descriptorSet;
		if (vk_result = vk->vkAllocateDescriptorSets(vk->device, &allocInfo, &descriptorSet))
			return make_error(VkResultToString(vk_result));
		
		VkDescriptorBufferInfo bufferInfo = {};
		bufferInfo.buffer = vk->uniform_buffer.buffer;
		bufferInfo.offset = 0;
		bufferInfo.range = sizeof(vulkan::ubo);

		VkWriteDescriptorSet descriptorWrite = {};
		descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrite.dstSet = descriptorSet;
		descriptorWrite.dstBinding = 0;
		descriptorWrite.dstArrayElement = 0;
		descriptorWrite.descriptorCount = 1;
		descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		descriptorWrite.pBufferInfo = &bufferInfo;
		descriptorWrite.pImageInfo = nullptr; // Optional
		descriptorWrite.pTexelBufferView = nullptr; // Optional

		VkWriteDescriptorSet descriptorWrites[1] = {
			descriptorWrite,
		};

		vk->vkUpdateDescriptorSets(vk->device, 1, descriptorWrites, 0, nullptr);
		vk->descriptorset_dynamic_ubo = descriptorSet;
	}

	{
		{
			VkAttachmentDescription description = {};
			description.format = vk->swapchain.format;
			description.samples = VK_SAMPLE_COUNT_1_BIT;
			description.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			description.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
			vk->ad_swapchain = description;
		}
		{
			VkAttachmentDescription description = {};
			description.format = vk->rt_zbuffer.format;
			description.samples = VK_SAMPLE_COUNT_1_BIT;
			description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;//VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;//VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			description.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;//VK_IMAGE_LAYOUT_UNDEFINED;
			vk->ad_zbuffer = description;
		}
		{
			VkAttachmentDescription description = {};
			description.format = vk->rt_zbuffer_atc.format;
			description.samples = vk->pipeline.multisample_atc.rasterizationSamples;
			description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			description.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			description.finalLayout = VK_IMAGE_LAYOUT_ATTACHMENT_OPTIMAL_KHR;//VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;//IDK???VK_IMAGE_LAYOUT_UNDEFINED;
			vk->ad_zbuffer_multisample_atc = description;
		}
		{
			VkAttachmentDescription description = {};
			description.format = vk->rt_color_atc.format;
			description.samples = vk->pipeline.multisample_atc.rasterizationSamples;
			description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			description.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			description.finalLayout = VK_IMAGE_LAYOUT_ATTACHMENT_OPTIMAL_KHR;//VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;//IDK???VK_IMAGE_LAYOUT_UNDEFINED;
			vk->ad_color_multisample_atc = description;
		}
	}

	{
		VkAttachmentReference colorAttachments[1] = {
			{0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL},
		};

		VkSubpassDescription subpass = {};
		subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpass.colorAttachmentCount = 1;
		subpass.pColorAttachments = colorAttachments;

		VkRenderPassCreateInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassInfo.attachmentCount = 1;
		renderPassInfo.pAttachments = &vk->ad_swapchain;
		renderPassInfo.subpassCount = 1;
		renderPassInfo.pSubpasses = &subpass;

		VkPtr renderPass;
		if (vk_result = vk->vkCreateRenderPass(vk->device, &renderPassInfo, nullptr, &renderPass))
			return make_error(VkResultToString(vk_result));
		vk->pass.swapchain = renderPass;
	}

	{
		VkAttachmentReference colorAttachments[1] = {
			{0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL},
		};

		VkAttachmentReference depthAttachment = {
			1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
		};

		VkSubpassDescription subpass = {};
		subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpass.colorAttachmentCount = 1;
		subpass.pColorAttachments = colorAttachments;
		subpass.pDepthStencilAttachment = &depthAttachment;

		VkAttachmentDescription attachments[2] = {
			vk->ad_swapchain,
			vk->ad_zbuffer
		};

		VkRenderPassCreateInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassInfo.attachmentCount = 2;
		renderPassInfo.pAttachments = attachments;
		renderPassInfo.subpassCount = 1;
		renderPassInfo.pSubpasses = &subpass;

		VkPtr renderPass;
		if (vk_result = vk->vkCreateRenderPass(vk->device, &renderPassInfo, nullptr, &renderPass))
			return make_error(VkResultToString(vk_result));
		vk->pass.swapchain_zbuffer = renderPass;
	}

	print("AAA");
	{
		VkAttachmentReference colorAttachments[1] = {
			{0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL},
		};

		VkAttachmentReference depthAttachment[1] = {
			{2, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL},
		};

		VkAttachmentReference resolveAttachments[1] = {
			{1, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL},
		};

		VkSubpassDescription subpass = {};
		subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpass.colorAttachmentCount = 1;
		subpass.pColorAttachments = colorAttachments;
		subpass.pDepthStencilAttachment = depthAttachment;
		subpass.pResolveAttachments = resolveAttachments;

		VkAttachmentDescription attachments[3] = {
			vk->ad_color_multisample_atc,
			vk->ad_swapchain,
			vk->ad_zbuffer_multisample_atc,
		};

		VkRenderPassCreateInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassInfo.attachmentCount = 3;
		renderPassInfo.pAttachments = attachments;
		renderPassInfo.subpassCount = 1;
		renderPassInfo.pSubpasses = &subpass;

		//VkPtr renderPass;
		//if (vk_result = vk->vkCreateRenderPass(vk->device, &renderPassInfo, nullptr, &renderPass))
			//return make_error(VkResultToString(vk_result));
		//vk->pass.swapchain_zbuffer_atc = renderPass;
	}

	print("BBB");
	{
		var fs = vk->fullscreen;
		for (uptr i = 0; i < vk->swapchain.length; i++)
		{
			{
				VkFramebufferCreateInfo framebufferInfo = {};
				framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
				framebufferInfo.renderPass = vk->pass.swapchain;
				framebufferInfo.attachmentCount = 1;
				framebufferInfo.pAttachments = &vk->swapchain.views[i];
				framebufferInfo.width = fs.width;
				framebufferInfo.height = fs.height;
				framebufferInfo.layers = 1;

				VkPtr framebuffer;
				if (vk_result = vk->vkCreateFramebuffer(vk->device, &framebufferInfo, nullptr, &framebuffer))
					return make_error(VkResultToString(vk_result));
				vk->framebuffer.swapchain[i] = framebuffer;
			}
			{
				VkPtr attachments[2] = {
					vk->swapchain.views[i],
					vk->rt_zbuffer.view,
				};
				VkFramebufferCreateInfo framebufferInfo = {};
				framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
				framebufferInfo.renderPass = vk->pass.swapchain_zbuffer;
				framebufferInfo.attachmentCount = 3;
				framebufferInfo.pAttachments = attachments;
				framebufferInfo.width = fs.width;
				framebufferInfo.height = fs.height;
				framebufferInfo.layers = 1;

				VkPtr framebuffer;
				if (vk_result = vk->vkCreateFramebuffer(vk->device, &framebufferInfo, nullptr, &framebuffer))
					return make_error(VkResultToString(vk_result));
				vk->framebuffer.swapchain_zbuffer[i] = framebuffer;
			}
			/*{
				VkPtr attachments[3] = {
					vk->rt_color_atc.view,
					vk->swapchain.views[i],
					vk->rt_zbuffer_atc.view,
				};

				VkFramebufferCreateInfo framebufferInfo = {};
				framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
				framebufferInfo.renderPass = vk->pass.swapchain_zbuffer_atc;
				framebufferInfo.attachmentCount = 3;
				framebufferInfo.pAttachments = attachments;
				framebufferInfo.width = fs.width;
				framebufferInfo.height = fs.height;
				framebufferInfo.layers = 1;

				VkPtr framebuffer;
				if (vk_result = vk->vkCreateFramebuffer(vk->device, &framebufferInfo, nullptr, &framebuffer))
					return make_error(VkResultToString(vk_result));
				vk->framebuffer.swapchain_zbuffer_atc[i] = framebuffer;
			}*/
			
		}
	}
	print("BBB");
	return err;
}


error create_pipeline(vulkan* vk)
{
	VkResult vk_result;
	struct error err;
	var pipeline = vk->pipelines + 0;
	if (pipeline->handle)
		return error::ok;

	VkPipelineShaderStageCreateInfo shaderStages[2] = {
		create_stage("vertex", VK_SHADER_STAGE_VERTEX_BIT),
		create_stage("frag", VK_SHADER_STAGE_FRAGMENT_BIT),
	};
	VkPtr descriptorset_layouts[1] = {
		vk->descriptor_dynamic_uniform_buffer_object,
	};
	VkPtr descriptorset_layouts_textured[2] = {
		vk->descriptor_dynamic_uniform_buffer_object,
		vk->descriptor_combined_image_sampler,
	};
	
	VkGraphicsPipelineCreateInfo pipelineInfo {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = 2;
	pipelineInfo.pStages = shaderStages;
	pipelineInfo.pInputAssemblyState = &vk->pipeline.triangle_assembly;
	pipelineInfo.pViewportState = &vk->pipeline.fullscreen;
	pipelineInfo.pRasterizationState = &vk->pipeline.rasterization_cull_none;
	pipelineInfo.pMultisampleState = &vk->pipeline.multisample_atc;
	pipelineInfo.pColorBlendState = &vk->pipeline.color_blend_none;
	pipelineInfo.pVertexInputState = &vk->pipeline.vertex_in.info;
	pipelineInfo.renderPass = vk->pass.swapchain_zbuffer; // swapchain_zbuffer_atc
	pipelineInfo.pDepthStencilState = &vk->pipeline.zbuffer;
	pipelineInfo.pDynamicState = &vk->pipeline.scissor;

	VkPipelineLayoutCreateInfo layout_info {};
	layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	layout_info.setLayoutCount = 1;
	layout_info.pSetLayouts = descriptorset_layouts;

	VkPtr pipeline_layout;
	if (vk_result = vk->vkCreatePipelineLayout(vk->device, &layout_info, nullptr, &pipeline_layout))
		return make_error(VkResultToString(vk_result));


	VkPtr pipeline_handle;
	if (vk_result = vk->vkCreateGraphicsPipelines(vk->device, nullptr, 1, &pipelineInfo, nullptr, &pipeline_handle))
		return make_error(VkResultToString(vk_result));


	pipeline->handle = pipeline_handle;
	pipeline->layout = pipeline_layout;
	return err;
}


error init(vulkan* vk, struct video* video)
{
	vk->pool.ensured_capacity = 1 * MEGABYTE;
	var pool = &vk->pool;
	
	VkResult vk_result;
	struct error err;
	if (err = init_platform(vk))
		return err;
	
	var func_of = vk->func_of_instance;
	
	// Global functions
	VK_INIT_FUNC(null, vkEnumerateInstanceExtensionProperties);
	VK_INIT_FUNC(null, vkEnumerateInstanceLayerProperties)
	VK_INIT_FUNC(null, vkCreateInstance);

	print("[VULKAN] Getting extension properties");
	int required_extension_count = 0;
	const char* required_extensions[8];
	int required_layer_count = 0;
	const char* required_layers[8];
	{
		uint available_extension_count;
		vk->vkEnumerateInstanceExtensionProperties(null, &available_extension_count, null);
		var ep_buffer = push(pool, sizeof(VkExtensionProperties) * available_extension_count);
		vk->vkEnumerateInstanceExtensionProperties(null, &available_extension_count, ep_buffer.data);
		vk->extension_properties.elements = (VkExtensionProperties*)ep_buffer.data;
		vk->extension_properties.length = (int)available_extension_count;
		
		uint available_layer_count;
		vk->vkEnumerateInstanceLayerProperties(&available_layer_count, null);
		var el_buffer = push(pool, sizeof(VkLayerProperties) * available_layer_count);
		vk->vkEnumerateInstanceLayerProperties(&available_layer_count, el_buffer.data);
		vk->layer_properties.elements = (VkLayerProperties*)el_buffer.data;
		vk->layer_properties.length = (int)available_layer_count;

		if (err = init_instance_properties(vk, required_extensions, &required_extension_count, required_layers, &required_layer_count))
			return err;
	
		#if SOLITUDE_DEBUG
		required_extensions[required_extension_count++] = "VK_EXT_debug_report";
		required_extensions[required_extension_count++] = "VK_EXT_debug_utils";
		required_layers[required_layer_count++] = "VK_LAYER_KHRONOS_validation";
		#endif
	

		// Validate
		iterate(i, required_extension_count)
		{
			var available = false;
			var target = required_extensions[i];
			var target_length = len(target);
			foreach(var extension, vk->extension_properties)
			{
				var extension_name = extension.extensionName;
				var extension_length = len(extension_name);
				if (target_length == extension_length && equals(target, extension_name, target_length))
				{
					available = true;
					break;
				}
			}
			if (!available)
				return make_error(target);
		}
		iterate(i, required_layer_count)
		{
			var available = false;
			var target = required_layers[i];
			var target_length = len(target);
			print(target);
			foreach(var layer, vk->layer_properties)
			{
				//print("Layer proeprty %v: %v", layer.layerName, layer.description);
				var layer_name = layer.layerName;
				var layer_length = len(layer_name);
				if (target_length == layer_length && equals(target, layer_name, target_length))
				{
					available = true;
					break;
				}
			}
			if (!available)
				return make_error(target);
		}
	}
	
	print("[VULKAN] Creating instance");
	void* instance;
	{
		// Setup application info
		VkApplicationInfo appInfo;
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pNext = null;
		appInfo.pApplicationName = SOLITUDE_APP_NAME;
		appInfo.applicationVersion = vk_version(0, 1, 0, 0);
		appInfo.pEngineName = "Solitude";
		appInfo.engineVersion = vk_version(0, 0, 1, 0);
		appInfo.apiVersion = vk_version(0, 1, 2, 0);
		

		// Create instance
		VkInstanceCreateInfo instanceInfo;
		instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		instanceInfo.pNext = null;
		instanceInfo.flags = 0;
		instanceInfo.pApplicationInfo = &appInfo;
		instanceInfo.enabledLayerCount = required_layer_count;
		instanceInfo.ppEnabledLayerNames = required_layers;
		instanceInfo.enabledExtensionCount = required_extension_count;
		instanceInfo.ppEnabledExtensionNames = required_extensions;

		if (vk_result = vk->vkCreateInstance(&instanceInfo, null, &instance))
			return make_error(VkResultToString(vk_result));
	
		#if SOLITUDE_DEBUG
		{
			print("[VULKAN] Enabling debug callbacks");
			VkDebugUtilsMessengerCreateInfoEXT debugInfo{};
			debugInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
			debugInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
			debugInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
			debugInfo.pfnUserCallback = &vulkan_debug_callback;
			debugInfo.pUserData = vk;
			
			VkResult (*vkCreateDebugUtilsMessengerEXT)(void*, void*, void*, VkPtr);
			VK_LOCAL_FUNC(instance, vkCreateDebugUtilsMessengerEXT)
			if (vk_result = vkCreateDebugUtilsMessengerEXT(instance, &debugInfo, null, &vk->debug_messenger))
				return make_error(VkResultToString(vk_result));
		}
		#endif

		vk->instance = instance;
	}
	
	if (err = init_vulkan_procedures(vk))
		return err;
	
	print("[VULKAN] Creating surface");
	VkPtr surface;
	if (err = init_surface(vk, video, &surface))
		return err;
	


	// Instance is created




	print("[VULKAN] Finding all graphic devices and selecting the best one");
	void* physicalDevice = null;
	VkPhysicalDeviceProperties       physicalDeviceProperties;
	VkPhysicalDeviceFeatures         physicalDeviceFeatures;
	VkPhysicalDeviceMemoryProperties physicalDeviceMemory;
	{
		// TODO: Get the required physicalDeviceFeatures
		// and compare it to the ones the device has.


		uint device_count;
		void* devices[8];
		vk->vkEnumeratePhysicalDevices(instance, &device_count, null);
		vk->vkEnumeratePhysicalDevices(instance, &device_count, devices);
		iterate(i, device_count)
		{
			VkPhysicalDeviceProperties props;
			vk->vkGetPhysicalDeviceProperties(devices[i], &props);
			if (physicalDevice == null || physicalDeviceProperties.apiVersion < props.apiVersion)
			{
				physicalDevice = devices[i];
				physicalDeviceProperties = props;
			}
		}

		if (physicalDevice == null)
			return make_error("No suitable device found");

		vk->vkGetPhysicalDeviceFeatures(physicalDevice, &physicalDeviceFeatures);
		vk->vkGetPhysicalDeviceMemoryProperties(physicalDevice, &physicalDeviceMemory);
		vk->physical_device = physicalDevice;
		
		print("[VULKAN] Selected graphics device: %v", physicalDeviceProperties.deviceName);
	}
	
	
	
	
	print("[VULKAN] Detecting queue family indices");
	uint graphicsQueueIndex;
	uint computeQueueIndex;
	uint transferQueueIndex;
	uint presentationQueueIndex;
	{
		uint family_count;
		VkQueueFamilyProperties families[16];
		vk->vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &family_count, null);
		vk->vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &family_count, families);

		graphicsQueueIndex = UINTMAX;
		computeQueueIndex = UINTMAX;
		transferQueueIndex = UINTMAX;
		presentationQueueIndex = UINTMAX;
		iterate(i, family_count)
		{
			var f = families[i];
			if (graphicsQueueIndex == UINTMAX && f.queueFlags & VK_QUEUE_GRAPHICS_BIT)
				graphicsQueueIndex = i;

			if (computeQueueIndex == UINTMAX && f.queueFlags & VK_QUEUE_COMPUTE_BIT && !(f.queueFlags & VK_QUEUE_GRAPHICS_BIT))
				computeQueueIndex = i;

			if (transferQueueIndex == UINTMAX && f.queueFlags & VK_QUEUE_TRANSFER_BIT && !(f.queueFlags & (VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT)))
				transferQueueIndex = i;

			uint supports_present;
			vk->vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &supports_present);
			if (presentationQueueIndex == U32MAX && supports_present)
				presentationQueueIndex = i;
		}

		if (graphicsQueueIndex == UINTMAX)
			return make_error("Could not find a graphics queue");

		if (presentationQueueIndex == UINTMAX)
			return make_error("Could not find a present queue");
		
		// When no awesome compute- and transfer queue are found
		// use a less awesome one.
		iterate(i, family_count)
		{
			var f = families[i];
			if (computeQueueIndex == U32MAX && f.queueFlags & VK_QUEUE_COMPUTE_BIT)
				computeQueueIndex = i;

			if (transferQueueIndex == U32MAX && f.queueFlags & VK_QUEUE_TRANSFER_BIT)
				transferQueueIndex = i;
		}
	}

	print("[VULKAN] Creating logical device");
	void* device;
	void* graphicsQueue;
	void* presentationQueue;
	{
		// Device queue
		constexpr u32 queueCount = 1;
		float queuePriorities[queueCount];
		queuePriorities[0] = 0.0f;

		u32 queueInfoCount = 1;
		VkDeviceQueueCreateInfo queueInfos[4];
		queueInfos[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueInfos[0].pNext = null;
		queueInfos[0].flags = 0;
		queueInfos[0].queueFamilyIndex = graphicsQueueIndex;
		queueInfos[0].queueCount = queueCount;
		queueInfos[0].pQueuePriorities = queuePriorities;

		if (presentationQueueIndex != graphicsQueueIndex)
		{
			queueInfos[queueInfoCount].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueInfos[queueInfoCount].pNext = null;
			queueInfos[queueInfoCount].flags = 0;
			queueInfos[queueInfoCount].queueFamilyIndex = presentationQueueIndex;
			queueInfos[queueInfoCount].queueCount = queueCount;
			queueInfos[queueInfoCount].pQueuePriorities = queuePriorities;
			queueInfoCount++;
		}

		// TODO: It feels like a sound idea to change the #if's with something else
		#if VULKAN_REQUIRE_COMPUTE
		if (computeQueueIndex != graphicsQueueIndex && computeQueueIndex != presentationQueueIndex)
		{
			queueInfos[queueInfoCount].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueInfos[queueInfoCount].pNext = null;
			queueInfos[queueInfoCount].flags = 0;
			queueInfos[queueInfoCount].queueFamilyIndex = computeQueueIndex;
			queueInfos[queueInfoCount].queueCount = queueCount;
			queueInfos[queueInfoCount].pQueuePriorities = queuePriorities;
			queueInfoCount++;
		}
		#endif

		#if VULKAN_REQUIRE_TRANSFER
		if (transferQueueIndex != graphicsQueueIndex && transferQueueIndex != presentationQueueIndex && transferQueueIndex != computeQueueIndex)
		{
			queueInfos[queueInfoCount].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueInfos[queueInfoCount].pNext = null;
			queueInfos[queueInfoCount].flags = 0;
			queueInfos[queueInfoCount].queueFamilyIndex = transferQueueIndex;
			queueInfos[queueInfoCount].queueCount = queueCount;
			queueInfos[queueInfoCount].pQueuePriorities = queuePriorities;
			queueInfoCount++;
		}
		#endif

		// Device extensions
		u32 extensionCount = 0;
		const char* extensions[8];
		extensions[extensionCount++] = "VK_KHR_swapchain";
		
		#if SOLITUDE_DEBUG
		extensions[extensionCount++] = "VK_EXT_debug_marker";
		#endif
		
		// TODO: This should be done at the engine level
		// not on platform level. Find a way to do this
		// better
		VkPhysicalDeviceFeatures deviceFeatures = {};
		deviceFeatures.shaderStorageImageExtendedFormats = true;
		deviceFeatures.samplerAnisotropy = true;
		
		// Create actual device
		VkDeviceCreateInfo deviceInfo;
		deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceInfo.pNext = null;
		deviceInfo.flags = 0;
		deviceInfo.queueCreateInfoCount = queueInfoCount;
		deviceInfo.pQueueCreateInfos = queueInfos;
		deviceInfo.enabledLayerCount = 0; // this was validationLayerCount if debug is not working this is the cause
		deviceInfo.ppEnabledLayerNames = null; // this was validationLayers if debug is not working this is the cause
		deviceInfo.enabledExtensionCount = extensionCount;
		deviceInfo.ppEnabledExtensionNames = extensions;
		deviceInfo.pEnabledFeatures = &deviceFeatures;

		if (vk_result = vk->vkCreateDevice(physicalDevice, &deviceInfo, null, &device))
			return make_error(VkResultToString(vk_result));

		vk->vkGetDeviceQueue(device, graphicsQueueIndex, 0, &graphicsQueue);
		vk->vkGetDeviceQueue(device, presentationQueueIndex, 0, &presentationQueue);
		vk->device = device;
		vk->graphics_queue = graphicsQueue;
		vk->presentation_queue = presentationQueue;
	}

	



	print("[VULKAN] Creating swapchain");
	VkSurfaceCapabilitiesKHR surfaceCapabilities;
	VkFormat swapchainFormat;
	VkColorSpaceKHR swapchainColorSpace;
	uint imageBufferCount;
	VkPtr swapchain;
	uint2 extent;
	{
		vk->vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &surfaceCapabilities);
		if (surfaceCapabilities.currentExtent.x == U32MAX)
		{
			// TODO: This
			//int w, h;
			//SDL_GetWindowSize(window, &w, &h);
			//SDL_Vulkan_GetDrawableSize(window, &w, &h);
			surfaceCapabilities.currentExtent.x = clamp(1920U, surfaceCapabilities.minImageExtent.x, surfaceCapabilities.maxImageExtent.x);
			surfaceCapabilities.currentExtent.y = clamp(1080U, surfaceCapabilities.minImageExtent.y, surfaceCapabilities.maxImageExtent.y);
			return make_error("Invalid surface capabilities extent; fallback not implemented.");
		}
		extent = surfaceCapabilities.currentExtent;

		// Find preferred surface format, if format_count is 1 and it is undefined: no format is preffered
		uint format_count;
		VkSurfaceFormatKHR formats[16];
		vk->vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &format_count, null);
		vk->vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &format_count, formats);
		
		swapchainFormat = formats[0].format;
		swapchainColorSpace = formats[0].colorSpace;
		for (var i = 1; i < format_count; i++)
		{
			var format = formats[i];
			if (format.format == VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
			{
				swapchainFormat = VK_FORMAT_B8G8R8A8_UNORM;
				swapchainColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
				break;
			}
		}
		if (!swapchainFormat)
			swapchainFormat = VK_FORMAT_B8G8R8A8_UNORM;
		
		// Find present mode
		uint mode_count;
		VkPresentModeKHR modes[16];
		vk->vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &mode_count, null);
		vk->vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &mode_count, modes);

		VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR;
		for (var i = 0; i < mode_count; i++)
		{
			if (modes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
				presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
				break;
			}
		}

		VkCompositeAlphaFlagBitsKHR surfaceComposite =
			(surfaceCapabilities.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR)
			? VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR
			: (surfaceCapabilities.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR)
			? VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR
			: (surfaceCapabilities.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR)
			? VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR
			: VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR;

		imageBufferCount = surfaceCapabilities.minImageCount + 1;
		if (surfaceCapabilities.maxImageCount > 0 && imageBufferCount > surfaceCapabilities.maxImageCount)
			imageBufferCount = surfaceCapabilities.maxImageCount;

		VkSwapchainCreateInfoKHR createInfo {};
		createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		createInfo.surface = surface;
		createInfo.minImageCount = imageBufferCount;
		createInfo.imageFormat = swapchainFormat;
		createInfo.imageColorSpace = swapchainColorSpace;
		createInfo.imageExtent = surfaceCapabilities.currentExtent;
		createInfo.imageArrayLayers = 1;
		createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_STORAGE_BIT;
		createInfo.preTransform = surfaceCapabilities.currentTransform;
		createInfo.compositeAlpha = surfaceComposite;
		createInfo.presentMode = presentMode;
		createInfo.clipped = true;

		if (graphicsQueue == presentationQueue)
		{
			createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
			createInfo.queueFamilyIndexCount = 0;
			createInfo.pQueueFamilyIndices = null;
		}
		else
		{
			u32 queueFamilyIndices[2] = { graphicsQueueIndex, presentationQueueIndex };
			createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
			createInfo.queueFamilyIndexCount = 2;
			createInfo.pQueueFamilyIndices = queueFamilyIndices;
		}

		if (vk_result = vk->vkCreateSwapchainKHR(device, &createInfo, 0, &swapchain))
			return make_error(VkResultToString(vk_result));

		vk->swapchain.handle = swapchain;
		vk->swapchain.extent = extent;
		vk->swapchain.format = swapchainFormat;
	}

	print("[VULKAN] Creating swapchain images");
	uint image_count = 0;
	{
		if (vk_result = vk->vkGetSwapchainImagesKHR(device, swapchain, &image_count, null))
			return make_error(VkResultToString(vk_result));
		
		assert(image_count <= len(vk->swapchain.images));
		if (vk_result = vk->vkGetSwapchainImagesKHR(device, swapchain, &image_count, vk->swapchain.images))
			return make_error(VkResultToString(vk_result));
		
		iterate(i, image_count)
		{
			print("create_image_view %v", i);
			VkPtr image = vk->swapchain.images[i];
			VkPtr* view = vk->swapchain.views + i;
			*view = create_image_view(vk, image, vk->swapchain.format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D);
		}
	}

	vk->uniform_buffer         = create_buffer(vk, 4 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	vk->uniform_buffer_staging = create_buffer(vk, 4 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	vk->vertex_buffer          = create_buffer(vk, 4 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	vk->vertex_buffer_staging  = create_buffer(vk, 4 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	vk->index_buffer           = create_buffer(vk, 4 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	vk->index_buffer_staging   = create_buffer(vk, 4 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);


	print("[VULKAN] Creating command pool");
	{
		VkCommandPoolCreateInfo cmdpoolInfo = {};
		cmdpoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		cmdpoolInfo.pNext = null;
		cmdpoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
		cmdpoolInfo.queueFamilyIndex = graphicsQueueIndex;
		if (vk_result = vk->vkCreateCommandPool(device, &cmdpoolInfo, null, &vk->command_pool))
			return make_error(VkResultToString(vk_result));

		VkCommandBufferAllocateInfo cmdBufferInfo = {};
		cmdBufferInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		cmdBufferInfo.commandPool = vk->command_pool;
		cmdBufferInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		cmdBufferInfo.commandBufferCount = imageBufferCount;
		if (vk_result = vk->vkAllocateCommandBuffers(device, &cmdBufferInfo, vk->command_buffers))
			return make_error(VkResultToString(vk_result));

	}



	print("[VULKAN] Creating synchronization objects");
	// init ImageSemaphores, RenderSemaphores, ImageFences and RenderFences
	{
		VkSemaphoreCreateInfo semaphoreCreateInfo {};
		semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

		if (vk_result = vk->vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, &vk->request_semaphore))
			return make_error(VkResultToString(vk_result));

		if (vk_result = vk->vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, &vk->present_semaphore))
			return make_error(VkResultToString(vk_result));


		VkFenceCreateInfo fenceCreateInfo {};
		fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

		iterate(i, image_count)
		{
			var fence = vk->swapchain.fences + i;
			if (vk_result = vk->vkCreateFence(device, &fenceCreateInfo, nullptr, fence))
				return make_error(VkResultToString(vk_result));
		}
	}


	
	print("[VULKAN] Creating timestamp query pool");
	{
		VkPtr query_pool = null;
		VkQueryPoolCreateInfo create_info{};
		create_info.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
		create_info.queryType = VK_QUERY_TYPE_TIMESTAMP;
		create_info.queryCount = 2;
		if (vk_result = vk->vkCreateQueryPool(device, &create_info, nullptr, &query_pool))
			return make_error(VkResultToString(vk_result));
		vk->timestamp_query_pool = query_pool;
	}



	{
		VkCommandBufferBeginInfo info {};
		info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
		info.pInheritanceInfo = nullptr;

		vk->staging_buffer = create_buffer(vk, 64 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		vk->command_buffer = vk->command_buffers[0];
		if (vk_result = vk->vkBeginCommandBuffer(vk->command_buffer, &info))
			return make_error(VkResultToString(vk_result));
	}

	if (err = init_renderer(vk))
		return err;

	if (err = create_pipeline(vk))
		return err;
	// TODO: Create white, magenta, black....
	//
	

	{
		if (vk_result = vk->vkEndCommandBuffer(vk->command_buffer))
			return make_error(VkResultToString(vk_result));

		VkSubmitInfo info{};
		info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		info.commandBufferCount = 1;
		info.pCommandBuffers = &vk->command_buffer;
		vk->vkQueueSubmit(vk->graphics_queue, 1, &info, null);
		vk->vkDeviceWaitIdle(vk->device);
	}

	return err;
}

void begin_frame(vulkan* vk)
{
	constexpr ulong timeout = 1000000000UL;
	VkResult vk_result;
	var device = vk->device;


	vk_result = vk->vkWaitForFences(device, 1, vk->swapchain.fences + vk->swapchain.index, VK_TRUE, timeout);
	assert(vk_result == VK_SUCCESS);

	vk_result = vk->vkResetCommandPool(vk->device, vk->command_pool, 0);
	assert(vk_result == VK_SUCCESS);

	vk_result = vk->vkAcquireNextImageKHR(vk->device, vk->swapchain.handle, timeout, vk->request_semaphore, null, &vk->swapchain.index);
	assert(vk_result == VK_SUCCESS);

	var index = vk->swapchain.index;

	{
		VkCommandBufferBeginInfo info {};
		info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
		info.pInheritanceInfo = nullptr;

		vk->command_buffer = vk->command_buffers[index];
		vk_result = vk->vkBeginCommandBuffer(vk->command_buffer, &info);
		assert(vk_result == VK_SUCCESS);

		// TODO: timestamp query pool
	}
	print("begin_frame %v", index);
}

void end_frame(vulkan* vk)
{
	VkResult vk_result;
	var device = vk->device;
	// TODO: timestamp query pool
	{
		vk_result = vk->vkEndCommandBuffer(vk->command_buffer);
		assert(vk_result == VK_SUCCESS);
	}

	var index = vk->swapchain.index;
	print("end_frame %v", index);
	// Submit
	{
		flags32 pipe_stage_flags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;//VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;

		VkSubmitInfo submitInfo {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = &vk->request_semaphore;
		submitInfo.pWaitDstStageMask = &pipe_stage_flags;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &vk->command_buffer;
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = &vk->present_semaphore;

		//vk->vkResetFences(device, 1, vk->swapchain.fences + vk->swapchain.index);
		vk_result = vk->vkQueueSubmit(vk->graphics_queue, 1, &submitInfo, null);//vk->swapchain.fences + vk->swapchain.index);
		assert(vk_result == VK_SUCCESS);

		VkPresentInfoKHR presentInfo{};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = &vk->present_semaphore;
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = &vk->swapchain.handle;
		presentInfo.pImageIndices = &vk->swapchain.index;
		//presentInfo.pResult = ...

		vk_result = vk->vkQueuePresentKHR(vk->presentation_queue, &presentInfo);
		assert(vk_result == VK_SUCCESS);
	}
	vk->vkDeviceWaitIdle(vk->device);

	// TODO: get timestamp data
}

