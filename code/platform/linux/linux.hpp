#define FNPTR(x) reinterpret_cast<void*&>(x)
#define LOAD_FNPTR(name) \
	reinterpret_cast<void*&>(name) = dlsym(handle, #name); \
	if (!name) { \
		dlclose(handle); \
		return make_error("Could not find " #name " using dlsym."); \
	}

struct 
{
	mutex memory_mutex;
	memory::block memory_sentinel;
	char executable_file_name[512];
} g_linux_state;


error init_platform()
{
	g_linux_state = {};
	g_linux_state.memory_sentinel._next = &g_linux_state.memory_sentinel;
	g_linux_state.memory_sentinel._previous = &g_linux_state.memory_sentinel;
	return error::ok;
}


memory::block* allocate(uptr size, flags32 flags)
{
	constexpr int mmap_protection = PROT_READ | PROT_WRITE;
	constexpr int mmap_flags = MAP_PRIVATE | MAP_ANONYMOUS;
	
	var offset = 64; //sizeof(memory::block);
	var total_size = size + offset;
	var block = (memory::block*)mmap(0, total_size, mmap_protection, mmap_flags, -1, 0);
	block->data = (byte*)block + offset;
	
	var sentinel = &g_linux_state.memory_sentinel;
	block->_next = sentinel;
	block->capacity = size;
	block->flags = flags;
	
	lock(&g_linux_state.memory_mutex);
	{
		block->_previous = sentinel->_previous;
		block->_previous->_next = block;
		block->_next->_previous = block;
	}
	unlock(&g_linux_state.memory_mutex);

	return block;
}

void deallocate(memory::block* block)
{
	var capacity = block->capacity;
	var total_size = capacity + 64;//sizeof(memory::block);
	
	lock(&g_linux_state.memory_mutex);
	{
		block->_previous->_next = block->_next;
		block->_next->_previous = block->_previous;
	}
	unlock(&g_linux_state.memory_mutex);
	munmap(block, total_size);
}

void error_popup(error err)
{
	//Video video {};
	//if (init(&video)) CRASH;

}

