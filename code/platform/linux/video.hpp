extern "C"
{
	#if 1 // Hint NVIDIA and AMD to run on GPU
	int NvOptimusEnablement = 1;
	int AmdPowerXpressRequestHighPerformance = 1;
	#endif
}

namespace x11
{

	static constexpr int NoEventMask = 0L;
	static constexpr int KeyPressMask = (1L<<0);
	static constexpr int KeyReleaseMask = (1L<<1);
	static constexpr int ButtonPressMask = (1L<<2);
	static constexpr int ButtonReleaseMask = (1L<<3);
	static constexpr int EnterWindowMask = (1L<<4);
	static constexpr int LeaveWindowMask = (1L<<5);
	static constexpr int PointerMotionMask = (1L<<6);
	static constexpr int PointerMotionHintMask = (1L<<7);
	static constexpr int Button1MotionMask = (1L<<8);
	static constexpr int Button2MotionMask = (1L<<9);
	static constexpr int Button3MotionMask = (1L<<10);
	static constexpr int Button4MotionMask = (1L<<11);
	static constexpr int Button5MotionMask = (1L<<12);
	static constexpr int ButtonMotionMask = (1L<<13);
	static constexpr int KeymapStateMask = (1L<<14);
	static constexpr int ExposureMask = (1L<<15);
	static constexpr int VisibilityChangeMask = (1L<<16);
	static constexpr int StructureNotifyMask = (1L<<17);
	static constexpr int ResizeRedirectMask = (1L<<18);
	static constexpr int SubstructureNotifyMask = (1L<<19);
	static constexpr int SubstructureRedirectMask = (1L<<20);
	static constexpr int FocusChangeMask = (1L<<21);
	static constexpr int PropertyChangeMask = (1L<<22);
	static constexpr int ColormapChangeMask = (1L<<23);
	static constexpr int OwnerGrabButtonMask = (1L<<24);


	union Event
	{
		static constexpr int KeyPress = 2;
		static constexpr int KeyRelease = 3;
		static constexpr int ButtonPress = 4;
		static constexpr int ButtonRelease = 5;
		static constexpr int MotionNotify = 6;
		static constexpr int EnterNotify = 7;
		static constexpr int LeaveNotify = 8;
		static constexpr int FocusIn = 9;
		static constexpr int FocusOut = 10;
		static constexpr int KeymapNotify = 11;
		static constexpr int Expose = 12;
		static constexpr int GraphicsExpose = 13;
		static constexpr int NoExpose = 14;
		static constexpr int VisibilityNotify = 15;
		static constexpr int CreateNotify = 16;
		static constexpr int DestroyNotify = 17;
		static constexpr int UnmapNotify = 18;
		static constexpr int MapNotify = 19;
		static constexpr int MapRequest = 20;
		static constexpr int ReparentNotify = 21;
		static constexpr int ConfigureNotify = 22;
		static constexpr int ConfigureRequest = 23;
		static constexpr int GravityNotify = 24;
		static constexpr int ResizeRequest = 25;
		static constexpr int CirculateNotify = 26;
		static constexpr int CirculateRequest = 27;
		static constexpr int PropertyNotify = 28;
		static constexpr int SelectionClear = 29;
		static constexpr int SelectionRequest = 30;
		static constexpr int SelectionNotify = 31;
		static constexpr int ColormapNotify = 32;
		static constexpr int ClientMessage = 33;
		static constexpr int MappingNotify = 34;
		static constexpr int GenericEvent = 35;
		static constexpr int LASTEvent = 36;

		int type;
		long padding[24];
	};
}


struct video
{
	void* libx11;
	uptr window;
	void* display;
	
	void(*next_event)(void*, x11::Event*);
	void(*close_display)(void*);
};

error init(video* v)
{
	void* (*XOpenDisplay)(char*);
	int   (*XDefaultScreen)(void*);
	uptr  (*XRootWindow)(void*, int);
	uptr  (*XBlackPixel)(void*, int);
	uptr  (*XWhitePixel)(void*, int);
	uptr  (*XCreateSimpleWindow)(void*, uptr, int, int, uint, uint, uint, uptr, uptr);
	void  (*XSelectInput)(void*, uptr, long);
	void  (*XMapWindow)(void*, uptr);

	var handle = dlopen("libX11.so", RTLD_NOW|RTLD_LOCAL);
	if (!handle)
	{
		handle = dlopen("libX11.so.6", RTLD_NOW|RTLD_LOCAL);
		if (!handle)	
			return make_error("Could not open libX11.so nor libX11.so.6 using dlopen with arguments RTLD_NOW|RTLD_LOCAL.");
	}

	LOAD_FNPTR(XOpenDisplay);
	LOAD_FNPTR(XDefaultScreen);
	LOAD_FNPTR(XRootWindow);
	LOAD_FNPTR(XBlackPixel);
	LOAD_FNPTR(XWhitePixel);
	LOAD_FNPTR(XCreateSimpleWindow);
	LOAD_FNPTR(XSelectInput);
	LOAD_FNPTR(XMapWindow);
	
	FNPTR(v->next_event) = dlsym(handle, "XNextEvent");
	if (!v->next_event)
	{
		dlclose(handle);
		return make_error("Could not find symbol XNextEvent in libX11 using dlsym.");
	}

	FNPTR(v->close_display) = dlsym(handle, "XCloseDisplay");
	if (!v->close_display)
	{
		dlclose(handle);
		return make_error("Could not find symbol XCloseDisplay in libX11 using dlsym.");
	}

	// TODO: replace null with user defined display	
	var display = XOpenDisplay(null);
	if (display == null)
		return make_error("Cannot open display using XOpenDisplay(null).");
	
	var screen = XDefaultScreen(display);
	var root = XRootWindow(display, screen);
	var black = XBlackPixel(display, screen);
	var white = XWhitePixel(display, screen);
	var window = XCreateSimpleWindow(display, root, 10, 10, 100, 100, 1, black, white);
	XSelectInput(display, window, x11::ExposureMask | x11::KeyPressMask);
	XMapWindow(display, window);
	
	v->libx11 = handle;
	v->window = window;
	v->display = display;
	return error::ok;
}

InputEvent poll(video* v)
{
	InputEvent result;
	x11::Event event;
	v->next_event(v->display, &event); // TODO: use a non-blocking alternative instead
	if (event.type == x11::Event::KeyPress)
	{
		result.type = InputEvent::Key;
	}
	else
	{
		result.type = 0;
	}
	return result;
}

void dispose(video* v)
{
	if (v->display)
	{
		v->close_display(v->display);
		v->display = null;
		v->close_display = null;
	}

	if (v->libx11)
	{
		dlclose(v->libx11);
		v->libx11 = null;
	}
}








