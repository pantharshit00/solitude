



/**
 * SDL: ~/projects/external/SDL2/src/...
 * ./SDL.c:148 (SDL_InitSubSystem is equivilent of SDL_Init)
 * :163 Events get initialized when using VIDEO
 * :185 Events get initialized by calling SDL_EventsInit
 *     ./events/SDL_events.c:1078 (seems to do initiale logging and event loop; more info needed)
 * :213 Video gets initialized by calling SDL_VideoInit
 *     ./video/SDL_video.c:469 (called with null as parameter)
 *     :496 (optional) driver name get determined by calling SDL_getenv
 *	       ./stdlib/SDL_getenv.c:174
 *     :60 bootstrap is defined here, this is a way to do cross platform stuff
 *         ./video/sysvideo.h:412 typedef VideoBootStrap
 *     :506 looping through all compiled video drivers and creating each of them until one succeeds.
 *         ./video/x11/SDL_x11video.c:288
 *         :145 X11_CreateDevice it returns a SDL_VideoDevice
 *             ./video/SDL_sysvideo.h definition of SDL_VideoDevice
 *         :152 SDL_X11_LoadSymbols
 *             ./video/x11/SDL_x11dyn.c:153
 *         :158 
 *         :161 
 *         
 *
 */
error init_platform(vulkan* vk)
{
	var handle = dlopen("libvulkan.so", RTLD_NOW|RTLD_LOCAL);
	if (!handle)
	{
		handle = dlopen("libvulkan.so.1", RTLD_NOW|RTLD_LOCAL);
		if (!handle)	
			return make_error("Could not open libvulkan.so nor libvulkan.so.1 using dlopen with arguments RTLD_NOW|RTLD_LOCAL.");
	}
	
	vulkan::get_proc_addr_t func_of_instance;
	FNPTR(func_of_instance) = dlsym(handle, "vkGetInstanceProcAddr");
	if (!func_of_instance)
	{
		FNPTR(func_of_instance) = dlsym(handle, "_vkGetInstanceProcAddr");
		if (!func_of_instance)
		{
			dlclose(handle);
			return make_error("Could not find symbol vkGetInstanceProcAddr nor _vkGetInstanceProcAddr using dlsym.");
		}
	}
	
	vulkan::get_proc_addr_t func_of_device;
	FNPTR(func_of_device) = dlsym(handle, "vkGetDeviceProcAddr");
	if (!func_of_device)
	{
		FNPTR(func_of_device) = dlsym(handle, "_vkGetDeviceProcAddr");
		if (!func_of_device)
		{
			dlclose(handle);
			return make_error("Could not find symbol vkGetInstanceProcAddr nor _vkGetInstanceProcAddr using dlsym.");
		}
	}
	
	vk->libvulkan = handle;
	vk->func_of_instance = func_of_instance;
	vk->func_of_device = func_of_device;
	return error::ok;
}

error init_instance_properties(vulkan* vk, const char* extensions[], int* extension_count, const char* layers[], int* layer_count)
{
	var count = *extension_count;
	extensions[count++] = "VK_KHR_surface";
	extensions[count++] = "VK_KHR_xlib_surface";

	var extension_properties = vk->extension_properties;
	foreach(var ep, extension_properties)
	{
		print("Extension Property (%v): %v", ep.specVersion, ep.extensionName);
	}

	*extension_count = count;
	return error::ok;
}

error init_surface(vulkan* vk, struct video* video, VkPtr* result)
{
	VkResult vk_result;
	struct {
		VkStructureType sType;
		const void* pNext;
		flags32 flags;
		void* dpy;
		uptr window;
	} info = {};
	info.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
	info.dpy = video->display;
	info.window = video->window;

	VkResult (*vkCreateXlibSurfaceKHR)(void*,const void*, const void*, void*);
	reinterpret_cast<void*&>(vkCreateXlibSurfaceKHR) = vk->func_of_instance(vk->instance, "vkCreateXlibSurfaceKHR");
	if (!vkCreateXlibSurfaceKHR)
		return make_error("Could not find vkCreateXlibSurfaceKHR.");

	if (vk_result = vkCreateXlibSurfaceKHR(vk->instance, &info, null, result))
		return make_error(VkResultToString(vk_result));
	
	return error::ok;
}

void dispose_platform(vulkan* vk)
{
	if (vk->libvulkan)
	{
		dlclose(vk->libvulkan);
		vk->libvulkan = null;
	}
}

