
// Limits
#define CHARMIN   (-128)
#define SBYTEMIN  (-128)
#define SHORTMIN  (-32768)
#define INTMIN    (-2147483648)
#define LONGMIN   (-9223372036854775808L)
#define FLOATMIN  (-3.402823466e+38F)

#define CHARMAX   127
#define SBYTEMAX  127
#define SHORTMAX  32767
#define INTMAX    2147483647
#define LONGMAX   9223372036854775807L
#define FLOATMAX  3.402823466e+38F

#define UCHARMAX  255
#define BYTEMAX   255
#define USHORTMAX 65535
#define UINTMAX   4294967295U
#define ULONGMAX  18446744073709551615UL

#define S8MIN  (-128)
#define S16MIN (-32768)
#define S32MIN (-2147483648)
#define S64MIN (-9223372036854775808L)
#define R32MIN (-3.402823466e+38F)

#define S8MAX  127
#define S16MAX 32767
#define S32MAX 2147483647
#define S64MAX 9223372036854775807L
#define R32MAX 3.402823466e+38F

#define U8MAX  255
#define U16MAX 65535
#define U32MAX 4294967295U
#define U64MAX 18446744073709551615UL

// Constants
#define EPSILON 0.00001f
#define PI      3.14159265358979323846f   // pi
#define TAU     6.28318530717958647692f   // 2pi
#define DEG2RAD 0.017453292519943295f     // pi/180
#define RAD2DEG 57.29577951308232f        // 180/pi
#define MATH_E  2.71828182845904523536f   // e (euler's number)
#define LOG2E   1.44269504088896340736f   // log2(e)
#define LOG10E  0.434294481903251827651f  // log10(e)
#define LN2     0.693147180559945309417f  // ln(2)
#define LN10    2.30258509299404568402f   // ln(10)
#define SQRT2   1.41421356237309504880f   // sqrt(2)

/**
 * TODO: If we wanted to use double precision floats,
 * we need to create additional macros directly after
 * the include: #define absd abs
 *
 * HACK: <cmath> can have global defined procedures.
 * This makes it impossible to use custom procedures
 * like acos(float2) acos(float). To cercumvent this
 * the functions are defined with macros.
 *     #define procedure proceduref
 *
 * If we wanted abs(double) we should #define absd abs
 * above the float macros.
 */
#include "math.h"
#define abs   fabs
#define acos  acosf
#define asin  asinf
#define atan  atanf
#define atan2 atan2f
#define ceil  ceilf
#define cbrt  cbrtf
#define cos   cosf
#define cosh  coshf
#define exp   expf
#define exp2  exp2f
#define floor floorf
#define fmod  fmodf
#define log   logf
#define log10 log10f
#define log2  log2f
#define modf  modff
#define pow   powf
#define round roundf
#define sin   sinf
#define sinh  sinhf
#define sqrt  sqrtf
#define tan   tanf
#define tanh  tanhf
#define trunc truncf

#define isinf isinff
#define isnan isnanf

inline float frac(float x)     { return x - floorf(x); }
inline float rsqrt(float x)    { return 1.0f / sqrtf(x); }
inline float saturate(float x) { return x < 0 ? 0 : x > 1 ? 1 : x; }
inline float radians(float x)  { return x * 57.29577951308232f; }
inline float degrees(float x)  { return x * 0.017453292519943295f; }






/**
 * 
 * 
 * 
 * UNTESTED:
 * refract
 * 
 * TODO FUNCIONS:
 * sign, sincos, wedge product, geometric product
 * 
 * IGNORED FUNCIONS:
 * frexp: float, float2, float3, float4
 * isfinite: float2, float3, float4
 * isinf: float2, float3, float4
 * isnan: float2, float3, float4
 * ldexpf: float, float2, float3, float4
 * step: float, float2, float3, float4
 * libc::erff
 * libc::fma
 */

#define PRIMES_COUNT 72
static const int primes[PRIMES_COUNT]{
	3, 7, 11, 17, 23, 29, 37, 47,
	59, 71, 89, 107, 131, 163, 197, 239,
	293, 353, 431, 521, 631, 761, 919, 1103,
	1327, 1597, 1931, 2333, 2801, 3371, 4049, 4861,
	5839, 7013, 8419, 10103, 12143, 14591, 17519, 21023,
	25229, 30293, 36353, 43627, 52361, 62851, 75431, 90523,
	108631, 130363, 156437, 187751, 225307, 270371, 324449, 389357,
	467237, 560689, 672827, 807403, 968897, 1162687, 1395263, 1674319,
	2009191, 2411033, 2893249, 3471899, 4166287, 4999559, 5999471, 7199369
};
inline int PrimeCeiled(int x)
{
	for (int i = 0; i < PRIMES_COUNT; i++)
	{
		int prime = primes[i];
		if (prime >= x)
			return prime;
	}

	assert(!"x is bigger the primes_max");
	return x | 1;
}




inline float2 operator - (float2 l) { return { -l.x, -l.y }; }
inline float3 operator - (float3 l) { return { -l.x, -l.y, -l.z }; }
inline float4 operator - (float4 l) { return { -l.x, -l.y, -l.z, -l.w }; }

inline float2 operator - (float2 l, float2 r) { return { l.x - r.x, l.y - r.y }; }
inline float3 operator - (float3 l, float3 r) { return { l.x - r.x, l.y - r.y, l.z - r.z }; }
inline float4 operator - (float4 l, float4 r) { return { l.x - r.x, l.y - r.y, l.z - r.z, l.w - r.w }; }
inline float2 operator - (float r, float2 l) { return { r - l.x, r - l.y }; }
inline float3 operator - (float r, float3 l) { return { r - l.x, r - l.y, r - l.z }; }
inline float4 operator - (float r, float4 l) { return { r - l.x, r - l.y, r - l.z, r - l.w }; }
inline float2 operator - (float2 l, float r) { return { l.x - r, l.y - r }; }
inline float3 operator - (float3 l, float r) { return { l.x - r, l.y - r, l.z - r }; }
inline float4 operator - (float4 l, float r) { return { l.x - r, l.y - r, l.z - r, l.w - r }; }

inline float2 operator + (float2 l, float2 r) { return { l.x + r.x, l.y + r.y }; }
inline float3 operator + (float3 l, float3 r) { return { l.x + r.x, l.y + r.y, l.z + r.z }; }
inline float4 operator + (float4 l, float4 r) { return { l.x + r.x, l.y + r.y, l.z + r.z, l.w + r.w }; }
inline float2 operator + (float r, float2 l) { return { r + l.x, r + l.y }; }
inline float3 operator + (float r, float3 l) { return { r + l.x, r + l.y, r + l.z }; }
inline float4 operator + (float r, float4 l) { return { r + l.x, r + l.y, r + l.z, r + l.w }; }
inline float2 operator + (float2 l, float r) { return { l.x + r, l.y + r }; }
inline float3 operator + (float3 l, float r) { return { l.x + r, l.y + r, l.z + r }; }
inline float4 operator + (float4 l, float r) { return { l.x + r, l.y + r, l.z + r, l.w + r }; }

inline float2 operator * (float2 l, float2 r) { return { l.x * r.x, l.y * r.y }; }
inline float3 operator * (float3 l, float3 r) { return { l.x * r.x, l.y * r.y, l.z * r.z }; }
inline float4 operator * (float4 l, float4 r) { return { l.x * r.x, l.y * r.y, l.z * r.z, l.w * r.w }; }
inline float2 operator * (float l, float2 r) { return { l * r.x, l * r.y }; }
inline float3 operator * (float l, float3 r) { return { l * r.x, l * r.y, l * r.z }; }
inline float4 operator * (float l, float4 r) { return { l * r.x, l * r.y, l * r.z, l * r.w }; }
inline float2 operator * (float2 l, float r) { return { l.x * r, l.y * r }; }
inline float3 operator * (float3 l, float r) { return { l.x * r, l.y * r, l.z * r }; }
inline float4 operator * (float4 l, float r) { return { l.x * r, l.y * r, l.z * r, l.w * r }; }

inline float2 operator / (float2 l, float r) { return { l.x / r, l.y / r }; }
inline float3 operator / (float3 l, float r) { return { l.x / r, l.y / r, l.z / r }; }
inline float4 operator / (float4 l, float r) { return { l.x / r, l.y / r, l.z / r, l.w / r }; }

inline float2& operator -= (float2& l, float2 r)
{
	l.x -= r.x;
	l.y -= r.y;
	return l;
}
inline float3& operator -= (float3& l, float3 r)
{
	l.x -= r.x;
	l.y -= r.y;
	l.z -= r.z;
	return l;
}
inline float4& operator -= (float4& l, float4 r)
{
	l.x -= r.x;
	l.y -= r.y;
	l.z -= r.z;
	l.w -= r.w;
	return l;
}
inline float2& operator -= (float2& l, float r)
{
	l.x -= r;
	l.y -= r;
	return l;
}
inline float3& operator -= (float3& l, float r)
{
	l.x -= r;
	l.y -= r;
	l.z -= r;
	return l;
}
inline float4& operator -= (float4& l, float r)
{
	l.x -= r;
	l.y -= r;
	l.z -= r;
	l.w -= r;
	return l;
}

inline float2& operator += (float2& l, float2 r)
{
	l.x += r.x;
	l.y += r.y;
	return l;
}
inline float3& operator += (float3& l, float3 r)
{
	l.x += r.x;
	l.y += r.y;
	l.z += r.z;
	return l;
}
inline float4& operator += (float4& l, float4 r)
{
	l.x += r.x;
	l.y += r.y;
	l.z += r.z;
	l.w += r.w;
	return l;
}
inline float2& operator += (float2& l, float r)
{
	l.x += r;
	l.y += r;
	return l;
}
inline float3& operator += (float3& l, float r)
{
	l.x += r;
	l.y += r;
	l.z += r;
	return l;
}
inline float4& operator += (float4& l, float r)
{
	l.x += r;
	l.y += r;
	l.z += r;
	l.w += r;
	return l;
}

inline float2& operator *= (float2& l, float2 r)
{
	l.x *= r.x;
	l.y *= r.y;
	return l;
}
inline float3& operator *= (float3& l, float3 r)
{
	l.x *= r.x;
	l.y *= r.y;
	l.z *= r.z;
	return l;
}
inline float4& operator *= (float4& l, float4 r)
{
	l.x *= r.x;
	l.y *= r.y;
	l.z *= r.z;
	l.w *= r.w;
	return l;
}
inline float2& operator *= (float2& l, float r)
{
	l.x *= r;
	l.y *= r;
	return l;
}
inline float3& operator *= (float3& l, float r)
{
	l.x *= r;
	l.y *= r;
	l.z *= r;
	return l;
}
inline float4& operator *= (float4& l, float r)
{
	l.x *= r;
	l.y *= r;
	l.z *= r;
	l.w *= r;
	return l;
}

inline float2& operator /= (float2& l, float r)
{
	l.x /= r;
	l.y /= r;
	return l;
}
inline float3& operator /= (float3& l, float r)
{
	l.x /= r;
	l.y /= r;
	l.z /= r;
	return l;
}
inline float4& operator /= (float4& l, float r)
{
	l.x /= r;
	l.y /= r;
	l.z /= r;
	l.w /= r;
	return l;
}






#define __MATH_METHOD_1(method) \
	inline float2 method(float2 v) { return { method(v.x), method(v.y) }; } \
	inline float3 method(float3 v) { return { method(v.x), method(v.y), method(v.z) }; } \
	inline float4 method(float4 v) { return { method(v.x), method(v.y), method(v.z), method(v.w) }; }
#define __MATH_METHOD_2(method) \
	inline float2 method(float2 l, float2 r) { return { method(l.x, r.x), method(l.y, r.y) }; } \
	inline float3 method(float3 l, float3 r) { return { method(l.x, r.x), method(l.y, r.y), method(l.z, r.z) }; } \
	inline float4 method(float4 l, float4 r) { return { method(l.x, r.x), method(l.y, r.y), method(l.z, r.z), method(l.w, r.w) }; }
__MATH_METHOD_1(abs)
__MATH_METHOD_1(acos)
__MATH_METHOD_1(asin)
__MATH_METHOD_1(atan)
__MATH_METHOD_2(atan2)
__MATH_METHOD_1(ceil)
__MATH_METHOD_1(cbrt)
__MATH_METHOD_1(cos)
__MATH_METHOD_1(cosh)
__MATH_METHOD_1(degrees)
__MATH_METHOD_1(exp)
__MATH_METHOD_1(exp2)
__MATH_METHOD_1(floor)
__MATH_METHOD_2(fmod)
__MATH_METHOD_1(frac)
__MATH_METHOD_1(log)
__MATH_METHOD_1(log10)
__MATH_METHOD_1(log2)
__MATH_METHOD_2(pow)
__MATH_METHOD_1(radians)
__MATH_METHOD_1(round)
__MATH_METHOD_1(rsqrt)
__MATH_METHOD_1(saturate)
__MATH_METHOD_1(sin)
__MATH_METHOD_1(sinh)
__MATH_METHOD_1(sqrt)
__MATH_METHOD_1(tan)
__MATH_METHOD_1(tanh)
__MATH_METHOD_1(trunc)
#undef __MATH_METHOD_1
#undef __MATH_METHOD_2


inline float2 modf(float2 l, float2* r) { return { modf(l.x, &r->x), modf(l.y, &r->y) }; }
inline float3 modf(float3 l, float3* r) { return { modf(l.x, &r->x), modf(l.y, &r->y), modf(l.z, &r->z) }; }
inline float4 modf(float4 l, float4* r) { return { modf(l.x, &r->x), modf(l.y, &r->y), modf(l.z, &r->z), modf(l.w, &r->w) }; }


inline sbyte  clamp(sbyte  x, sbyte  min, sbyte  max) { return x < min ? min : x > max ? max : x; }
inline short  clamp(short  x, short  min, short  max) { return x < min ? min : x > max ? max : x; }
inline int    clamp(int    x, int    min, int    max) { return x < min ? min : x > max ? max : x; }
inline long   clamp(long   x, long   min, long   max) { return x < min ? min : x > max ? max : x; }
inline byte   clamp(byte   x, byte   min, byte   max) { return x < min ? min : x > max ? max : x; }
inline ushort clamp(ushort x, ushort min, ushort max) { return x < min ? min : x > max ? max : x; }
inline uint   clamp(uint   x, uint   min, uint   max) { return x < min ? min : x > max ? max : x; }
inline ulong  clamp(ulong  x, ulong  min, ulong  max) { return x < min ? min : x > max ? max : x; }
inline float  clamp(float  x, float  min, float  max) { return x < min ? min : x > max ? max : x; }
inline double clamp(double x, double min, double max) { return x < min ? min : x > max ? max : x; }
inline float2 clamp(float2 x, float2 min, float2 max)
{
	return {
		x.x < min.x ? min.x : x.x > max.x ? max.x : x.x,
		x.y < min.y ? min.y : x.y > max.y ? max.y : x.y,
	};
}
inline float3 clamp(float3 x, float3 min, float3 max)
{
	return {
		x.x < min.x ? min.x : x.x > max.x ? max.x : x.x,
		x.y < min.y ? min.y : x.y > max.y ? max.y : x.y,
		x.z < min.z ? min.z : x.z > max.z ? max.z : x.z,
	};
}
inline float4 clamp(float4 x, float4 min, float4 max)
{
	return {
		x.x < min.x ? min.x : x.x > max.x ? max.x : x.x,
		x.y < min.y ? min.y : x.y > max.y ? max.y : x.y,
		x.z < min.z ? min.z : x.z > max.z ? max.z : x.z,
		x.w < min.w ? min.w : x.w > max.w ? max.w : x.w,
	};
}

inline float3 cross(float3 lhs, float3 rhs)
{
	return {
		lhs.y * rhs.z - lhs.z * rhs.y,
		lhs.z * rhs.x - lhs.x * rhs.z,
		lhs.x * rhs.y - lhs.y * rhs.x
	};
}

inline float distance(float l, float r) { return abs(l - r); }
inline float distance(float2 l, float2 r)
{
	float dx = l.x - r.x;
	float dy = l.y - r.y;
	return sqrt(dx * dx + dy * dy);
}
inline float distance(float3 l, float3 r)
{
	float dx = l.x - r.x;
	float dy = l.y - r.y;
	float dz = l.z - r.z;
	return sqrt(dx * dx + dy * dy + dz * dz);
}
inline float distance(float4 l, float4 r)
{
	float dx = l.x - r.x;
	float dy = l.y - r.y;
	float dz = l.z - r.z;
	float dw = l.w - r.w;
	return sqrt(dx * dx + dy * dy + dz * dz + dw * dw);
}

inline float dot(float l, float r) { return l * r; }
inline float dot(float2 l, float2 r) { return l.x * r.x + l.y * r.y; }
inline float dot(float3 l, float3 r) { return l.x * r.x + l.y * r.y + l.z * r.z; }
inline float dot(float4 l, float4 r) { return l.x * r.x + l.y * r.y + l.z * r.z + l.w * r.w; }

inline float2 faceforward(float2 n, float2 i, float2 g) { return dot(i, g) < 0 ? n : -n; }
inline float3 faceforward(float3 n, float3 i, float3 g) { return dot(i, g) < 0 ? n : -n; }
inline float4 faceforward(float4 n, float4 i, float4 g) { return dot(i, g) < 0 ? n : -n; }

inline float fixnan(float x, float value = 0) { return isnan(x) ? value : x; }
inline float2 fixnan(float2 x, float2 value = {}) { return { isnan(x.x) ? value.x : x.x, isnan(x.y) ? value.y : x.y }; }
inline float3 fixnan(float3 x, float3 value = {}) { return { isnan(x.x) ? value.x : x.x, isnan(x.y) ? value.y : x.y, isnan(x.z) ? value.z : x.z }; }
inline float4 fixnan(float4 x, float4 value = {}) { return { isnan(x.x) ? value.x : x.x, isnan(x.y) ? value.y : x.y, isnan(x.z) ? value.z : x.z, isnan(x.w) ? value.w : x.w }; }


inline float length(float x) { return abs(x); }
inline float length(float2 x) { return sqrt(x.x * x.x + x.y * x.y); }
inline float length(float3 x) { return sqrt(x.x * x.x + x.y * x.y + x.z * x.z); }
inline float length(float4 x) { return sqrt(x.x * x.x + x.y * x.y + x.z * x.z + x.w * x.w); }

inline float lerp(float l, float r, float t) { return l + t * (r - l); }
inline float2 lerp(float2 l, float2 r, float t) { return { lerp(l.x, r.x, t), lerp(l.y, r.y, t) }; }
inline float3 lerp(float3 l, float3 r, float t) { return { lerp(l.x, r.x, t), lerp(l.y, r.y, t), lerp(l.z, r.z, t) }; }
inline float4 lerp(float4 l, float4 r, float t) { return { lerp(l.x, r.x, t), lerp(l.y, r.y, t), lerp(l.z, r.z, t), lerp(l.w, r.w, t) }; }

inline float lerps(float l, float r, float t) { return l + saturate(t) * (r - l); }
inline float2 lerps(float2 l, float2 r, float t) { return { lerps(l.x, r.x, t), lerps(l.y, r.y, t) }; }
inline float3 lerps(float3 l, float3 r, float t) { return { lerps(l.x, r.x, t), lerps(l.y, r.y, t), lerps(l.z, r.z, t) }; }
inline float4 lerps(float4 l, float4 r, float t) { return { lerps(l.x, r.x, t), lerps(l.y, r.y, t), lerps(l.z, r.z, t), lerps(l.w, r.w, t) }; }

inline float magnitude(float x) { return abs(x); }
inline float magnitude(float2 x) { return sqrt(x.x * x.x + x.y * x.y); }
inline float magnitude(float3 x) { return sqrt(x.x * x.x + x.y * x.y + x.z * x.z); }
inline float magnitude(float4 x) { return sqrt(x.x * x.x + x.y * x.y + x.z * x.z + x.w * x.w); }

inline float max(float l, float r) { return l > r ? l : r; }
inline float2 max(float2 l, float2 r) { return { l.x > r.x ? l.x : r.x, l.y > r.y ? l.y : r.y }; }
inline float3 max(float3 l, float3 r) { return { l.x > r.x ? l.x : r.x, l.y > r.y ? l.y : r.y, l.z > r.z ? l.z : r.z }; }
inline float4 max(float4 l, float4 r) { return { l.x > r.x ? l.x : r.x, l.y > r.y ? l.y : r.y, l.z > r.z ? l.z : r.z, l.w > r.w ? l.w : r.w }; }

inline int min(int l, int r) { return l < r ? l : r; }
inline uint min(uint l, uint r) { return l < r ? l : r; }
inline float min(float l, float r) { return l < r ? l : r; }
inline float2 min(float2 l, float2 r) { return { l.x < r.x ? l.x : r.x, l.y < r.y ? l.y : r.y }; }
inline float3 min(float3 l, float3 r) { return { l.x < r.x ? l.x : r.x, l.y < r.y ? l.y : r.y, l.z < r.z ? l.z : r.z }; }
inline float4 min(float4 l, float4 r) { return { l.x < r.x ? l.x : r.x, l.y < r.y ? l.y : r.y, l.z < r.z ? l.z : r.z, l.w < r.w ? l.w : r.w }; }

inline float2 normalize(float2 v)
{
	auto length = v.x * v.x + v.y * v.y;
	if (length < EPSILON)
		return { 0.0f, 0.0f };

	length = rsqrt(length);
	return { v.x * length, v.y * length };
}
inline float3 normalize(float3 v)
{
	auto length = v.x * v.x + v.y * v.y + v.z * v.z;
	if (length < EPSILON)
		return { 0.0f, 0.0f, 0.0f };

	length = rsqrt(length);
	return { v.x * length, v.y * length, v.z * length };
}
inline float4 normalize(float4 v)
{
	auto length = v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w;
	if (length < EPSILON)
		return { 0.0f, 0.0f, 0.0f, 0.0f };

	length = rsqrt(length);
	return { v.x * length, v.y * length, v.z * length, v.w * length };
}

inline float reflect(float direction, float normal) { return -2.0f * dot(direction, normal) * normal + direction; }
inline float2 reflect(float2 direction, float2 normal) { return -2.0f * dot(direction, normal) * normal + direction; }
inline float3 reflect(float3 direction, float3 normal) { return -2.0f * dot(direction, normal) * normal + direction; }
inline float4 reflect(float4 direction, float4 normal) { return -2.0f * dot(direction, normal) * normal + direction; }

inline float2 refract(float2 i, float2 n, float eta)
{
	auto cosi = dot(-i, n);
	auto cost2 = 1.0f - eta * eta * (1.0f - cosi * cosi);
	return cost2 > 0 ? eta * i + ((eta * cosi - sqrt(abs(cost2))) * n) : float2{};
}
inline float3 refract(float3 i, float3 n, float eta)
{
	auto cosi = dot(-i, n);
	auto cost2 = 1.0f - eta * eta * (1.0f - cosi * cosi);
	return cost2 > 0 ? eta * i + ((eta * cosi - sqrt(abs(cost2))) * n) : float3{};
}
inline float4 refract(float4 i, float4 n, float eta)
{
	auto cosi = dot(-i, n);
	auto cost2 = 1.0f - eta * eta * (1.0f - cosi * cosi);
	return cost2 > 0 ? eta * i + ((eta * cosi - sqrt(abs(cost2))) * n) : float4{};
}

inline float smoothstep(float min, float max, float x)
{
	float t = saturate((x - min) / (max - min));
	return t * t * (3.0f - (2.0f * t));
}
inline float2 smoothstep(float2 min, float2 max, float2 x) { return { smoothstep(min.x, max.x, x.x), smoothstep(min.y, max.y, x.y) }; }
inline float3 smoothstep(float3 min, float3 max, float3 x) { return { smoothstep(min.x, max.x, x.x), smoothstep(min.y, max.y, x.y), smoothstep(min.z, max.z, x.z) }; }
inline float4 smoothstep(float4 min, float4 max, float4 x) { return { smoothstep(min.x, max.x, x.x), smoothstep(min.y, max.y, x.y), smoothstep(min.z, max.z, x.z), smoothstep(min.w, max.w, x.w) }; }

inline float sqr(float x) { return x * x; }
inline float sqrlength(float x) { return x * x; }
inline float sqrlength(float2 x) { return x.x * x.x + x.y * x.y; }
inline float sqrlength(float3 x) { return x.x * x.x + x.y * x.y + x.z * x.z; }
inline float sqrlength(float4 x) { return x.x * x.x + x.y * x.y + x.z * x.z + x.w * x.w; }

inline float sqrmagnitude(float x) { return x * x; }
inline float sqrmagnitude(float2 x) { return x.x * x.x + x.y * x.y; }
inline float sqrmagnitude(float3 x) { return x.x * x.x + x.y * x.y + x.z * x.z; }
inline float sqrmagnitude(float4 x) { return x.x * x.x + x.y * x.y + x.z * x.z + x.w * x.w; }


float smoothdamp(float current, float target, float* velocity, float smoothness, float timestep)
{
	var invsmoothness = 2 / smoothness;
	var x = invsmoothness * timestep;
	var exp = 1 / (1 + x + 0.48f * x * x + 0.235f * x * x * x);
	var delta = current - target;
	var temp = (*velocity + invsmoothness * delta) * timestep;
	var output = target + (delta + temp) * exp;
	if (delta < 0 == output > target)
	{
		*velocity = 0;
		return target;
	}
	*velocity = (*velocity - invsmoothness * temp) * exp;
	return output;
}






/**
 * MATRIX
 *
 *
 *
 *
 *
 *
 *
 *
 */




