#if SOLITUDE_GL
#if SOLITUDE_DEBUG_SHADERS
#define gl(x) gl##x
#else
#define gl(x) gl##x
#endif

struct program_common
{
	GLuint handle;
    
	// attributes
	GLuint position;
	GLuint tangent;
	GLuint normal;
	GLuint uv0;
	GLuint uv1;
	GLuint color;
};

struct program_sky : public program_common
{
    GLuint u_main_tex;
    GLuint u_vp;
};


struct program_default : public program_common
{
	// shared
	GLuint u_eye;
    
	// vertex
	GLuint u_color;
	GLuint u_model;
	GLuint u_modelinv;
	GLuint u_mvp;
	GLuint u_tex_st;
	GLuint u_light_st;
    
	// fragment
	GLuint u_parallax_tex;
	GLuint u_main_tex;
	GLuint u_bump_tex;
	GLuint u_roughness_tex;
	GLuint u_ambient_occlusion_tex;
	GLuint u_skysphere_tex;
	GLuint u_emission_tex;
	GLuint u_emission;
	GLuint u_parallax;
	GLuint u_bump_scale;
	GLuint u_roughness;
	GLuint u_occlusion;
	GLuint u_fog_params;
	GLuint u_sun_direction;
	GLuint u_sun_color;
	GLuint u_lights_color;
	GLuint u_lights_position;
	
	GLuint u_hammersley64;
};

struct program_ui : public program_common
{
	GLuint u_main_tex;
};

struct open_gl
{
	s32 Version;
	string Header;
    program_sky Sky;
	program_default Default;
	program_ui UI;
    
    union
    {
#define SOLITUDE_GL_BUFFER_SIZE 2
        struct
        {
            GLuint VertexBuffer;
            GLuint IndexBuffer;
        };
        
        GLuint Buffers[SOLITUDE_GL_BUFFER_SIZE];
    };
};


const r32 hammersley64[128] = {
    0.000000f, 0.000000f,
    0.015625f, 0.500000f,
    0.031250f, 0.250000f,
    0.046875f, 0.750000f,
    0.062500f, 0.125000f,
    0.078125f, 0.625000f,
    0.093750f, 0.375000f,
    0.109375f, 0.875000f,
    0.125000f, 0.062500f,
    0.140625f, 0.562500f,
    0.156250f, 0.312500f,
    0.171875f, 0.812500f,
    0.187500f, 0.187500f,
    0.203125f, 0.687500f,
    0.218750f, 0.437500f,
    0.234375f, 0.937500f,
    0.250000f, 0.031250f,
    0.265625f, 0.531250f,
    0.281250f, 0.281250f,
    0.296875f, 0.781250f,
    0.312500f, 0.156250f,
    0.328125f, 0.656250f,
    0.343750f, 0.406250f,
    0.359375f, 0.906250f,
    0.375000f, 0.093750f,
    0.390625f, 0.593750f,
    0.406250f, 0.343750f,
    0.421875f, 0.843750f,
    0.437500f, 0.218750f,
    0.453125f, 0.718750f,
    0.468750f, 0.468750f,
    0.484375f, 0.968750f,
    0.500000f, 0.015625f,
    0.515625f, 0.515625f,
    0.531250f, 0.265625f,
    0.546875f, 0.765625f,
    0.562500f, 0.140625f,
    0.578125f, 0.640625f,
    0.593750f, 0.390625f,
    0.609375f, 0.890625f,
    0.625000f, 0.078125f,
    0.640625f, 0.578125f,
    0.656250f, 0.328125f,
    0.671875f, 0.828125f,
    0.687500f, 0.203125f,
    0.703125f, 0.703125f,
    0.718750f, 0.453125f,
    0.734375f, 0.953125f,
    0.750000f, 0.046875f,
    0.765625f, 0.546875f,
    0.781250f, 0.296875f,
    0.796875f, 0.796875f,
    0.812500f, 0.171875f,
    0.828125f, 0.671875f,
    0.843750f, 0.421875f,
    0.859375f, 0.921875f,
    0.875000f, 0.109375f,
    0.890625f, 0.609375f,
    0.906250f, 0.359375f,
    0.921875f, 0.859375f,
    0.937500f, 0.234375f,
    0.953125f, 0.734375f,
    0.968750f, 0.484375f,
    0.984375f, 0.984375f,
};
#endif // SOLITUDE_GL
