


// https://www.khronos.org/opengl/wiki/Vertex_Rendering#Indirect_rendering

static GLuint
CreateProgram(const GLchar* header, const GLchar* vertexCode, const GLchar* fragmentCode, program_common* result)
{
	const GLchar* vertexShader[] = { header, vertexCode };
	var vertex = glCreateShader(GL_VERTEX_SHADER);
	gl(ShaderSource(vertex, 2, vertexShader, 0));
	gl(CompileShader(vertex));
	
	const GLchar* fragmentShader[] = { header, fragmentCode };
	var fragment = glCreateShader(GL_FRAGMENT_SHADER);
	gl(ShaderSource(fragment, 2, fragmentShader, 0));
	gl(CompileShader(fragment));
	
	u32 program = glCreateProgram();
	gl(AttachShader(program, vertex));
	gl(AttachShader(program, fragment));
	gl(LinkProgram(program));
	
#if SOLITUDE_DEBUG
	GLint linked = false;
	gl(ValidateProgram(program));
	gl(GetProgramiv(program, GL_LINK_STATUS, &linked));
	if (!linked)
	{
		GLsizei length; // Ignored
		char errors[4096];
		gl(GetShaderInfoLog(vertex, sizeof(errors), &length, errors));
		if (errors[0]) print_error("VERTEX: %s", errors);
		//if (errors[0]) print_error("VERTEX: %s\n%s", errors, vertexCode);
		gl(GetShaderInfoLog(fragment, sizeof(errors), &length, errors));
		if (errors[0]) print_error("FRAGMENT: %s", errors);
		//if (errors[0]) print_error("FRAGMENT: %s\n%s", errors, fragmentCode);
		gl(GetProgramInfoLog(program, sizeof(errors), &length, errors));
		if (errors[0]) print_error("PROGRAM: %s", errors);
		assert(!"Shader failed to validate");
	}
#endif
	
	gl(DeleteShader(vertex));
	gl(DeleteShader(fragment));
	
	result->handle = program;
	result->position = glGetAttribLocation(program, "position");
	result->tangent = glGetAttribLocation(program, "tangent");
	result->normal = glGetAttribLocation(program, "normal");
	result->uv0 = glGetAttribLocation(program, "uv0");
	result->uv1 = glGetAttribLocation(program, "uv1");
	result->color = glGetAttribLocation(program, "color");
	return program;
}

static void
CompileSkyProgram(open_gl* opengl, program_sky* result, string& path, string& vertex_buffer, string& fragment_buffer)
{
	print("Compiling 'default' shader..");
	
	var length = path.length;
	Append(path, "shaders/sky.vert");
	Terminate(path);
	ReadAllText(path.data_cchar, vertex_buffer);
	Terminate(vertex_buffer);
	path.length = length;
	
	Append(path, "shaders/sky.frag");
	Terminate(path);
	ReadAllText(path.data_cchar, fragment_buffer);
	Terminate(fragment_buffer);
	path.length = length;
	
	var program = CreateProgram(opengl->Header.data_cchar, vertex_buffer.data_cchar, fragment_buffer.data_cchar, result);
	
	// Shared
	result->u_main_tex = glGetUniformLocation(program, "u_main_tex");
	result->u_vp = glGetUniformLocation(program, "u_vp");
}

static void
CompileDefaultProgram(open_gl* opengl, program_default* result, string& path, string& vertex_buffer, string& fragment_buffer)
{
	print("Compiling 'default' shader..");
	
	var length = path.length;
	Append(path, "shaders/default.vert");
	Terminate(path);
	ReadAllText(path.data_cchar, vertex_buffer);
	Terminate(vertex_buffer);
	path.length = length;
	
	Append(path, "shaders/default.frag");
	Terminate(path);
	ReadAllText(path.data_cchar, fragment_buffer);
	Terminate(fragment_buffer);
	path.length = length;
	
	var program = CreateProgram(opengl->Header.data_cchar, vertex_buffer.data_cchar, fragment_buffer.data_cchar, result);
	
	// Shared
	result->u_eye = glGetUniformLocation(program, "u_eye");
	
	// Vertex program
	result->u_color = glGetUniformLocation(program, "u_color");
	result->u_model = glGetUniformLocation(program, "u_model");
	result->u_modelinv = glGetUniformLocation(program, "u_modelinv");
	result->u_mvp = glGetUniformLocation(program, "u_mvp");
	result->u_tex_st = glGetUniformLocation(program, "u_tex_st");
	result->u_light_st = glGetUniformLocation(program, "u_light_st");
	
	// Fragment program
	result->u_parallax_tex = glGetUniformLocation(program, "u_parallax_tex");
	result->u_main_tex = glGetUniformLocation(program, "u_main_tex");
	result->u_bump_tex = glGetUniformLocation(program, "u_bump_tex");
	result->u_roughness_tex = glGetUniformLocation(program, "u_roughness_tex");
	result->u_ambient_occlusion_tex = glGetUniformLocation(program, "u_ambient_occlusion_tex");
	result->u_skysphere_tex = glGetUniformLocation(program, "u_skysphere_tex");
	result->u_emission_tex = glGetUniformLocation(program, "u_emission_tex");
	result->u_emission = glGetUniformLocation(program, "u_emission");
	result->u_parallax = glGetUniformLocation(program, "u_parallax");
	result->u_bump_scale = glGetUniformLocation(program, "u_bump_scale");
	result->u_roughness = glGetUniformLocation(program, "u_roughness");
	result->u_occlusion = glGetUniformLocation(program, "u_occlusion");
	
	// Fragment program -> Global settings
	result->u_fog_params = glGetUniformLocation(program, "u_fog_params");
	result->u_sun_direction = glGetUniformLocation(program, "u_sun_direction");
	result->u_sun_color = glGetUniformLocation(program, "u_sun_color");
	result->u_lights_color = glGetUniformLocation(program, "u_lights_color");
	result->u_lights_position = glGetUniformLocation(program, "u_lights_position");
	
	if (opengl->Version < 420)
		result->u_hammersley64 = glGetUniformLocation(program, "u_hammersley64");
}

static void
CompileUiProgram(open_gl* opengl, program_ui* result, string& path, string& vertex_buffer, string& fragment_buffer)
{
	print("Compiling 'ui_default' shader..");
	
	var length = path.length;
	Append(path, "shaders/ui_default.vert");
	Terminate(path);
	ReadAllText(path.data_cchar, vertex_buffer);
	Terminate(vertex_buffer);
	path.length = length;
	
	Append(path, "shaders/ui_default.frag");
	Terminate(path);
	ReadAllText(path.data_cchar, fragment_buffer);
	Terminate(fragment_buffer);
	path.length = length;
	
	var program = CreateProgram(opengl->Header.data_cchar, vertex_buffer.data_cchar, fragment_buffer.data_cchar, result);
	
	result->u_main_tex = glGetUniformLocation(program, "u_main_tex");
}

static void
StartUse(open_gl* opengl, program_common* program)
{
	gl(UseProgram(program->handle));
	gl(EnableVertexAttribArray(program->position));
	gl(VertexAttribPointer(program->position, 4, GL_FLOAT, false, sizeof(Vertex), (void*)offsetof(Vertex, position)));
	gl(EnableVertexAttribArray(program->tangent));
	gl(VertexAttribPointer(program->tangent, 4, GL_FLOAT, false, sizeof(Vertex), (void*)offsetof(Vertex, tangent)));
	gl(EnableVertexAttribArray(program->normal));
	gl(VertexAttribPointer(program->normal, 3, GL_FLOAT, false, sizeof(Vertex), (void*)offsetof(Vertex, normal)));
	gl(EnableVertexAttribArray(program->uv0));
	gl(VertexAttribPointer(program->uv0, 2, GL_FLOAT, false, sizeof(Vertex), (void*)offsetof(Vertex, uv0)));
	gl(EnableVertexAttribArray(program->uv1));
	gl(VertexAttribPointer(program->uv1, 2, GL_FLOAT, false, sizeof(Vertex), (void*)offsetof(Vertex, uv1)));
	gl(EnableVertexAttribArray(program->color));
	gl(VertexAttribPointer(program->color, 4, GL_FLOAT, false, sizeof(Vertex), (void*)offsetof(Vertex, color)));
}

static void
StopUse(open_gl* opengl, program_common* program)
{
	gl(UseProgram(0));
	gl(DisableVertexAttribArray(program->position));
	gl(DisableVertexAttribArray(program->tangent));
	gl(DisableVertexAttribArray(program->normal));
	gl(DisableVertexAttribArray(program->uv0));
	gl(DisableVertexAttribArray(program->uv1));
	gl(DisableVertexAttribArray(program->color));
}

static void
StartUse(open_gl* opengl, RenderSettings* settings, program_default* program)
{
	StartUse(opengl, program);
	//gl(UniformMatrix4fv(program->pro));
	gl(Uniform3fv(program->u_eye, 1, settings->Eye.e));
	gl(Uniform4fv(program->u_fog_params, 1, settings->FogParams.e));
	if (opengl->Version < 420)
		gl(Uniform2fv(program->u_hammersley64, 64, hammersley64));
}





Mesh* test = nullptr;
Mesh* ui_test = nullptr;


open_gl* gl_Setup(Solitude& solitude, SDL_Window*& window, SDL_GLContext& context)
{
	assert(sizeof(GLboolean) == sizeof(bool));
	assert(sizeof(GLbyte) == sizeof(s8));
	assert(sizeof(GLubyte) == sizeof(u8));
	assert(sizeof(GLshort) == sizeof(s16));
	assert(sizeof(GLushort) == sizeof(u16));
	assert(sizeof(GLint) == sizeof(s32));
	assert(sizeof(GLuint) == sizeof(u32));
	assert(sizeof(GLsizei) == sizeof(s32));
	assert(sizeof(GLenum) == sizeof(s32));
	assert(sizeof(GLbitfield) == sizeof(b32));
	assert(sizeof(GLfloat) == sizeof(r32));
	assert(sizeof(GLdouble) == sizeof(r64));
	
	if (SDL_Init(SDL_INIT_VIDEO))
	{
		print_error("[SDL_Init] %s", SDL_GetError());
		exit(1);
	}
	
	window = SDL_CreateWindow(
		"Solitude",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		1280,
		720,
		SDL_WINDOW_OPENGL);
	
	if (!window)
	{
		print_error("[SDL_CreateWindow] %s", SDL_GetError());
		exit(1);
	}
	
	// Initialize OpenGL https://en.wikipedia.org/wiki/OpenGL_Shading_Language
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetSwapInterval(1);
	
	// Create context
	var tmp0 = StaticBuffer(solitude.Temp);
	var tmp1 = StaticBuffer(solitude.Temp);
	var tmp2 = StaticBuffer(solitude.Temp);
	var opengl = new open_gl();
	var gl_header = String(tmp0);
	context = SDL_GL_CreateContext(window);
	if (context)
	{
		opengl->Version = 460;
		Append(gl_header, "#version 460 core\n");
		Append(gl_header, "#define SOLITUDE_GL_VERSION 460\n");
	}
	else
	{
		opengl->Version = 330;
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
		Append(gl_header, "#version 330 core\n");
		Append(gl_header, "#define SOLITUDE_GL_VERSION 330\n");
		
		context = SDL_GL_CreateContext(window);
		if (!context)
		{
			print_error("[SDL_GL_CreateContext] %s", SDL_GetError());
			exit(1);
		}
	}
	// Create standard library for glsl
	{
		var path = String(solitude.App.AssetPath, tmp1);
		Append(path, "shaders/solitude.glsl");
		Terminate(path);
		var buffer = String(tmp2);
		ReadAllText(path.data_cchar, buffer);
		Append(gl_header, buffer);
	}
	opengl->Header = StringAlloc(gl_header);
	
	
	print("OpenGL version: %s", glGetString(GL_VERSION));
	
	// Initialize GLEW
	//glewExperimental = GL_TRUE;
	auto glewcode = glewInit();
	if (GLEW_OK != glewcode)
	{
		print_error("[glewInit] %s", glewGetErrorString(glewcode));
		exit(1);
	}
	SDL_GLContext();
	return opengl;
}

static void
gl_Start(Solitude& solitude, open_gl* opengl)
{
	GLuint vertexArray;
	gl(GenVertexArrays(1, &vertexArray));
	gl(BindVertexArray(vertexArray));
	gl(GenBuffers(SOLITUDE_GL_BUFFER_SIZE, opengl->Buffers));
	
	
	var tmp0 = StaticBuffer(solitude.Temp);
	var tmp1 = StaticBuffer(solitude.Temp);
	var tmp2 = StaticBuffer(solitude.Temp);
	var path = String(solitude.App.AssetPath, tmp0);
	var vertex = String(tmp1);
	var fragment = String(tmp2);
	
	CompileSkyProgram(opengl, &opengl->Sky, path, vertex, fragment);
	CompileDefaultProgram(opengl, &opengl->Default, path, vertex, fragment);


	var res = MatrixView({}, {1.0f, 4.0f, 5.0f, 1.0f});
	print_matrix(res.forward);









	m4 foo = {
		1.0f, 3.0f, 5.0f, 9.0,
		1.0f, 3.0f, 1.0f, 7.0f,
		4.0f, 3.0f, 9.0f, 7.0f,
		5.0f, 2.0f, 0.0f, 9.0f };
	var bar = foo * foo;
	print_matrix(bar);


	m4 inv = Inverse(foo);
	print_matrix(inv);

	r32 det = Determinant({
		1.0f, 3.0f, 5.0f, 9.0,
		1.0f, 3.0f, 1.0f, 7.0f,
		4.0f, 3.0f, 9.0f, 7.0f,
		5.0f, 2.0f, 0.0f, 9.0f});








}

static void
gl_Render(Solitude& solitude, open_gl* opengl)
{
	solitude.ToRender.IndexCount = 0;
	solitude.ToRender.VertexCount = 0;
	
	
	// push
	{
		var mesh = Sphere;
		var& renderer = solitude.ToRender;
		memcpy(&renderer.Vertices[renderer.VertexCount], mesh.Vertices, mesh.VertexCount * sizeof(Vertex));
		for (var i = 0; i < mesh.IndexCount; i++)
			renderer.Indices[renderer.IndexCount + i] = renderer.VertexCount + mesh.Indices[i];
		
		renderer.IndexCount += mesh.IndexCount;
		renderer.VertexCount += mesh.VertexCount;
	}
	
	
	
	
	gl(Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
	gl(ClearColor(0.2f, 0.4f, 0.9f, 1.0f));
	gl(ClearDepth(1.0f));
	
	gl(DepthMask(GL_TRUE));
	gl(ColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE));
	gl(DepthFunc(GL_LEQUAL));
	gl(Enable(GL_DEPTH_TEST));
	gl(Enable(GL_CULL_FACE));
	gl(CullFace(GL_BACK));
	gl(FrontFace(GL_CCW));
	gl(Disable(GL_BLEND));
	
	gl(BindBuffer(GL_ARRAY_BUFFER, opengl->VertexBuffer));
	gl(BufferData(
		GL_ARRAY_BUFFER,
		solitude.ToRender.VertexCount * sizeof(Vertex),
		solitude.ToRender.Vertices,
		GL_STREAM_DRAW));
	gl(BindBuffer(GL_ELEMENT_ARRAY_BUFFER, opengl->IndexBuffer));
	gl(BufferData(
		GL_ELEMENT_ARRAY_BUFFER,
		solitude.ToRender.IndexCount * sizeof(u32),
		solitude.ToRender.Indices,
		GL_STREAM_DRAW));
	
	var camera = solitude.Game.MainCamera;
	var projection = solitude.Rendering.Projection;
	var vp = solitude.Rendering.VP;

	var test = M4Identity();
	var test_inv = Inverse(test);

	// TODO: Render skybox
	if (false) {
		m4_inv sky_view = MatrixView({0.0f, 0.0f, 0.0f}, camera.transform.rotation);
		m4 vp = projection.forward * sky_view.inverse;
		
		gl(Disable(GL_DEPTH_TEST));
		gl(ActiveTexture(GL_TEXTURE0));
		gl(BindTexture(GL_TEXTURE_2D, solitude.Assets.Sky.handle));
		
		StartUse(opengl, &opengl->Sky);
		gl(UniformMatrix4fv(opengl->Sky.u_vp, 1, GL_FALSE, vp.e));
		gl(DrawElements(GL_TRIANGLES, solitude.ToRender.IndexCount, GL_UNSIGNED_INT, 0));
		StopUse(opengl, &opengl->Sky);
	}
	
	
	// Render opaque
	{
		gl(Enable(GL_DEPTH_TEST));
		gl(CullFace(GL_FRONT));
		
		
		StartUse(opengl, &solitude.Rendering, &opengl->Default);
		m4 mvp = vp.forward * test;
		//mvp = projection.forward * view.inverse;

		v4 color{ 1.0f, 1.0f, 1.0f, 1.0f };
		v4 tex_st{ 1.0f, 1.0f, 0.0f, 0.0f };
		v4 light_st{ 1.0f, 1.0f, 0.0f, 0.0f };
		
		v4 emission{ 0.0f, 0.0f, 0.0f, 0.0f };
		r32 parallax = 0.0f;
		r32 bump_scale = 1.0f;
		r32 roughness = 1.0f;
		r32 occlusion = 1.0f;
		
		
		
		v4 fog_params = Color24ToColor(0xA9C8E5, 0.03f);
		v3 sun_direction = Normalize(v3{ 1.0f, -3.0f, 1.0f });
		v3 sun_color{ 1.0f, 1.0f, 1.0f };
		gl(Uniform4fv(opengl->Default.u_fog_params, 1, fog_params.e));
		gl(Uniform3fv(opengl->Default.u_sun_direction, 1, sun_direction.e));
		gl(Uniform3fv(opengl->Default.u_sun_color, 1, sun_color.e));
		//gl(UniformMatrix4fv(opengl->Default.u_lights_color, 1, GL_FALSE, test_inv.e));
		//gl(UniformMatrix4fv(opengl->Default.u_lights_position, 1, GL_FALSE, test_inv.e));
		
		
		
		
		gl(Uniform3fv(opengl->Default.u_eye, 1, camera.transform.position.e));
		
		gl(Uniform4fv(opengl->Default.u_color, 1, color.e));
		gl(UniformMatrix4fv(opengl->Default.u_model, 1, GL_FALSE, test.e));
		gl(UniformMatrix4fv(opengl->Default.u_modelinv, 1, GL_FALSE, test_inv.e));
		gl(UniformMatrix4fv(opengl->Default.u_mvp, 1, GL_FALSE, mvp.e));
		gl(Uniform4fv(opengl->Default.u_tex_st, 1, tex_st.e));
		gl(Uniform4fv(opengl->Default.u_light_st, 1, light_st.e));
		
		
		
		
		gl(Uniform1i(opengl->Default.u_parallax_tex, 0));
		gl(ActiveTexture(GL_TEXTURE0));
		gl(BindTexture(GL_TEXTURE_2D, solitude.Assets.RockyMoss.height.handle));
		
		gl(Uniform1i(opengl->Default.u_main_tex, 1));
		gl(ActiveTexture(GL_TEXTURE1));
		gl(BindTexture(GL_TEXTURE_2D, solitude.Assets.RockyMoss.albedo.handle));
		gl(BindTexture(GL_TEXTURE_2D, solitude.Assets.White.handle));
		
		gl(Uniform1i(opengl->Default.u_bump_tex, 2));
		gl(ActiveTexture(GL_TEXTURE2));
		gl(BindTexture(GL_TEXTURE_2D, solitude.Assets.RockyMoss.normal.handle));
		
		gl(Uniform1i(opengl->Default.u_roughness_tex, 3));
		gl(ActiveTexture(GL_TEXTURE3));
		gl(BindTexture(GL_TEXTURE_2D, solitude.Assets.RockyMoss.roughness.handle));
		
		gl(Uniform1i(opengl->Default.u_ambient_occlusion_tex, 4));
		gl(ActiveTexture(GL_TEXTURE4));
		gl(BindTexture(GL_TEXTURE_2D, solitude.Assets.RockyMoss.ao.handle));
		
		gl(Uniform1i(opengl->Default.u_skysphere_tex, 5));
		gl(ActiveTexture(GL_TEXTURE5));
		gl(BindTexture(GL_TEXTURE_2D, solitude.Assets.Sky.handle));
		
		gl(Uniform1i(opengl->Default.u_emission_tex, 6));
		gl(ActiveTexture(GL_TEXTURE6));
		gl(BindTexture(GL_TEXTURE_2D, solitude.Assets.Clear.handle));
		
		gl(Uniform4fv(opengl->Default.u_emission, 1, emission.e));
		gl(Uniform1f(opengl->Default.u_parallax, parallax));
		gl(Uniform1f(opengl->Default.u_bump_scale, bump_scale));
		gl(Uniform1f(opengl->Default.u_roughness, roughness));
		gl(Uniform1f(opengl->Default.u_occlusion, occlusion));
		
		
		
		
		
		
		gl(DrawElements(GL_TRIANGLES, solitude.ToRender.IndexCount, GL_UNSIGNED_INT, 0));
		StopUse(opengl, &opengl->Default);
	}
	
	// TODO: Transparent
	
	// TODO: UI
}




