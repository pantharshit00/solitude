Texture BlackTexture;
Texture MagentaTexture;
Texture WhiteTexture;

Texture Color32ToTexture(Color32 color)
{
	GLuint texture;
	gl(GenTextures(1, &texture));
	gl(BindTexture(GL_TEXTURE_2D, texture));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));
	gl(TexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, color.e));
    
	return {
		(s32)texture,
		(s32)1,
		(s32)1,
	};
}

inline Texture
Color32ToTexture(u8 r, u8 g, u8 b, u8 a)
{
	return Color32ToTexture({r,g,b,a});
}

inline Texture
ColorToTexture(v4 color)
{
	r32 r8 = color.r * 255;
	u8 r = r8 >= 255 ? 255 : r8 < 0 ? 0 : (u8)r8;
	r32 g8 = color.g * 255;
	u8 g = g8 >= 255 ? 255 : g8 < 0 ? 0 : (u8)g8;
	r32 b8 = color.b * 255;
	u8 b = b8 >= 255 ? 255 : b8 < 0 ? 0 : (u8)b8;
	r32 a8 = color.a * 255;
	u8 a = a8 >= 255 ? 255 : a8 < 0 ? 0 : (u8)a8;
	return Color32ToTexture({r,g,b,a});
}

void InitializeDefaultTextures()
{
	BlackTexture = Color32ToTexture(0, 0, 0, 255);
	MagentaTexture = Color32ToTexture(255, 0, 255, 255);
	WhiteTexture = Color32ToTexture(255, 255, 255, 255);
}

Texture LoadTexture(
                    const char* path,
                    s32 min_filter = GL_NEAREST,
                    s32 mag_filter = GL_NEAREST,
                    s32 wrap_s = GL_REPEAT,
                    s32 wrap_t = GL_REPEAT)
{
	/*s32 x, y, channels;
	auto uc = stbi_load(path, &x, &y, &channels, 4);
	if (uc)
	{
		GLuint texture;
		gl(GenTextures(1, &texture));
		gl(BindTexture(GL_TEXTURE_2D, texture));
		gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filter));
		gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag_filter));
		gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_s));
		gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_t));
		gl(TexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE, uc));
		return {(s32)texture, x, y};
	}*/
    
	print("Cannot read file \"%s\" as texture", path);
	return MagentaTexture;
}
