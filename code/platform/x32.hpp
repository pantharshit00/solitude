#if defined __x86_64__ && !defined __ILP32__
#error !Could not compile x32
#endif

typedef signed char sbyte;
typedef unsigned char byte;
typedef unsigned char uchar;
typedef short unsigned int ushort;
typedef long long unsigned int ulong;
typedef unsigned int uint;
typedef unsigned int uptr;
typedef signed int sptr;

typedef unsigned int flags32;
typedef long long unsigned int flags64;

typedef           unsigned char u8;
typedef short     unsigned int  u16;
typedef           unsigned int  u32;
typedef long long unsigned int  u64;
typedef             signed char s8;
typedef short       signed int  s16;
typedef             signed int  s32;
typedef long long   signed int  s64;


