/**
 * Default types that are used in this program are:
 *     char      1 byte signed integer only used for platform specifics
 *     uchar     1 byte unsigned integer used as a character
 *     sbyte     1 byte signed integer
 *     byte      1 byte unsigned integer
 *     short     2 bytes signed integer
 *     ushort    2 bytes unsigned integer
 *     int       4 bytes signed integer
 *     uint      4 bytes unsigned integer
 *     long      8 bytes singed integer
 *     ulong     8 bytes unsigned integer
 *     float     4 bytes floating point
 *     double    8 bytes floating point
 *     uptr      unsigned pointer is on x64 8 bytes and on x32 4 bytes
 *     sptr      signed pointer is on x64 8 bytes and on x32 4 bytes
 *    
 *     flags32   4 bytes unsigned integer
 *     flags64   8 bytes unsigned integer 
 *
 *     int2
 *     float2
 *     float3
 *     float4
 *     float2x2
 *     float3x3
 *     float4x4
 * 
 *     Buffer
 *     utf8
 *     string
 * 
 * These are extension of default types:
 */

template<typename T, typename... Args>
void print(const char* const, T, Args...);

// Compiler stuff
#if !COMPILER_MSVC && !COMPILER_LLVM
	#if _MSC_VER
	#undef COMPILER_MSVC
	#define COMPILER_MSVC 1
	//#include <intrin.h>
	#else
	#undef COMPILER_LLVM
	#define COMPILER_LLVM 1
	//#include <x86intrin.h>
	#endif
#endif

// Platform stuff
#if _WIN64
	#define SOLITUDE_WIN       1
	#define SOLITUDE_WIN64     1
	#define SOLITUDE_X64       1
	#define _CRT_SECURE_NO_WARNINGS
	#include <Windows.h>
	#include <stdio.h>
	#include "x64.hpp"
	#include "win/win.hpp"
	#include "win64/win64.hpp"
	#include "types.hpp"
	#if SOLITUDE_VULKAN
		#include "win/vulkan.hpp"
	#endif
#elif _WIN32
	#define SOLITUDE_WIN       1
	#define SOLITUDE_WIN32     1
	#define SOLITUDE_X32       1
	#include "x32.hpp"
	#include "win/win.hpp"
	#include "win86/win86.hpp"
	#include "types.hpp"
	#if SOLITUDE_VULKAN
		#include "win/vulkan.hpp"
	#endif
#elif __ANDROID__ && (__x86_64__ || __ppc64__)
	#define SOLITUDE_ANDROID   1
	#define SOLITUDE_ANDROID64 1
	#define SOLITUDE_X64       1
	#include "x64.hpp"
#elif __ANDROID__
	#define SOLITUDE_ANDROID   1
	#define SOLITUDE_ANDROID32 1
	#define SOLITUDE_X32       1
	#include "x32.hpp"
#elif __unix__ && (__x86_64__ || __ppc64__)
	#define SOLITUDE_LINUX     1
	#define SOLITUDE_LINUX64   1
	#define SOLITUDE_X64       1
	#include "x64.hpp"
	#include <sys/mman.h>            // mmap
	#include <sys/stat.h>            // stat
	#include <dlfcn.h>
	#include <stdio.h>               // fprintf, stderr
	#include "linux64/linux64.hpp"
	#include "types.hpp"
	#include "math.hpp"
	#include "io.hpp"
	#include "memory.hpp"
	#include "array.hpp"
	#include "linux/linux.hpp"
	#include "linux/video.hpp"
	#if SOLITUDE_VULKAN
		#include "vulkan/vulkan.hpp"
		#include "linux/vulkan.hpp"
	#endif
#elif __unix__
	#define SOLITUDE_LINUX     1
	#define SOLITUDE_LINUX32   1
	#define SOLITUDE_X32       1
	#include "x32.hpp"
	#include "linx32/linux32.hpp"
	#include "linux/linux.hpp"
	#include "types.hpp"
	#if SOLITUDE_VULKAN
		#include "linux/vulkan.hpp"
	#endif
#elif __APPLE__
	#define SOLITUDE_APPLE     1
	#error Apple/iOS/osX not supported
#else
	#error This is an unknown and unsupported platform
#endif



