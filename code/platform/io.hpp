/**
 * TODO:
 * Catch crashes in platform.hpp
 * print and all the other types of print
 * check if text coloring is supported
 * 
 * to_string for all types
 * 
 * move to types.hpp
 * 
 * 
 * 
 * 
 * 
 */

namespace io
{
	const char* numbers   = "0123456789";
	const char* lower_hex = "0123456789abcdef";
	const char* upper_hex = "0123456789ABCDEF";

	constexpr flags32 WRITE_TO_FILE   = 1 << 0;
	constexpr flags32 WRITE_TO_BUFFER = 1 << 1;
	
	constexpr flags32 SIGN            = 1 <<  2; // Explicit sign
	constexpr flags32 TYPE            = 1 <<  3; // Type of the value
	constexpr flags32 UPPER           = 1 <<  4; // Use upper case
	constexpr flags32 SCIENTIFIC      = 1 <<  5; // Scientific notation
	constexpr flags32 HEX             = 1 <<  6; // Base 16 notation
	constexpr flags32 OCT             = 1 <<  7; // Base 8 notation
	constexpr flags32 BIN             = 1 <<  8; // Base 2 noration
	constexpr flags32 BOOL            = 1 <<  9; // true or false
	constexpr flags32 ESCAPE          = 1 << 10; // Escape a string
	constexpr flags32 VALUE           = 1 << 11; // If it is a pointer, dereference if possible
	
	struct writer
	{
		int type;
		union 
		{
			FILE* file;
			struct buffer buffer;
		};
	};

	struct fmt : public writer
	{
		flags32 flags;
		int preceding;  // How many 0's to prepend a number 4 of 21 gives 0021
		int precision;  // How digits to round a number of 4 of 2.34567 gives 2.3457
		char tmp[128];
	};
}

inline error append(buffer* dst, const byte* buffer, uptr length)
{
	if (dst->length + length >= dst->capacity)
		return make_error("Cannot write to buffer, insufficient capacity");

	var d = dst->data + dst->length;
	dst->length += length;
	while(length--)
		*d++ = *buffer++;

	return error::ok;
}
inline error append(buffer* dst, byte value)
{
	if (dst->length + 1 >= dst->capacity)
		return make_error("Cannot write to buffer, insufficient capacity");
	
	dst->data[dst->length++] = value;
	return error::ok;
}





inline error write(io::writer* w, const void* buffer, uptr length)
{
	error err = error::ok;
	if (w->type & io::WRITE_TO_FILE)
		fwrite(buffer, length, sizeof(byte), w->file);
	if (w->type & io::WRITE_TO_BUFFER)
		err = append(&w->buffer, (const byte*)buffer, length);
	return err;
}
inline error write(io::writer* w, fixed_buffer buffer)
{
	error err = error::ok;
	if (w->type & io::WRITE_TO_FILE)
		fwrite(buffer.data, buffer.length, sizeof(byte), w->file);
	if (w->type & io::WRITE_TO_BUFFER)
		err = append(&w->buffer, buffer.data, buffer.length);
	return err;
}
inline error write(io::writer* w, byte c)
{
	if (w->type & io::WRITE_TO_FILE)
		fputc((char)c, w->file);
	if (w->type & io::WRITE_TO_BUFFER)
		return append(&w->buffer, c);
	return error::ok;
}
inline error write(io::writer* w, char c)
{
	if (w->type & io::WRITE_TO_FILE)
		fputc(c, w->file);
	if (w->type & io::WRITE_TO_BUFFER)
		return append(&w->buffer, (byte)c);
	return error::ok;
}
inline error write(io::writer* w, const char* s)
{
	if (w->type & io::WRITE_TO_FILE)
		fputs(s, w->file);
	if (w->type & io::WRITE_TO_BUFFER)
		return append(&w->buffer, (const byte*)s, len(s));
	return error::ok;
}

void write_s64(io::fmt* fmt, long value)
{
	char* buffer = fmt->tmp;
	char* ptr = buffer;
	
	var flags = fmt->flags;
	if (flags & io::BOOL)
	{
		if (flags & io::UPPER) write(fmt, value ? "TRUE" : "FALSE");
		else write(fmt, value ? "true" : "false");
		return;
	}
	
	var base = 10;
	if (flags & io::HEX) base = 16;
	if (flags & io::OCT) base = 8;
	if (flags & io::BIN) base = 2;
	
	var numbers = io::lower_hex;
	if (flags & io::UPPER) numbers = io::upper_hex;
	
	if (value < 0)
	{
		*ptr++ = '-';
		value = -value;
	}
	else if (flags & io::SIGN)
		*ptr++ = '+';
	
	var tmp = value;
	do {
		*ptr++ = '0';
		tmp /= base;
	} while(tmp || ptr - buffer < fmt->preceding);
	
	var length = ptr - buffer;
	do {
		var next = value / base;
		*--ptr = numbers[value - next * base];
		value = next;
	} while(value);

	write(fmt, buffer, length);
}
void write_r32(io::fmt* fmt, float value)
{
	var flags = fmt->flags;
	var base = 10;
	if (flags & (io::HEX | io::OCT | io::BIN))
	{
		//to_string(fmt, x.u);
		return;
	}
	
	if (isnan(value))
	{
		write(fmt, "NaN");
		return;
	}

	var numbers = io::lower_hex;
	if (flags & io::UPPER) numbers = io::upper_hex;

	union
	{
		float f;
		int i;
	} x {value};
	
	char buffer[64];
	var ptr = buffer;
	short exp2 = (uchar)(x.i >> 23) - SBYTEMAX;
	ulong mantissa = (x.i & 0xFFFFFF) | (1 << 23);
	ulong frac_val = 0;
	ulong int_val = 0;
	int safe_shift = -(exp2 + 1);
	ulong safe_mask = ULONGMAX >> (64 - 24 - safe_shift); 

	if (x.i < 0) *ptr++ = '-';
	else if (flags & io::SIGN) *ptr++ = '+';

	if (exp2 < -36)
	{
		var preceed = fmt->preceding;
		do *ptr++ = '0';
		while (--preceed > 0);

		var precision = fmt->precision;
		if (precision)
		{
			*ptr++ = '.';
			do *ptr++ = '0';
			while (--preceed > 0);
		}

		write(fmt, buffer, ptr - buffer);
	}
	else if (isinf(value))
	{
		write(fmt, buffer, ptr - buffer);
		if (flags & io::UPPER) write(fmt, "INFINITY");
		else write(fmt, "infinity");
	}
	else
	{
		if (exp2 >= 64) int_val = ULONGMAX;
		else if (exp2 >= 23) int_val = mantissa << (exp2 - 23);
		else if (exp2 >= 0)
		{
			int_val = mantissa >> (23 - exp2);
			frac_val = mantissa & safe_mask;
		}
		else frac_val = mantissa & 0xFFFFFF;

		var preceed = fmt->preceding;
		var int_tmp = int_val;
		do {
			*ptr++ = '0';
			int_tmp /= base;
		} while(int_tmp || ptr - buffer < preceed);
		
		var resume_ptr = ptr;
		var length = ptr - buffer;
		do {
			int_tmp = int_val / base;
			*--ptr = numbers[int_val - int_tmp * base];
			int_val = int_tmp;
		} while(int_val);
		ptr = resume_ptr;
		

		int precision = fmt->precision;
		if (frac_val)
		{
			*ptr++ = '.';
			var prec = precision;
			if (!prec) prec = 4;
			else if (prec > 8) prec = 8;
			while(--prec > 0)
			{
				frac_val *= base;
				*ptr++ = numbers[frac_val >> (24 + safe_shift)];
				frac_val &= safe_mask;
			}
			// We do not round to safe complexity: 99.9999 or hex values
			if (!precision)	while (*ptr == '0') ptr--;
		}
		else if (precision)
		{
			if (!frac_val) *ptr++= '.';
			resume_ptr = ptr;
			while (precision--) *ptr++ = '0';
		}
		
		write(fmt, buffer, ptr - buffer);
	}
}
void write_r32f(io::fmt* fmt, float value)
{
	union
	{
		float f;
		struct
		{
			uint ml : 16;
			uint mh : 7;
			uint e : 8;
			uint s : 1;
		};
	} x {value};
	
	var flags = fmt->flags;
	var base = 10;
	if (flags & (io::HEX | io::OCT | io::BIN))
	{
		//to_string(fmt, x.u);
		return;
	}
	
	var numbers = io::lower_hex;
	if (flags & io::UPPER) numbers = io::upper_hex;

	char buffer[64];
	var ptr = buffer;
	sbyte e = (sbyte)x.e - SBYTEMAX;
	if (x.s) *ptr++ = '-';
	else if (flags & io::SIGN) *ptr++ = '+';

	if (e > 18)
	{
		if (flags & io::UPPER) write(fmt, "INFINITY");
		else write(fmt, "infinity");
	}
	else if (e < -3)
	{
		write(fmt, "0");
		return;
	}
	else
	{
		ulong mantissa = (ulong)x.ml + ((ulong)x.mh << 16) + (1<<23);
		var val_int = mantissa >> (23 - e);
		var tmp_int = val_int;
		do {
			*ptr++ = '0';
			tmp_int /= base;
		} while(tmp_int || ptr - buffer < fmt->preceding);
		
		var resume_ptr = ptr;
		var length = ptr - buffer;
		do {
			tmp_int = val_int / base;
			*--ptr = numbers[val_int - tmp_int * base];
			val_int = tmp_int;
		} while(val_int);
		ptr = resume_ptr;

		*ptr++ = '.';
		printf("[%X]", mantissa >> (20 - e));
		switch (0x7 & (mantissa >> (20 - e)))
		{
			case 0:
				*ptr++ = '0';
				*ptr++ = '0';
				*ptr++ = '0';
				break;
			case 1:
				*ptr++ = '1';
				*ptr++ = '2';
				*ptr++ = '5';
				break;
			case 2:
				*ptr++ = '2';
				*ptr++ = '5';
				*ptr++ = '0';
				break;
			case 3:
				*ptr++ = '3';
				*ptr++ = '7';
				*ptr++ = '5';
				break;
			case 4:
				*ptr++ = '5';
				*ptr++ = '0';
				*ptr++ = '0';
				break;
			case 5:
				*ptr++ = '6';
				*ptr++ = '2';
				*ptr++ = '5';
				break;
			case 6:
				*ptr++ = '7';
				*ptr++ = '5';
				*ptr++ = '0';
				break;
			case 7:
				*ptr++ = '8';
				*ptr++ = '7';
				*ptr++ = '5';
				break;
		}
		
		length = ptr - buffer;
		write(fmt, buffer, length);
	}
}


void to_string(io::fmt* fmt, long value)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "LONG");
		else write(fmt, "long");
	}
	else
	{
		write_s64(fmt, value);
	}
}
void to_string(io::fmt* fmt, ulong value)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "ULONG");
		else write(fmt, "ulong");
	}
	else
	{
		assert(value <= LONGMAX);
		write_s64(fmt, (long)value);
	}
}
void to_string(io::fmt* fmt, int value)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "INT");
		else write(fmt, "int");
	}
	else
	{
		write_s64(fmt, (long)value);
	}
}
void to_string(io::fmt* fmt, uint value)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "UINT");
		else write(fmt, "uint");
	}
	else
	{
		write_s64(fmt, (long)value);
	}
}
void to_string(io::fmt* fmt, short value)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "SHORT");
		else write(fmt, "short");
	}
	else
	{
		write_s64(fmt, (long)value);
	}
}
void to_string(io::fmt* fmt, ushort value)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "USHORT");
		else write(fmt, "ushort");
	}
	else
	{
		write_s64(fmt, (long)value);
	}
}
void to_string(io::fmt* fmt, sbyte value)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "SBYTE");
		else write(fmt, "sbyte");
	}
	else
	{
		write_s64(fmt, (long)value);
	}
}
void to_string(io::fmt* fmt, byte value)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "BYTE");
		else write(fmt, "byte");
	}
	else
	{
		write_s64(fmt, (long)value);
	}
}
void to_string(io::fmt* fmt, bool value)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "BOOL");
		else write(fmt, "bool");
	}
	else
	{
		write_s64(fmt, (long)value);
	}
}
void to_string(io::fmt* fmt, float value)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "FLOAT");
		else write(fmt, "float");
	}
	else write_r32(fmt, value);
}
void to_string(io::fmt* fmt, char c)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "CHAR");
		else write(fmt, "char");
	}
	else write(fmt, c);
}
void to_string(io::fmt* fmt, const char* c)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "CSTRING");
		else write(fmt, "cstring");
	}
	else write(fmt, c);
}

#define IO_CUSTOM_FMT(size) \
	char buffer[size];\
	var custom = *fmt;\
	custom.type = io::WRITE_TO_BUFFER;\
	custom.buffer.capacity = size;\
	custom.buffer.length = 0;\
	custom.buffer.data = (byte*)buffer;

void to_string(io::fmt* fmt, int2 arg)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "INT2");
		else write(fmt, "int2");
	}
	else
	{
		IO_CUSTOM_FMT(64);
		write(&custom, '(');
		write_s64(&custom, arg.x);
		write(&custom, ", ");
		write_s64(&custom, arg.y);
		write(&custom, ')');
		write(fmt, custom.buffer);
	}
}
void to_string(io::fmt* fmt, float2 arg)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "FLOAT2");
		else write(fmt, "float2");
	}
	else
	{
		IO_CUSTOM_FMT(64);
		write(&custom, '(');
		write_r32(&custom, arg.x);
		write(&custom, ", ");
		write_r32(&custom, arg.y);
		write(&custom, ')');
		write(fmt, custom.buffer);
	}
}
void to_string(io::fmt* fmt, float3 arg)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "FLOAT2");
		else write(fmt, "float2");
	}
	else
	{
		IO_CUSTOM_FMT(96);
		write(&custom, '(');
		write_r32(&custom, arg.x);
		write(&custom, ", ");
		write_r32(&custom, arg.y);
		write(&custom, ", ");
		write_r32(&custom, arg.z);
		write(&custom, ')');
		write(fmt, custom.buffer);
	}
}
void to_string(io::fmt* fmt, float4 arg)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "FLOAT2");
		else write(fmt, "float2");
	}
	else
	{
		IO_CUSTOM_FMT(128);
		write(&custom, '(');
		write_r32(&custom, arg.x);
		write(&custom, ", ");
		write_r32(&custom, arg.y);
		write(&custom, ", ");
		write_r32(&custom, arg.z);
		write(&custom, ", ");
		write_r32(&custom, arg.w);
		write(&custom, ')');
		write(fmt, custom.buffer);
	}
}
void to_string(io::fmt* fmt, fixed_buffer arg)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "STRING");
		else write(fmt, "string");
	}
	else
	{
		write(fmt, arg);
	}
}

void to_string(io::fmt* fmt, error arg)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "ERROR");
		else write(fmt, "error");
	}
	else
	{
		write(fmt, 'E');
		write_s64(fmt, (long)arg.code);
		write(fmt, ' ');
		write(fmt, arg.message);
		write(fmt, " (");
		write(fmt, arg.trace);
		write(fmt, ')');
	}
}

void __print_impl(io::fmt* fmt, const char* const format)
{
	const char* traverse = format;
	while (*traverse && *traverse != '%') traverse++;
	var length = traverse - format;
	if (length > 0)
		write(fmt, format, length);
	assert(!*traverse);
}

template<typename T, typename... Args>
void __print_impl(io::fmt* fmt, const char* const format, T arg, Args... args)
{
	const char* traverse = format;
	while (*traverse && *traverse != '%') traverse++;
	var length = traverse - format;
	if (length > 0)
		write(fmt, format, length);
	if (!*traverse)
		return;

	flags32 flags = 0;
	while (*++traverse)
	{
		var c = *traverse;
		switch (c)
		{
			/*case '#': flags.sharp = true; break;
			case '0': flags.zero = !flags.minus; break;
			case '+': flags.plus = true; break;
			case '-': flags.minus = true; flags.zero = false; break;
			case ' ': flags.space = true; break;
			*/
			
			case 't': flags |= io::TYPE; goto write_arg;
			case 'v': flags |= io::VALUE; goto write_arg;

			/*default:
			{
				if ('a' <= c && c <= 'z')
				{
					flags.type = c;
					__print_arg(p, flags, arg);
					traverse++;
				}
			} goto exit_flags;*/
		}
	}
	write_arg:
	traverse++;
	fmt->flags = flags;
	to_string(fmt, arg);
	exit_flags:
	__print_impl(fmt, traverse, args...);
}

template<typename T, typename... Args>
void print(const char* const format, T arg, Args... args)
{
	io::fmt fmt {};
	fmt.type = io::WRITE_TO_FILE;
	fmt.file = stdout;
	__print_impl(&fmt, format, arg, args...);
	fputc('\n', fmt.file);
	fflush(fmt.file);
}

template<typename T>
void print(T arg)
{
	io::fmt fmt {};
	fmt.type = io::WRITE_TO_FILE;
	fmt.file = stdout;
	to_string(&fmt, arg);
	fputc('\n', fmt.file);
	fflush(fmt.file);
}

