#ifdef __ILP32__
!Could not compile x64
#endif

typedef signed char sbyte;
typedef unsigned char byte;
typedef unsigned char uchar;
typedef short unsigned int ushort;
typedef unsigned int uint;
typedef long unsigned int ulong;
typedef long unsigned int uptr;
typedef long signed int sptr;

typedef unsigned int flags32;
typedef long unsigned int flags64;

typedef       unsigned char u8;
typedef short unsigned int  u16;
typedef       unsigned int  u32;
typedef long  unsigned int  u64;
typedef         signed char s8;
typedef short   signed int  s16;
typedef         signed int  s32;
typedef long    signed int  s64;


