// TODO: Convert to void and have out as parameter pointer
void
OptimizedPath(string path, string& out)
{
	var prev = -1;
	var state = 0;
	for (var i = 0; i < path.length; i++)
	{
		var c = path[i];

		if (c == '/' || c == '\\')
		{
			if (state == 2)
			{
				for (var j = out.length - 1; j > 0; j--)
				{
					if (out[j] == '/' && !--state)
					{
						out.length = j + 1;
						break;
					}
				}
			}
			else if (prev != i - 1)
				out[out.length++] = '/';

			prev = i;
		}
		else
		{
			if (path[i] == '.')
				state++;
			else
				state = 0;

			out[out.length++] = c;
		}
	}

	out[out.length++] = '/';
}




inline string
ReadAllText(const char* path)
{
	FILE* file = fopen(path, "rb");
	assert(file);
	
	s32 size;
	fseek(file, 0, SEEK_END);
	size = ftell(file);
	fseek(file, 0, SEEK_SET);
	
	auto buffer = malloc(size + 1);
	fread(buffer, 1, size, file);
	fclose(file);
	
	string result;
	result.data_void = buffer;
	result.data[size] = 0;
	result.length = size;
	result.capacity = size + 1;
	return result;
}

inline void
ReadAllText(const char* path, string& destination)
{
	FILE* file = fopen(path, "rb");
	assert(file);

	s32 size;
	fseek(file, 0, SEEK_END);
	size = ftell(file);
	fseek(file, 0, SEEK_SET);
	assert(size < destination.capacity);

	fread(destination.data_void, 1, size, file);
	destination.length = size;
}


Font LoadFont(Solitude& solitude, string name, static_buffer* tmp)
{
	Font font;
	font.Name = name;
	
	var path = String(tmp);
	Append(path, solitude.App.AssetPath);
	Append(path, name);
	Append(path, ".png");
	Terminate(path);

	// font.Map = LoadTexture(path.data_cchar); // TODO
	
	path.length -= 3;
	Append(path, "sfont");
	Terminate(path);
	FILE* file = fopen(path.data_cchar, "rb");
	assert(file);
	s32 size;
	fseek(file, 0, SEEK_END);
	size = ftell(file);
	fseek(file, 0, SEEK_SET);
	//font.length = size / sizeof(Glyph);
	
	auto buffer = malloc(size);
	fread(buffer, 1, size, file);
	//font.Glyphs = reinterpret_cast<Glyph*>(buffer);

	return font;
}














// LOAD .OBJ
inline v3 UpOf(Quaternion l)
{
	auto x = l.x * 2;
	auto y = l.y * 2;
	auto z = l.z * 2;
	
	return {
		l.x * y - l.w * z,
		1 - (l.x * x + l.z * z),
		l.y * z + l.w * x,
	};
}

inline m4
MatrixOf(Transform transform)
{
	v3& p = transform.position;
	Quaternion& l = transform.rotation;
	
	auto x = l.x * 2; // prolly multiply with scale?
	auto y = l.y * 2;
	auto z = l.z * 2;
	auto xx = l.x * x;
	auto yz = l.y * z;
	auto wx = l.w * x;
	
	v3 up {
		l.x * y - l.w * z,
		1 - (xx + l.z * z),
		yz + wx,
	};
	
	v3 fwd {
		l.x * z + l.w * y,
		yz - wx,
		1 - (xx + l.y * y),
	};
	
	v3 right = Cross(up, fwd);
	
	return {
		right.x, up.x, fwd.x, p.x,
		right.y, up.y, fwd.y, p.y,
		right.z, up.z, fwd.z, p.z,
		0.0f, 0.0f, 0.0f, 1.0f
	};
}

inline void
ParseObjVertex(char* line, s32& v, s32& uv, s32& n)
{
	for (char* lptr = line; lptr; ++lptr)
	{
		if (*lptr == '/') {
			*lptr = 0;
			var uvstart = lptr + 1;
			for (char* uvptr = uvstart; uvptr; ++uvptr)
			{
				if (*uvptr == '\0')
				{
					// Vertex and UV are used
					v = (s32)strtol(line, 0, 10);
					uv = (s32)strtol(uvstart, 0, 10);
					n = 0;
					return;
				}
				else if (*uvptr == '/')
				{
					// Vertex and normals are used and UV is maybe used
					*uvptr = 0;
					v = (s32)strtol(line, 0, 10);
					uv = (uvptr - uvstart) > 1 ? (s32)strtol(uvstart, 0, 10) : 0;
					n = (s32)strtol(uvptr + 1, 0, 10);
					return;
				}
			}
			
			assert(false);
		}
	}
	
	// Only vertex is used
	v = (s32)strtol(line, 0, 10);
	uv = 0;
	n = 0;
}

/*
inline Mesh
LoadObj(Solitude& solitude, string name)
{
	var tmp0 = Use(solitude.Temp);

	Mesh mesh;
	
	string m_path = String(solitude.App.AssetPath, tmp0);
	Append(m_path, "models/bunkbed.obj");
	Terminate(m_path);
	FILE* m_file = fopen(m_path.CData, "r");
	if (m_file)
	{
		for (char line[128];;) {
			auto res = fscanf(m_file, "%s", line);
			if (res == EOF)
				break;
			
			if (line[0] == 'o')
			{
				fscanf(m_file, "%s", line);
				mesh.Name = StringNonAlloc(line);
				mesh.VertexCount = 0;
				mesh.Vertices = null;
				mesh.IndexCount = 0;
				mesh.Indices = null;
				
				var tmp_vertices_buffer = Use(solitude.Temp);
				var tmp_vertices = reinterpret_cast<v4*>(tmp_vertices_buffer->data);
				var tmp_vertex_count = 0;
				var tmp_normals_buffer = Use(solitude.Temp);
				var tmp_normals = reinterpret_cast<v3*>(tmp_normals_buffer->data);
				var tmp_normal_count = 0;
				var tmp_uvs_buffer = Use(solitude.Temp);
				var tmp_uvs = reinterpret_cast<v2*>(tmp_uvs_buffer->data);
				var tmp_uv_count = 0;
				
				for (;;)
				{
					res = fscanf(m_file, "%s", line);
					if (res == EOF)
						break;
					
					
					if (line[0] == 'v' && line[1] == '\0')
					{
						assert(tmp_vertex_count * 3 < SOLITUDE_TEMP_SIZE);
						v4 v;
						fscanf(m_file, "%s", line);
						v.x = strtof(line, 0);
						fscanf(m_file, "%s", line);
						v.y = strtof(line, 0);
						fscanf(m_file, "%s", line);
						v.z = strtof(line, 0);
						v.w = 1.0f;
						tmp_vertices[tmp_vertex_count++] = v;
					}
					else if (line[0] == 'v' && line[1] == 'n')
					{
						assert(tmp_normal_count * 3 < SOLITUDE_TEMP_SIZE);
						v3 v;
						fscanf(m_file, "%s", line);
						v.x = strtof(line, 0);
						fscanf(m_file, "%s", line);
						v.y = strtof(line, 0);
						fscanf(m_file, "%s", line);
						v.z = strtof(line, 0);
						tmp_normals[tmp_normal_count++] = v;
					}
					else if (line[0] == 'v' && line[1] == 't')
					{
						assert(tmp_uv_count * 2 < SOLITUDE_TEMP_SIZE);
						v2 v;
						fscanf(m_file, "%s", line);
						v.x = strtof(line, 0);
						fscanf(m_file, "%s", line);
						v.y = strtof(line, 0);
						tmp_uvs[tmp_uv_count++] = v;
					}
					else if (line[0] == 'f' && line[1] == '\0')
					{
						if (!mesh.Vertices)
						{
							var buffersize = 12;//tmp_vertex_count * 6;
							mesh.Vertices = alloc(Vertex, buffersize);
							mesh.VertexCapacity = buffersize;
						}
						
						if (mesh.VertexCount + 3 >= mesh.VertexCapacity) {
							Reserve(
								mesh.Vertices,
								mesh.VertexCapacity,
								mesh.VertexCapacity * 2 + 3);
						}
						
						for (var i = 0; i < 3; i++)
						{
							s32 p, uv, n;
							fscanf(m_file, "%s", line);
							ParseObjVertex(line, p, uv, n);
							Vertex v;
							v.position = tmp_vertices[p - 1];
							v.uv0 = uv ? tmp_uvs[uv - 1] : v2{0.0f, 0.0f};
							v.uv1 = {0.0f, 0.0f};
							v.color = {1.0f, 1.0f, 1.0f, 1.0f};
							v.normal = n ? tmp_normals[n - 1] : v3{0.0f, 0.0f, 0.0f};
							v.tangent = {0.0f, 0.0f, 0.0f, 0.0f}; // http://www.terathon.com/code/tangent.html
							
							mesh.Vertices[mesh.VertexCount + 2 - i] = v;
						}
						mesh.VertexCount += 3;
					}
					else
					{
						//print("Ignoring: \"%s\"", line);
					}
				}
				
				tmp_vertices_buffer->used = false;
				tmp_normals_buffer->used = false;
				tmp_uvs_buffer->used = false;
			}
		}
		fclose(m_file);
	}

	tmp0->used = false;
	
	mesh.Indices = alloc(u32, mesh.VertexCount);
	mesh.IndexCapacity = mesh.IndexCount = mesh.VertexCount;
	for (var i = 0; i < mesh.VertexCount; i++)
		mesh.Indices[i] = (u32)i;
	
	return mesh;
}*/