inline float3 operator * (float3x3 l, float3 r)
{
	return {
		l.m00 * r.x + l.m01 * r.y + l.m02 * r.z,
		l.m10 * r.x + l.m11 * r.y + l.m12 * r.z,
		l.m20 * r.x + l.m21 * r.y + l.m22 * r.z,
	};
}
inline float4x4 operator * (float4x4 l, float4x4 r)
{
	return {
		l.m00 * r.m00 + l.m01 * r.m10 + l.m02 * r.m20 + l.m03 * r.m30,
		l.m00 * r.m01 + l.m01 * r.m11 + l.m02 * r.m21 + l.m03 * r.m31,
		l.m00 * r.m02 + l.m01 * r.m12 + l.m02 * r.m22 + l.m03 * r.m32,
		l.m00 * r.m03 + l.m01 * r.m13 + l.m02 * r.m23 + l.m03 * r.m33,

		l.m10 * r.m00 + l.m11 * r.m10 + l.m12 * r.m20 + l.m13 * r.m30,
		l.m10 * r.m01 + l.m11 * r.m11 + l.m12 * r.m21 + l.m13 * r.m31,
		l.m10 * r.m02 + l.m11 * r.m12 + l.m12 * r.m22 + l.m13 * r.m32,
		l.m10 * r.m03 + l.m11 * r.m13 + l.m12 * r.m23 + l.m13 * r.m33,

		l.m20 * r.m00 + l.m21 * r.m10 + l.m22 * r.m20 + l.m23 * r.m30,
		l.m20 * r.m01 + l.m21 * r.m11 + l.m22 * r.m21 + l.m23 * r.m31,
		l.m20 * r.m02 + l.m21 * r.m12 + l.m22 * r.m22 + l.m23 * r.m32,
		l.m20 * r.m03 + l.m21 * r.m13 + l.m22 * r.m23 + l.m23 * r.m33,

		l.m30 * r.m00 + l.m31 * r.m10 + l.m32 * r.m20 + l.m33 * r.m30,
		l.m30 * r.m01 + l.m31 * r.m11 + l.m32 * r.m21 + l.m33 * r.m31,
		l.m30 * r.m02 + l.m31 * r.m12 + l.m32 * r.m22 + l.m33 * r.m32,
		l.m30 * r.m03 + l.m31 * r.m13 + l.m32 * r.m23 + l.m33 * r.m33
	};
}
inline float4 operator * (float4x4 l, float4 r)
{
	return {
		l.m00 * r.x + l.m01 * r.y + l.m02 * r.z + l.m03 * r.w,
		l.m10 * r.x + l.m11 * r.y + l.m12 * r.z + l.m13 * r.w,
		l.m20 * r.x + l.m21 * r.y + l.m22 * r.z + l.m23 * r.w,
		l.m30 * r.x + l.m31 * r.y + l.m32 * r.z + l.m33 * r.w,
	};
}


inline float determinant(float2x2 x)
{
	return x.m00 * x.m11
		 - x.m01 * x.m10;
}
inline float determinant(float3x3 m)
{
	return
		+ m.m00 * (m.m11 * m.m22 - m.m21 * m.m12)
		- m.m01 * (m.m10 * m.m22 - m.m20 * m.m12)
		+ m.m02 * (m.m10 * m.m21 - m.m20 * m.m11);
}
inline float determinant(float4x4 x)
{
	var a0 = x.m00 * x.m11 - x.m10 * x.m01;
	var a1 = x.m00 * x.m21 - x.m20 * x.m01;
	var a2 = x.m00 * x.m31 - x.m30 * x.m01;
	var a3 = x.m10 * x.m21 - x.m20 * x.m11;
	var a4 = x.m10 * x.m31 - x.m30 * x.m11;
	var a5 = x.m20 * x.m31 - x.m30 * x.m21;

	var b0 = x.m02 * x.m13 - x.m12 * x.m03;
	var b1 = x.m02 * x.m23 - x.m22 * x.m03;
	var b2 = x.m02 * x.m33 - x.m32 * x.m03;
	var b3 = x.m12 * x.m23 - x.m22 * x.m13;
	var b4 = x.m12 * x.m33 - x.m32 * x.m13;
	var b5 = x.m22 * x.m33 - x.m32 * x.m23;

	return a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;
}


inline float2x2 transpose(float2x2 x)
{
	return {
		x.m00, x.m10,
		x.m01, x.m11,
	};
}
inline float3x3 transpose(float3x3 x)
{
	return {
		x.m00, x.m10, x.m20,
		x.m01, x.m11, x.m21,
		x.m02, x.m12, x.m22,
	};
}
inline float4x4 transpose(float4x4 x)
{
	return {
		x.m00, x.m10, x.m20, x.m30,
		x.m01, x.m11, x.m21, x.m31,
		x.m02, x.m12, x.m22, x.m32,
		x.m03, x.m13, x.m23, x.m33,
	};
}





inline float2x2 inverse(float2x2 x)
{
	var determinant = x.m00 * x.m11 - x.m01 * x.m10;
	assert(determinant != 0);

	var inv_determinant = 1.0f / determinant;
	return {
		+ x.m11 * inv_determinant,
		- x.m01 * inv_determinant,
		- x.m10 * inv_determinant,
		+ x.m00 * inv_determinant,
	};
}
inline float3x3 inverse(float3x3 m)
{
	var determinant =
		+ m.m00 * (m.m11 * m.m22 - m.m21 * m.m12)
		- m.m01 * (m.m10 * m.m22 - m.m20 * m.m12)
		+ m.m02 * (m.m10 * m.m21 - m.m20 * m.m11);
	assert(determinant != 0);

	var inv_determinant = 1.0f / determinant;
	return {
		+(m.m11 * m.m22 - m.m12 * m.m21) * inv_determinant,
		-(m.m01 * m.m22 - m.m21 * m.m02) * inv_determinant,
		+(m.m01 * m.m12 - m.m11 * m.m02) * inv_determinant,

		-(m.m10 * m.m22 - m.m12 * m.m20) * inv_determinant,
		+(m.m00 * m.m22 - m.m20 * m.m02) * inv_determinant,
		-(m.m00 * m.m12 - m.m10 * m.m02) * inv_determinant,

		+(m.m10 * m.m21 - m.m20 * m.m11) * inv_determinant,
		-(m.m00 * m.m21 - m.m20 * m.m01) * inv_determinant,
		+(m.m00 * m.m11 - m.m01 * m.m10) * inv_determinant,
	};
}
inline float4x4 inverse(float4x4 x)
{
	var a0 = x.m00 * x.m11 - x.m10 * x.m01;
	var a1 = x.m00 * x.m21 - x.m20 * x.m01;
	var a2 = x.m00 * x.m31 - x.m30 * x.m01;
	var a3 = x.m10 * x.m21 - x.m20 * x.m11;
	var a4 = x.m10 * x.m31 - x.m30 * x.m11;
	var a5 = x.m20 * x.m31 - x.m30 * x.m21;
	
	var b0 = x.m02 * x.m13 - x.m12 * x.m03;
	var b1 = x.m02 * x.m23 - x.m22 * x.m03;
	var b2 = x.m02 * x.m33 - x.m32 * x.m03;
	var b3 = x.m12 * x.m23 - x.m22 * x.m13;
	var b4 = x.m12 * x.m33 - x.m32 * x.m13;
	var b5 = x.m22 * x.m33 - x.m32 * x.m23;
	
	var determinant = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;
	assert(determinant != 0);

	var inv_determinant = 1.0f / determinant;
	return {
		( x.m11 * b5 - x.m21 * b4 + x.m31 * b3) * inv_determinant,
		(-x.m01 * b5 + x.m21 * b2 - x.m31 * b1) * inv_determinant,
		( x.m01 * b4 - x.m11 * b2 + x.m31 * b0) * inv_determinant,
		(-x.m01 * b3 + x.m11 * b1 - x.m21 * b0) * inv_determinant,

		(-x.m10 * b5 + x.m20 * b4 - x.m30 * b3) * inv_determinant,
		( x.m00 * b5 - x.m20 * b2 + x.m30 * b1) * inv_determinant,
		(-x.m00 * b4 + x.m10 * b1 - x.m30 * b0) * inv_determinant,
		( x.m00 * b3 - x.m10 * b1 + x.m20 * b0) * inv_determinant,

		( x.m13 * a5 - x.m23 * a4 + x.m33 * a3) * inv_determinant,
		(-x.m03 * a5 + x.m23 * a2 - x.m33 * a1) * inv_determinant,
		( x.m03 * a4 - x.m13 * a2 + x.m33 * a0) * inv_determinant,
		(-x.m03 * a3 + x.m13 * a1 - x.m23 * a0) * inv_determinant,

		(-x.m12 * a5 + x.m22 * a4 - x.m32 * a3) * inv_determinant,
		( x.m02 * a5 - x.m22 * a2 + x.m32 * a1) * inv_determinant,
		(-x.m02 * a4 + x.m12 * a2 - x.m32 * a0) * inv_determinant,
		( x.m02 * a3 - x.m12 * a1 + x.m22 * a0) * inv_determinant,
	};
}




/*
static inline m4
M4Identity()
{
	return {
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f,
	};
}

inline m4_inv
Orthographics(r32 aspect, r32 ncp, r32 fcp)
{

	r32 a = 1.0f;
	r32 b = aspect;
	r32 n = ncp;
	r32 f = fcp;
	r32 d = 2.0f / (n - f);
	r32 e = (n + f) / (n - f);
	return
	{
		{
			a,  0,  0,  0,
			0,  b,  0,  0,
			0,  0,  d,  e,
			0,  0,  0,  1
		},

		{
			1 / a,   0,   0,    0,
			0, 1 / b,   0,    0,
			0,   0, 1 / d, -e / d,
			0,   0,   0,    1
		},
	};
}

/**
 * aspect = width / height
 * ffd = Flange focal distance -> field of view
 * ncp = near clip plane distance
 * fcp = far clip plane distance
 * /
inline m4_inv
Perspective(r32 aspect, r32 ffd, r32 ncp, r32 fcp)
{
	r32 a = 1.0f;
	r32 b = aspect;
	r32 c = ffd;
	r32 n = ncp;
	r32 f = fcp;
	r32 d = (n + f) / (n - f);
	r32 e = (2 * f * n) / (n - f);
	return
	{
		{
			a * c, 0, 0, 0,
			0, b * c, 0, 0,
			0, 0,  d, e,
			0, 0, -1, 0,
		},
		{
			1 / (a * c), 0, 0, 0,
			0, 1 / (b * c), 0, 0,
			0, 0, 0, -1.0f,
			0, 0, 1 / e, d / e,
		}
	};
}

// seems not right
inline m4_inv
MatrixView(v3 position, Quaternion rotation)
{
	m4_inv result;
	m3 r = RotationMatrixOf(rotation);
	result.forward = {
		r.m00, r.m01, -r.m02, position.x,
		r.m10, r.m11, -r.m12, position.y,
		r.m20, r.m21, -r.m22, position.z,
		 0.0f,  0.0f, -0.0f, 1.0f,
	};
	result.inverse = Inverse(result.forward);
	return result;
}

inline m4_inv
MatrixTRS(v3 position, Quaternion rotation, v3 scale)
{
	m4_inv result;
	m3 r = RotationMatrixOf(rotation);
	result.forward = {
		r.m00 * scale.x, r.m01 * scale.x, r.m02 * scale.x, position.x,
		r.m10 * scale.y, r.m11 * scale.y, r.m12 * scale.y, position.y,
		r.m20 * scale.z, r.m21 * scale.z, r.m22 * scale.z, position.z,
		0.0f,  0.0f, 0.0f, 1.0f,
	};
	result.inverse = Inverse(result.forward);
	return result;
}

// untested
inline m4_inv
MatrixEuler(v3 euler, v3 position)
{
	var ca = Cos(euler.x);
	var sa = Sin(euler.x);
	var ch = Cos(euler.y);
	var sh = Sin(euler.y);
	var cb = Cos(euler.z);
	var sb = Sin(euler.z);

	m4_inv result{};
	result.forward.m00 = ch * ca;
	result.forward.m01 = sh * sb - ch * sa * cb;
	result.forward.m02 = ch * sa * sb + sh * cb;
	result.forward.m03 = position.x;
	result.forward.m10 = sa;
	result.forward.m11 = ca * cb;
	result.forward.m12 = -ca * sb;
	result.forward.m13 = position.y;
	result.forward.m20 = -sh * ca;
	result.forward.m21 = sh * sa * cb + ch * sb;
	result.forward.m22 = -sh * sa * sb + ch * cb;
	result.forward.m23 = position.z;
	result.forward.m33 = 1.0f;
	result.inverse = Inverse(result.forward);
	return result;
}



inline v3 MatrixX(m4 m) { return *(v3*)(&m.e[0]); }
inline v3 MatrixY(m4 m) { return *(v3*)(&m.e[4]); }
inline v3 MatrixZ(m4 m) { return *(v3*)(&m.e[8]); }
inline v3 MatrixP(m4 m) { return *(v3*)(&m.e[12]); }*/






/*inline m3
RotationMatrixOf(Quaternion q)
{
	var sqw = q.w * q.w;
	var sqx = q.x * q.x;
	var sqy = q.y * q.y;
	var sqz = q.z * q.z;
	var invs = 1.0f / (sqx + sqy + sqz + sqw);
	var m00 = (sqx - sqy - sqz + sqw) * invs;
	var m11 = (-sqx + sqy - sqz + sqw) * invs;
	var m22 = (-sqx - sqy + sqz + sqw) * invs;

	var tmp1 = q.x * q.y;
	var tmp2 = q.z * q.w;
	var m10 = 2.0f * (tmp1 + tmp2) * invs;
	var m01 = 2.0f * (tmp1 - tmp2) * invs;

	tmp1 = q.x * q.z;
	tmp2 = q.y * q.w;
	var m20 = 2.0f * (tmp1 - tmp2) * invs;
	var m02 = 2.0f * (tmp1 + tmp2) * invs;
	tmp1 = q.y * q.z;
	tmp2 = q.x * q.w;
	var m21 = 2.0f * (tmp1 + tmp2) * invs;
	var m12 = 2.0f * (tmp1 - tmp2) * invs;

	return {
		m00, m01, m02,
		m10, m11, m12,
		m20, m21, m22,
	};
}*/