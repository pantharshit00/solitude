#define D_TYPE PPCAT(dictionary, D_SUFF)
#define D_CONS PPCAT(Dictionary, D_SUFF)

struct D_TYPE
{
	struct Entry
	{
		s32 bucket;
		
		s32 hash;
		s32 next;
		D_TKEY key;
		D_TVAL value;
	};
	
	Entry* Entries;
	s32 capacity;
	s32 Count;
	s32 FreeList;
	s32 FreeCount;
};

// Remove macros to be able to add additional versions
#undef D_TYPE
#undef D_CONS
#undef D_SUFF
#undef D_TKEY
#undef D_TVAL
