// TODO: This doesn't feel right, do something so we can initialize with a seed


// RANDOM: value between 0.. 1
#define random_value_r64 ((r64)rand() / RAND_MAX)
#define random_value_ex_r64 ((r64)rand() / (RAND_MAX + 1U))

// RANDOM: value between min [inclusive] and max [inclusive]
inline r32
Random_r32(r32 min, r32 max)
{
	assert(min <= max);
	return min + (r32)(random_value_r64 * (max - min));
}

inline r64
Random_r64(r64 min, r64 max)
{
	assert(min <= max);
	return min + (random_value_r64 * (max - min));
}

inline s32
Random_s32(s32 min, s32 max)
{
	assert(min <= max);
	return min + (s32)(random_value_ex_r64 * (max - min + 1)); 
}

inline s64
Random_s64(s64 min, s64 max)
{
	assert(min <= max);
	return min + (s64)(random_value_ex_r64 * (max - min + 1)); 
}

inline u32
Random_u32(u32 min, u32 max)
{
	assert(min <= max);
	return min + (u32)(random_value_ex_r64 * (max - min + 1)); 
}

inline u64
Random_u64(u64 min, u64 max)
{
	assert(min <= max);
	return min + (u64)(random_value_ex_r64 * (max - min + 1)); 
}

// RANDOM: value between min [inclusive] and max [exclusive]
inline r32
Random_ex_r32(r32 min, r32 max)
{
	assert(min <= max);
	return min + (r32)(random_value_ex_r64 * (max - min));
}

inline r64
Random_ex_r64(r64 min, r64 max)
{
	assert(min <= max);
	return min + (random_value_ex_r64 * (max - min));
}

inline s32
Random_ex_s32(s32 min, s32 max)
{
	assert(min <= max);
	return min + (s32)(random_value_ex_r64 * (max - min)); 
}

inline s64
Random_ex_s64(s64 min, s64 max)
{
	assert(min <= max);
	return min + (s64)(random_value_ex_r64 * (max - min)); 
}

inline s32
Random_ex_u32(u32 min, u32 max)
{
	assert(min <= max);
	return min + (u32)(random_value_ex_r64 * (max - min)); 
}

inline u64
Random_ex_u64(u64 min, u64 max)
{
	assert(min <= max);
	return min + (u64)(random_value_ex_r64 * (max - min)); 
}
