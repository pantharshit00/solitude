#define A_NAME PPCAT(array, A_SUFF)
#define A_CONS PPCAT(Array, A_SUFF)

inline A_NAME
A_CONS (s32 size = 8)
{
	A_NAME result;
	result.length = 0;
	result.data = reinterpret_cast<A_TYPE*>(malloc(sizeof(A_TYPE) * size));
	result.capacity = 8;
	return result;
}

inline void
Free(A_NAME& arr)
{
	if (arr.capacity < 1)
		return;
	
	free(arr.data);
	arr.length = 0;
	arr.capacity = 0;
}

inline void
Resize(A_NAME& arr, s32 count)
{
	assert(count >= 0);
	assert(count >= arr.length);
	
	if (arr.capacity != count)
		arr.data = reinterpret_cast<A_TYPE*>(realloc(arr.data, sizeof(A_TYPE) * count));
}

inline void
EnsureCapacity(A_NAME& arr, s32 count)
{
	assert(count >= 0);
	if (arr.capacity < count)
		arr.data = reinterpret_cast<A_TYPE*>(realloc(arr.data, sizeof(A_TYPE) * count));
}

inline void
Reserve(A_NAME& arr, s32 count)
{
	if (arr.capacity < count)
	{
		if (count < 2 * arr.capacity)
			count = 2 * arr.capacity;
		
		auto data = reinterpret_cast<A_TYPE*>(malloc(sizeof(A_TYPE) * count));
		memcpy(data, arr.data, sizeof(A_TYPE) * arr.length);
		delete [] arr.data;
		arr.data = data;
	}
}

inline void 
Push(A_NAME& arr, A_TYPE item)
{
	Reserve(arr, arr.length + 1);
	arr.data[arr.length++] = item;
}

inline A_TYPE
Pop(A_NAME& arr)
{
	assert(arr.length > 0);
	return arr.data[--arr.length];
}




inline void
Add(A_NAME& arr, A_TYPE item)
{
	Reserve(arr, arr.length + 1);
	arr.data[arr.length++] = item;
}

inline void
Add(A_NAME& l, const A_NAME r)
{
	s32 total = l.length + r.length;
	
	if (l.capacity >= total)
	{
		memcpy(&l.data[l.length], r.data, r.length * sizeof(A_TYPE));
		l.length += r.length;
	}
	else
	{
		auto size = total * sizeof(A_TYPE);
		auto data = reinterpret_cast<A_TYPE*>(malloc(size));
		memcpy(data, l.data, l.length);
		memcpy(data + l.length, r.data, size);
		
		delete [] l.data;
		
		l.data = data;
		l.length = total;
		l.capacity = total;
	}
}


inline A_TYPE*
AddNew(A_NAME& arr)
{
	Reserve(arr, arr.length + 1);
	return &arr.data[arr.length++];
}

const A_NAME
Range(const A_NAME& arr, s32 start, s32 length)
{
	assert(start >= 0);
	assert(start <= arr.length);
	assert(length >= 0);
	assert(start <= arr.length - length);
	
	A_NAME result;
	result.length = length;
	result.capacity = length;
	result.data = &arr.data[start];
	return result;
}


A_NAME
CopyRange(const A_NAME& arr, s32 start, s32 length)
{
	assert(start >= 0);
	assert(start <= arr.length);
	assert(length >= 0);
	assert(start <= arr.length - length);
	
	A_NAME result;
	result.length = length;
	result.capacity = length;
	
	if (length)
	{
		result.data = reinterpret_cast<A_TYPE*>(malloc(sizeof(A_TYPE) * length));
		memcpy(result.data, &arr.data[start], sizeof(A_TYPE) * length);
	}
	
	return result;
}


#define A_DELEGATE PPCAT(A_NAME, ForeachDelegate_r)
using A_DELEGATE = void(A_TYPE&);
inline void
Foreach(A_NAME& arr, A_DELEGATE f)
{
	for (s32 i = 0; i < arr.length; i++)
		f(arr.data[i]);
}
#undef A_DELEGATE

#define A_DELEGATE PPCAT(A_NAME, ForeachDelegate)
using A_DELEGATE = void(A_TYPE);
inline void
Foreach(A_NAME& arr, A_DELEGATE f)
{
	for (s32 i = 0; i < arr.length; i++)
		f(arr.data[i]);
}
#undef A_DELEGATE

// Remove macros to be able to add additional versions
#undef A_NAME
#undef A_CONS
#undef A_SUFF
#undef A_TYPE
