

struct RenderSettings
{
	m4_inv Projection;
	m4_inv View;
	m4_inv VP;
	v3 Eye;
	v4 FogParams;
};

struct RenderCommands
{
	s32 VertexCapacity;
	s32 VertexCount;
	Vertex* Vertices;

	s32 IndexCapacity;
	s32 IndexCount;
	u32* Indices;
};

struct Mesh
{
	string Name;
	s32 VertexCount;
	Vertex* Vertices;
	s32 IndexCount;
	u32* Indices;
};

struct Transform
{
	v3 position;
	Quaternion rotation;
	v3 scale;
};

struct UnscaledTransform
{
	v3 position;
	Quaternion rotation;
};


/**
 * https://en.wikipedia.org/wiki/Flange_focal_distance
 * https://en.wikipedia.org/wiki/Depth_of_focus
 * https://en.wikipedia.org/wiki/Depth_of_field
 * https://en.wikipedia.org/wiki/Focal_length
 */
struct Camera
{
	UnscaledTransform transform;
	r32 ffd; // flange focal distance, infleunces the field of view
	r32 ncp; // near clip plane
	r32 fcp; // far clip plane
	r32 aspect; // width / height
};


