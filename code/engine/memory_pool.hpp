#ifndef INNER_TYPE
#define INNER_TYPE TYPE
#endif

#ifndef TYPE_POOL_CAPACITY
#define TYPE_POOL_CAPACITY SOLITUDE_TEMP_COLLECTION_SIZE
#endif

#ifndef TYPE_POOL
#define TYPE_POOL PPCAT(TYPE, _pool)
#endif

#ifndef POOL_TYPE_NAME
#define POOL_TYPE_NAME PPCAT(TYPE_NAME, Pool)
#endif

struct TYPE_POOL
{
	static s32 const capacity = TYPE_POOL_CAPACITY;

	INNER_TYPE* current;
	INNER_TYPE* first;
	TYPE_POOL* next;
};

static INNER_TYPE*
TYPE_NAME(TYPE_POOL* pool)
{
	if (!pool->current)
	{
		var items = alloc(INNER_TYPE, TYPE_POOL_CAPACITY);
		for (var i = 0; i < TYPE_POOL_CAPACITY - 1; i++)
			items[i].next = &items[i + 1];

		items[TYPE_POOL_CAPACITY - 1].next = null;
		pool->current = items;
		pool->next = alloc(TYPE_POOL);
		pool->next->current = items;
		pool->next->first = items;
		pool->next->next = null;
	}

	var result = pool->current;
	pool->current = result->next;
	return result;
}

static void
Reset(TYPE_POOL* pool)
{
	var current = pool;
	for (;;)
	{
		var items = current->first;
		for (var i = 0; i < TYPE_POOL_CAPACITY - 1; i++)
			items[i].next = &items[i + 1];

		current->current = items;
		if (current->next)
		{
			items[TYPE_POOL_CAPACITY - 1].next = current->first;
			current = current->next;
		}
		else
		{
			items[TYPE_POOL_CAPACITY - 1].next = null;
			break;
		}
	}
}

static TYPE_POOL*
POOL_TYPE_NAME()
{
	TYPE_POOL* pool = alloc(TYPE_POOL);
	pool->first = alloc(INNER_TYPE, TYPE_POOL_CAPACITY);
	pool->next = null;
	Reset(pool);
	return pool;
}

#undef INNER_TYPE
#undef TYPE
#undef TYPE_NAME
#undef TYPE_POOL
#undef POOL_TYPE_NAME
