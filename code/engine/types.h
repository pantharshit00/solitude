

/*
// Buffer



struct MemoryArena
{

	uptr BlockSize;
	flags64 AllocationFlags;
	int Length;
};
struct MemoryTemporary
{
	MemoryArena* Arena;

	uptr Used;
};



struct static_buffer
{
	static_buffer* next;
	union
	{
		byte data[SOLITUDE_TEMP_SIZE];
		char chardata[SOLITUDE_TEMP_SIZE];
		uchar uchardata[SOLITUDE_TEMP_SIZE];
	};
};
#define TYPE static_buffer
#define TYPE_NAME StaticBuffer
#include "memory_pool.hpp"
*/


struct rect
{
	union
	{
		float2 min;
		struct { float min_x, min_y; };
	};
	union
	{
		float2 max;
		struct { float max_x, max_y; };
	};
};

typedef float4 Color;
union Color32
{
	struct { byte r, g, b, a; };
	int hex;
	byte e[4];
};





