#ifndef D_HASH
#define D_HASH(x) Hash(x)
#endif

#ifndef D_COMP
#define D_COMP(x,y) Equals(x, y)
#endif

#define D_TYPE PPCAT(dictionary, D_SUFF)
#define D_CONS PPCAT(Dictionary, D_SUFF)

inline D_TYPE
D_CONS(s32 size = 0)
{
	D_TYPE d;
	
	// Initialize buckets
	d.capacity = size = PrimeCeiled(size);
	d.Entries = new D_TYPE::Entry[size];
	for (s32 i = 0; i < size; i++)
		d.Entries[i].bucket = -1;
	
	d.Count = 0;
	d.FreeList = -1;
	d.FreeCount = 0;
	
	return d;
}

inline void
Resize(D_TYPE& d, s32 size)
{
	size = PrimeCeiled(size);
	assert(size > d.capacity);
	
	auto entries = new D_TYPE::Entry[size];
	memcpy(entries, d.Entries, sizeof(D_TYPE::Entry) * d.capacity);
	for (s32 i = 0; i < size; i++)
		entries[i].bucket = -1;
	
	for (s32 i = 0; i < d.Count; i++)
	{
		if (entries[i].hash >= 0)
		{
			s32 bucket = entries[i].hash % size;
			entries[i].next = entries[bucket].bucket;
			entries[bucket].bucket = i;
		}
	}
	
	delete [] d.Entries;
	d.Entries = entries;
	d.capacity = size;
}

inline void
Resize(D_TYPE& d) { Resize(d, d.capacity * 2); }

inline s32
IndexOf(D_TYPE& d, D_TKEY key)
{
	auto hash = D_HASH(key) & S32MAX;
	auto target = hash % d.capacity;
	
	for (s32 i = d.Entries[target].bucket; i >= 0; i = d.Entries[i].next)
	{
		auto& entry = d.Entries[i];
		if (entry.hash == hash && D_COMP(entry.key, key))
			return i;
	}
	
	return -1;
}

inline D_TVAL&
ValueOf(D_TYPE& d, D_TKEY key)
{
	s32 index = IndexOf(d, key);
	return d.Entries[index].value;
}

inline D_TVAL*
PtrOf(D_TYPE& d, D_TKEY key)
{
	s32 index = IndexOf(d, key);
	return index >= 0
		? &d.Entries[index].value
		: nullptr;
}

inline bool
Contains(D_TYPE& d, D_TKEY key) { return IndexOf(d, key) >= 0; }

inline void
Insert(D_TYPE& d, D_TKEY key, D_TVAL value)
{
	auto hash = D_HASH(key) & S32MAX;
	auto target = hash % d.capacity;
	
	// Check if it has to be replaced replace
	for (s32 i = d.Entries[target].bucket; i >= 0; i = d.Entries[i].next)
	{
		auto& entry = d.Entries[i];
		if (entry.hash == hash && D_COMP(entry.key, key))
		{
			entry.value = value;
			return;
		}
	}
	
	// Add new entry
	s32 index;
	if (d.FreeCount > 0)
	{
		index = d.FreeList;
		d.FreeList = d.Entries[index].next;
		d.FreeCount--;
	}
	else
	{
		if (d.Count == d.capacity)
		{
			Resize(d);
			target = hash % d.capacity;
		}
		
		index = d.Count++;
	}
	
	d.Entries[index].hash = hash;
	d.Entries[index].next = d.Entries[target].bucket;
	d.Entries[index].key = key;
	d.Entries[index].value = value;
	d.Entries[target].bucket = index;
}

inline bool
Remove(D_TYPE& d, D_TKEY key)
{
	auto hash = D_HASH(key) & S32MAX;
	auto bucket = hash % d.capacity;
	
	for (auto i = d.Entries[bucket].bucket, last = -1;
		 i >= 0;
		 last = i, i = d.Entries[i].next)
	{
		auto& entry = d.Entries[i];
		if (entry.hash == hash && D_COMP(entry.key, key))
		{
			if (last < 0) d.Entries[bucket].bucket = entry.next;
			else d.Entries[last].next = entry.next;
			
			entry.hash = -1;
			entry.next = d.FreeList;
			entry.key = {};
			entry.value = {};
			d.FreeList = i;
			d.FreeCount++;
			return true;
		}
	}
	
	return false;
}

inline void
Clear(D_TYPE& d)
{
	if (d.Count > 0)
	{
		for (s32 i = 0; i < d.capacity; i++)
		{
			d.Entries[i] = {};
			d.Entries[i].bucket = -1;
		}
		
		d.Count = 0;
		d.FreeList = -1;
		d.FreeCount = 0;
	}
}








// Foreach

typedef void (*PPCAT(dictionary_krvr_delegate, D_SUFF)) (D_TKEY& k, D_TVAL& v);
typedef void (*PPCAT(dictionary_krv_delegate, D_SUFF)) (D_TKEY& k, D_TVAL v);
typedef void (*PPCAT(dictionary_kvr_delegate, D_SUFF)) (D_TKEY k, D_TVAL& v);
typedef void (*PPCAT(dictionary_kv_delegate, D_SUFF)) (D_TKEY k, D_TVAL v);
typedef void (*PPCAT(dictionary_kr_delegate, D_SUFF)) (D_TKEY& k);
typedef void (*PPCAT(dictionary_vr_delegate, D_SUFF)) (D_TVAL& v);
typedef void (*PPCAT(dictionary_k_delegate, D_SUFF)) (D_TKEY k);
typedef void (*PPCAT(dictionary_v_delegate, D_SUFF)) (D_TVAL v);

inline void
Foreach(D_TYPE& d, PPCAT(dictionary_kv_delegate, D_SUFF) f)
{
	for (s32 i = 0; i < d.capacity; i++)
	{
		auto bucket = d.Entries[i].bucket;
		
		if (bucket >= 0)
		{
			auto& entry = d.Entries[bucket];
			f(entry.key, entry.value);
		}
	}
}

inline void
Foreach(D_TYPE& d, PPCAT(dictionary_krvr_delegate, D_SUFF) f)
{
	for (s32 i = 0; i < d.capacity; i++)
	{
		auto bucket = d.Entries[i].bucket;
		
		if (bucket >= 0)
		{
			auto& entry = d.Entries[bucket];
			f(entry.key, entry.value);
		}
	}
}

inline void
Foreach(D_TYPE& d, PPCAT(dictionary_krv_delegate, D_SUFF) f)
{
	for (s32 i = 0; i < d.capacity; i++)
	{
		auto bucket = d.Entries[i].bucket;
		
		if (bucket >= 0)
		{
			auto& entry = d.Entries[bucket];
			f(entry.key, entry.value);
		}
	}
}

inline void
Foreach(D_TYPE& d, PPCAT(dictionary_kvr_delegate, D_SUFF) f)
{
	for (s32 i = 0; i < d.capacity; i++)
	{
		auto bucket = d.Entries[i].bucket;
		
		if (bucket >= 0)
		{
			auto& entry = d.Entries[bucket];
			f(entry.key, entry.value);
		}
	}
}

inline void
ForeachKey(D_TYPE& d, PPCAT(dictionary_kr_delegate, D_SUFF) f)
{
	for (s32 i = 0; i < d.capacity; i++)
	{
		auto bucket = d.Entries[i].bucket;
		
		if (bucket >= 0)
		{
			auto& entry = d.Entries[bucket];
			f(entry.key);
		}
	}
}

inline void
ForeachKey(D_TYPE& d, PPCAT(dictionary_k_delegate, D_SUFF) f)
{
	for (s32 i = 0; i < d.capacity; i++)
	{
		auto bucket = d.Entries[i].bucket;
		
		if (bucket >= 0)
		{
			auto& entry = d.Entries[bucket];
			f(entry.key);
		}
	}
}

inline void
ForeachValue(D_TYPE& d, PPCAT(dictionary_vr_delegate, D_SUFF) f)
{
	for (s32 i = 0; i < d.capacity; i++)
	{
		auto bucket = d.Entries[i].bucket;
		
		if (bucket >= 0)
		{
			auto& entry = d.Entries[bucket];
			f(entry.value);
		}
	}
}

inline void
ForeachValue(D_TYPE& d, PPCAT(dictionary_v_delegate, D_SUFF) f)
{
	for (s32 i = 0; i < d.capacity; i++)
	{
		auto bucket = d.Entries[i].bucket;
		
		if (bucket >= 0)
		{
			auto& entry = d.Entries[bucket];
			f(entry.value);
		}
	}
}


#undef D_TYPE
#undef D_CONS
#undef D_SUFF
#undef D_TKEY
#undef D_TVAL
#undef D_HASH
#undef D_COMP