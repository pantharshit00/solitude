




/*
template <typename T> inline void
Reserve(T*& data, s32& capacity, s32 length)
{
	assert(length < SOLITUDE_MAX_ALLOC);
	if (length <= capacity)
		return;
	
	data = reinterpret_cast<T*>(realloc(data, length * sizeof(T)));
	capacity = length;
}


// https://sites.google.com/site/murmurhash/
s32 MurmurHash2(const uchar* data, s32 len, u32 seed = 0)
{
	const s32 m = 0x5BD1E995;
	const s32 r = 24;
	u32 h = seed ^ len;
	while (len >= 4)
	{
		s32 k = *reinterpret_cast<const s32*>(data);
		k *= m;
		k ^= k >> r;
		k *= m;
		h *= m;
		h ^= k;
		
		data += 4;
		len -= 4;
	}
	
	switch (len)
	{
		case 3: h ^= data[2] << 16;
		case 2: h ^= data[1] << 8;
		case 1: h ^= data[0];
		h *= m;
	}
	
	h ^= h >> 13;
	h *= m;
	h ^= h >> 15;
	
	return h;
}

inline s32
HashOf(const void* key, s32 len)
{
	return MurmurHash2(reinterpret_cast<const uchar*>(key), len, 0);
}
*/
