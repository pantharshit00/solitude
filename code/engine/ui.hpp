
void GenerateCharacter(
	char c,
	Font font,
	Vertex* vertices,
	s32& vertex_length,
	s32& vertex_capacity,
	u32* indices,
	s32& index_length,
	s32& index_capacity)
{
	Glyph glyph;
	var texture = font.Map;
	/*for (var i = 0; i < font.length; i++)
	{
		if (font.Glyphs[i].code == c)
		{
			glyph = font.Glyphs[i];
			break;
		}
	}*/

	var width = (r32)texture.width;
	var height = (r32)texture.height;
	var start_x = glyph.x / width;
	var start_y = 1.0f - glyph.y / height;
	var size_x = glyph.width / width;
	var size_y = glyph.height / height;

	Vertex v0{
		-1.0f, -1.0f, 0.0f,
		start_x, start_y - size_y,
		0.0f, 0.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
	};

	Vertex v1{
		1.0f, -1.0f, 0.0f,
		start_x + size_x, start_y - size_y,
		1.0f, 0.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
	};

	Vertex v2{
		-1.0f, 1.0f, 0.0f,
		start_x, start_y,
		0.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
	};

	Vertex v3{
		1.0f, 1.0f, 0.0f,
		start_x + size_x, start_y,
		1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
	};

	if (vertex_length + 4 > vertex_capacity)
	{
		// TODO: realloc()
	}

	vertices[vertex_length + 0] = v0;
	vertices[vertex_length + 1] = v1;
	vertices[vertex_length + 2] = v2;
	vertices[vertex_length + 3] = v3;

	if (index_length + 6 > index_capacity)
	{
		// TODO: realloc()
	}

	indices[index_length + 0] = vertex_length + 0;
	indices[index_length + 1] = vertex_length + 1;
	indices[index_length + 2] = vertex_length + 2;
	indices[index_length + 3] = vertex_length + 1;
	indices[index_length + 4] = vertex_length + 3;
	indices[index_length + 5] = vertex_length + 2;
	index_length += 6;
	vertex_length += 4;

}

// TODO(Sietse) 20191128: check compiled code if it is benificial to inline this
inline Glyphf
CharToGlyph(char c, Font& font)
{
	var texture = font.Map;
	/*for (var i = 0; i < font.length; i++)
	{
		if (font.Glyphs[i].code & U8MAX == c)
		{
			var glyph = font.Glyphs[i];
			var map = font.Map;
			var width = (r32)map.width;
			var height = (r32)map.height;
			var start_x = glyph.x / width;
			// TODO: check if we are on a UV space starts at the top of the screen
			var start_y = 1.0f - glyph.y / height;
			var size_x = glyph.width / width;
			var size_y = glyph.height / height;
			return {
				glyph.code,
				(r32)glyph.advance / map.width,
				start_x,
				start_y - size_y,
				start_x + size_x,
				start_y,
				0.0f, 0.0f, // TODO(Sietse) 20191128: figure out the size of the glyph using left and top of `glyph`
				(r32)glyph.advance / map.width,
				0.0f
			};
		}
	}*/

	assert(false);
	return Glyphf();
}

void StringToGlyphs(
	string s,
	array_glyphf& o,
	Font& font)
{
	EnsureCapacity(o, s.length);
	For(s, i)
	{
		var glyph = CharToGlyph(s[i], font);
		Push(o, glyph);
	}
}

void StringToGlyphs(
	string s,
	array_glyphf& o,
	Font& regular,
	Font& bold,
	Font& itallic,
	Font& bold_itallic)
{
	EnsureCapacity(o, s.length);
	Font& current = regular;
	For(s, i)
	{
		auto c = s[i];
		if (c == '<')
		{
			for (var j = i + 1; j < s.length; j++)
			{
				if (s[j] == '>')
				{
					if (Equals(s.data + i + 1, "b", 1))
					{
						// TODO: Do bold
					}
					else if (Equals(s.data + i + 1, "i", 1))
					{

					}
				}
			}
		}

		var glyph = CharToGlyph(s[i], regular);
		Push(o, glyph);
	}
}


void GenerateString(
	char c,
	v2& pointer,
	Mesh& mesh,
	TextSettings& settings,
	Font& font)
{

}

/*
void GenerateCharacter(
	string c,
	Mesh& mesh,
	TextSettings& settings,
	Font* reqular,
	Font* bold,
	Font* itallic,
	Font* bold_itallic)
{



	Glyph glyph;
	var texture = font.Map;
	for (var i = 0; i < font.length; i++)
	{
		if (font.Glyphs[i].code == c)
		{
			glyph = font.Glyphs[i];
			break;
		}
	}

	var width = (r32)texture.width;
	var height = (r32)texture.height;
	var start_x = glyph.x / width;
	var start_y = 1.0f - glyph.y / height;
	var size_x = glyph.width / width;
	var size_y = glyph.height / height;

	Vertex v0{
		-1.0f, -1.0f, 0.0f,
		start_x, start_y - size_y,
		0.0f, 0.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
	};

	Vertex v1{
		1.0f, -1.0f, 0.0f,
		start_x + size_x, start_y - size_y,
		1.0f, 0.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
	};

	Vertex v2{
		-1.0f, 1.0f, 0.0f,
		start_x, start_y,
		0.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
	};

	Vertex v3{
		1.0f, 1.0f, 0.0f,
		start_x + size_x, start_y,
		1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
	};

	if (vertex_length + 4 > vertex_capacity)
	{
		// TODO: realloc()
	}

	vertices[vertex_length + 0] = v0;
	vertices[vertex_length + 1] = v1;
	vertices[vertex_length + 2] = v2;
	vertices[vertex_length + 3] = v3;

	if (index_length + 6 > index_capacity)
	{
		// TODO: realloc()
	}

	indices[index_length + 0] = vertex_length + 0;
	indices[index_length + 1] = vertex_length + 1;
	indices[index_length + 2] = vertex_length + 2;
	indices[index_length + 3] = vertex_length + 1;
	indices[index_length + 4] = vertex_length + 3;
	indices[index_length + 5] = vertex_length + 2;
	index_length += 6;
	vertex_length += 4;

}


void GenerateCharacter(char c, Font font, Vertex* vertices, s32& vertex_length, s32& vertex_capacity, u32* indices, s32& index_length, s32& index_capacity)
{
	Glyph glyph;
	var texture = font.Map;
	for (var i = 0; i < font.length; i++)
	{
		if (font.Glyphs[i].code == c)
		{
			glyph = font.Glyphs[i];
			break;
		}
	}

	var width = (r32)texture.width;
	var height = (r32)texture.height;
	var start_x = glyph.x / width;
	var start_y = 1.0f - glyph.y / height;
	var size_x = glyph.width / width;
	var size_y = glyph.height / height;

	Vertex v0 {
		-1.0f, -1.0f, 0.0f,
		start_x, start_y - size_y,
		0.0f, 0.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
	};

	Vertex v1 {
		1.0f, -1.0f, 0.0f,
		start_x + size_x, start_y - size_y,
		1.0f, 0.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
	};

	Vertex v2 {
		-1.0f, 1.0f, 0.0f,
		start_x, start_y,
		0.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
	};

	Vertex v3 {
		1.0f, 1.0f, 0.0f,
		start_x + size_x, start_y,
		1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
	};

	if (vertex_length + 4 > vertex_capacity)
	{
		// TODO: realloc()
	}

	vertices[vertex_length + 0] = v0;
	vertices[vertex_length + 1] = v1;
	vertices[vertex_length + 2] = v2;
	vertices[vertex_length + 3] = v3;

	if (index_length + 6 > index_capacity)
	{
		// TODO: realloc()
	}

	indices[index_length + 0] = vertex_length + 0;
	indices[index_length + 1] = vertex_length + 1;
	indices[index_length + 2] = vertex_length + 2;
	indices[index_length + 3] = vertex_length + 1;
	indices[index_length + 4] = vertex_length + 3;
	indices[index_length + 5] = vertex_length + 2;
	index_length += 6;
	vertex_length += 4;

}

*/

