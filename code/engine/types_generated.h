#if SOLITUDE_CPP
#define ARRAY_INCLUDE "array.hpp"
#define DICTIONARY_INCLUDE "dictionary.hpp"
#else
#define ARRAY_INCLUDE "array.h"
#define DICTIONARY_INCLUDE "dictionary.h"
#endif



// Dictionaries
#if SOLITUDE_CPP
#define D_HASH(x) x
#define D_COMP(x,y) (x == y)
#endif
#define D_TKEY s32
#define D_TVAL s32
#define D_SUFF _s32_s32
#include DICTIONARY_INCLUDE

#define D_TKEY string
#define D_TVAL s32
#define D_SUFF _s32
#include DICTIONARY_INCLUDE

#define D_TKEY string
#define D_TVAL void*
#define D_SUFF _ptr
#include DICTIONARY_INCLUDE



#undef ARRAY_INCLUDE
#undef DICTIONARY_INCLUDE


#define SOLITUDE_CPP 1
