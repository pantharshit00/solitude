
inline v3
operator * (Quaternion l, v3 r)
{
	auto x = l.x * 2;
	auto y = l.y * 2;
	auto z = l.z * 2;
	auto xx = l.x * x;
	auto yy = l.y * y;
	auto zz = l.z * z;
	auto xy = l.x * y;
	auto xz = l.x * z;
	auto yz = l.y * z;
	auto wx = l.w * x;
	auto wy = l.w * y;
	auto wz = l.w * z;
	
	return {
		(1 - (yy + zz)) * r.x + (xy - wz) * r.y + (xz + wy) * r.z,
		(xy + wz) * r.x + (1 - (xx + zz)) * r.y + (yz - wx) * r.z,
		(xz - wy) * r.x + (yz + wx) * r.y + (1 - (xx + yy)) * r.z,
	};
}

inline Quaternion
operator * (Quaternion l, Quaternion r)
{
	return {
		l.w * r.x + l.x * r.w + l.y * r.z - l.z * r.y,
		l.w * r.y + l.y * r.w + l.z * r.x - l.x * r.z,
		l.w * r.z + l.z * r.w + l.x * r.y - l.y * r.x,
		l.w * r.w - l.x * r.x - l.y * r.y - l.z * r.z,
	};
}

inline Quaternion&
operator *= (Quaternion& l, Quaternion r)
{
	Quaternion q = {
		l.w * r.x + l.x * r.w + l.y * r.z - l.z * r.y,
		l.w * r.y + l.y * r.w + l.z * r.x - l.x * r.z,
		l.w * r.z + l.z * r.w + l.x * r.y - l.y * r.x,
		l.w * r.w - l.x * r.x - l.y * r.y - l.z * r.z,
	};
	
	memcpy(&l, &q, sizeof(Quaternion));
	return l;
}

inline Quaternion
operator + (Quaternion l, r32 r)
{
	return {
		l.x * r, l.y, l.z, l.w,
	};
}


inline Quaternion
AngleAxis(r32 angle, v3 axis)
{
	var v = angle / 2;
	var sv = Sin(v);
	return {
		axis.x * sv,
		axis.y * sv,
		axis.z * sv,
		Cos(v),
	};
}

// TODO: Axis are wrong!!!!!
inline Quaternion
ToQuaternion(v3 euler)
{
	var v = euler / 2;
	r32 cx = Cos(v.y);
	r32 sx = Sin(v.y);
	r32 cy = Cos(v.z);
	r32 sy = Sin(v.z);
	r32 cz = Cos(v.x);
	r32 sz = Sin(v.x);
	
	return {
		cx * cy * sz + sx * sy * cz,
		sx * cy * cz + cx * sy * sz,
		cx * sy * cz - sx * cy * sz,
		cx * cy * cz - sx * sy * sz,
	};
}

inline Quaternion
ToQuaternionDeg(v3 euler)
{
	return ToQuaternion(euler * DEG2RAD);
}

inline m4
ToMatrix(Quaternion q, v3 scale, v3 position)
{
	var sqw = q.w*q.w;
	var sqx = q.x*q.x;
	var sqy = q.y*q.y;
	var sqz = q.z*q.z;
	var invs = 1.0f / (sqx + sqy + sqz + sqw);
	var m00 = ( sqx - sqy - sqz + sqw)*invs;
	var m11 = (-sqx + sqy - sqz + sqw)*invs;
	var m22 = (-sqx - sqy + sqz + sqw)*invs;
	
	var tmp1 = q.x*q.y;
	var tmp2 = q.z*q.w;
	var m10 = 2.0f * (tmp1 + tmp2)*invs;
	var m01 = 2.0f * (tmp1 - tmp2)*invs;
	
	tmp1 = q.x*q.z;
	tmp2 = q.y*q.w;
	var m20 = 2.0f * (tmp1 - tmp2)*invs;
	var m02 = 2.0f * (tmp1 + tmp2)*invs;
	tmp1 = q.y*q.z;
	tmp2 = q.x*q.w;
	var m21 = 2.0f * (tmp1 + tmp2)*invs;
	var m12 = 2.0f * (tmp1 - tmp2)*invs;
	
	return {
		scale.x * m00, scale.y * m10, scale.z * m20, 0.0f,
		scale.x * m01, scale.y * m11, scale.z * m21, 0.0f,
		scale.x * m02, scale.y * m12, scale.z * m22, 0.0f,
		position.x,   position.y,	 position.z,	1.0f,
	};
}

inline r32
Dot(Quaternion x0, Quaternion x1) { return x0.x * x1.x + x0.y * x1.y + x0.z * x1.z + x0.w * x1.w; }


Quaternion
Normalize(Quaternion v) {
	r32 length = v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w;
	if (length < EPSILON)
		return { 0.0f, 0.0f, 0.0f, 1.0f };
	
	length = Sqrt(length);
	return {
		v.x / length,
		v.y / length,
		v.z / length,
		v.w / length
	};
}

inline Quaternion
Conjugate(Quaternion v)
{
	return {
		-v.x, -v.y, -v.z, v.w,
	};
}
