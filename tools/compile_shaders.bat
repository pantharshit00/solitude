@echo off
set compiler=C:\Libraries\vulkan\Bin\glslc.exe
set input=C:\Projects\solitude\code\shaders\
set output=C:\Projects\solitude\assets\shaders\
set files=test.vert test.frag
(for %%a in (%files%) do (
	echo Compiling %%a...
	%compiler% %input%%%a -o %output%%%a.spv
))
